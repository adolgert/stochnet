#ifndef _PETRI_STRUCTURE_H_
#define _PETRI_STRUCTURE_H_ 1

#include <tuple>


namespace afidd
{


/*! Transforms the identifiers in a petri structure to a larger space.
 */
template<typename Left, typename Right>
struct concatenate_compose
{
  typedef std::tuple<Left,Right> type;
  typedef Left left_type;
  typedef Right right_type;


  static type apply(const Left& l, const Right& g)
  {
    return std::make_tuple(l, g);
  }


  static left_type left(const type& full)
  {
    return std::get<0>(full);
  }


  static right_type right(const type& full)
  {
    return std::get<1>(full);
  }
};




template<typename Left, typename Right>
struct strategy_compose
{
  typedef void type;
  //typedef concatenate_compose<Left,Right> type;
};



/*! Given a place or description descriptor, get its index.
 */
template <typename Place>
struct access {};

/* For example, if the place descriptor had a kind and index.
template <>
struct access<MyPlace>
{
  static char kind(MyPlace const& p)
  {
    return std::get<0>(p);
  }

  static size_t index(MyPlace const& p)
  {
    return std::get<1>(p);
  }
};
*/


template<typename Petri>
struct petri_structure_traits
{
  typedef typename Petri::place_descriptor place_descriptor;
  typedef typename Petri::transition_descriptor transition_descriptor;
  typedef typename Petri::in_place_range in_place_range;
  typedef typename Petri::out_place_range out_place_range;
  typedef typename Petri::transition_range transition_range;
};




template<typename Petri>
typename petri_structure_traits<Petri>::transition_range
out_transitions
  (
    typename petri_structure_traits<Petri>::place_descriptor place,
    const Petri& net
  )
{
  return net.out_transitions(place);
}



template<typename Petri>
typename petri_structure_traits<Petri>::in_place_range
in_places
  (
    typename petri_structure_traits<Petri>::transition_descriptor transition,
    const Petri& net
  )
{
  return net.in_places(transition);
}



template<typename Petri>
typename petri_structure_traits<Petri>::out_place_range
out_places
  (
    typename petri_structure_traits<Petri>::transition_descriptor transition,
    const Petri& net
  )
{
  return net.out_places(transition);
}




}



#endif
