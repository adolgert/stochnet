import logging
logger=logging.getLogger(__file__)
import sys
import os
import math
import collections
import argparse
import numpy as np
import scipy
import networkx as nx
import json
import transition_fusion as tf
import herd_plot
try:
    import matplotlib.pyplot as plt
    import matplotlib
except ImportError:
    logger.warn('Cannot plot without matplotlib')




def plot(G, layout=None):
    if not layout:
        layout=nx.spring_layout(G)
    nx.draw(G, layout)
    plt.draw()
    plt.show()




def herd_sir():
    '''
    s->i->r. Transitions i0 for local i to s infection,
    i1 and i2 fuse to i3 for infecting neighbor, r0 for local recovery.
    '''
    ps=nx.DiGraph()
    z, s, i, r, i0, i1, i2, i3, r0,*zz=range(12)
    logger.debug('s {0} i {1} r {2} i0 {3} i1 {4} i2 {5} i3 {6} r0 {7}'.format(
            s, i, r, i0, i1, i2, i3, r0))
    arcs=[
        (s, i0, -1),
        (i, i0, -1),
        (i0, i, 2),
        (s, i1, -1),
        (i1, i, 1),
        (i, i2, -1),
        (i2, i, 1),
        (i, r0, -1),
        (r0, r, 1)
        ]
    for s, f, w in arcs:
        ps.add_edge(s, f, weight=w)

    transition_fusions={ (i1, i2) : i3 }

    def predicted_node_edge_count(contact_graph_v, contact_graph_e):
        '''
        With V vertices, and E edges, that's 3V places, 2V+2E transitions, for
        a total of 5V+2E nodes in the graph. (V=5, E=5, so 35)
        How many edges? Locally, 5V edges. Between nodes, 8E. (13*5=65)
        '''
        node=5*contact_graph_v + 2*contact_graph_e
        edge=5*contact_graph_v + 8*contact_graph_e
        return node, edge

    return ps, transition_fusions, predicted_node_edge_count



def contact_graph():
    '''
    5 nodes, 5 edges
    '''
    cg=nx.Graph()
    cg.add_edges_from([(1,2), (1,3), (3,4), (4,5), (1,5)])
    #cg.add_node(6)
    return cg


def hard_core_iterative(cnt, tol, trial_cnt=100, max_fails=None):
    '''
    Pick random points in a (0,1) by (0,1) square where they avoid
    each other with a hard sphere tolerance of tol.
    '''
    remaining_area=1-cnt*(tol*tol)*math.pi/4
    if max_fails is None:
        logger.debug('rem {0}'.format(remaining_area))
        if remaining_area>0:
            max_fails=cnt*int(1/remaining_area)
        else:
            print('not enough remaining area. smaller tolerance needed')
            return
    elif max_fails==0:
        max_fails=sys.maxsize

    logger.debug('max_fails {0}'.format(max_fails))
    fail_cnt=0
    vals=np.zeros((cnt, 2))
    vals[0]=np.random.rand(2)
    val_cnt=1
    while val_cnt<cnt and val_cnt<trial_cnt and fail_cnt<max_fails:
        trial=np.random.rand(2)
        if min(np.linalg.norm(vals[0:val_cnt]-trial, axis=1))>tol:
            vals[val_cnt]=trial
            val_cnt+=1
        else:
            fail_cnt+=1

    while val_cnt<cnt and fail_cnt<max_fails:
        trial_idx=0
        trial_rands=np.random.rand(trial_cnt, 2)
        kdtree=scipy.spatial.cKDTree(vals[0:val_cnt])
        near_dict=dict()
        while val_cnt<cnt and trial_idx<trial_cnt:
            trial=trial_rands[trial_idx]
            nearest, dx=kdtree.query(trial)
            if dx>tol:
                if nearest in near_dict:
                    close=np.vstack(near_dict[nearest])
                    if min(np.linalg.norm(close-trial, axis=1))>tol:
                        near_dict[nearest].append(trial)
                        vals[val_cnt]=trial
                        val_cnt+=1
                    else:
                        fail_cnt+=1
                else:
                    near_dict[nearest]=[trial]
                    vals[val_cnt]=trial
                    val_cnt+=1
            else:
                fail_cnt+=1
            trial_idx+=1

    if fail_cnt==max_fails:
        logger.warn('reached max_fails {0}'.format(fail_cnt))
    return vals



def delaunay_graph(points):
    simplex=scipy.spatial.Delaunay(points)
    G=nx.Graph()
    for s in simplex.simplices:
        for left, right in [(0, 1), (1, 2), (2, 0)]:
            edge=int(s[left]), int(s[right])
            edge=(min(edge), max(edge))
            if edge not in G.edges():
                G.add_edge(edge[0], edge[1])
    return G



def spatial_contact_graph(cnt, tol):
    points=hard_core_iterative(cnt, tol)
    G=delaunay_graph(points)
    for idx, p in enumerate(points):
        G.node[idx]['pos']=points[idx]
    return G



def erdos_renyi_giant(count, p):
    '''
    Erdos Renyi, but remove nodes that aren't connected.
    '''
    cg=nx.erdos_renyi_graph(count, p)
    cg.remove_edges_from(cg.selfloop_edges())
    giant_set=set(nx.connected_components(cg)[0])
    to_remove=set(cg.nodes())-giant_set
    for dead in to_remove:
        cg.remove_node(dead)
    return cg





def stateful_test():
    g=nx.DiGraph()
    structure, transition_fusions, estimate=herd_sir()
    cg=contact_graph()
    association=lambda n: cg.neighbors(n)
    place_fuse=lambda a: False
    forest=tf.PetriForest(association, place_fuse, transition_fusions)
    translated=tf.NodeRelabel(g)
    for n in cg.nodes():
        forest.add_subnet(structure, n)
        forest.copy_subnet(n, translated)
    logger.debug('nodes {0} edges {1}'.format(
            len(g.nodes()), len(g.edges())))

    for f in g.nodes():
        print('{0}: {1}'.format(f, g.node[f]))
    logger.debug(forest.transition_fusion)
    n_cnt, e_cnt=estimate(len(cg.nodes()), len(cg.edges()))
    assert(n_cnt==len(g.nodes()))
    assert(e_cnt==len(g.edges()))



def breadth_test():
    g=nx.DiGraph()
    structure, transition_fusions, estimate=herd_sir()
    cg=contact_graph()
    association=lambda n: cg.neighbors(n)
    place_fuse=lambda a: False
    forest=tf.PetriForest(association, place_fuse, transition_fusions)
    translated=tf.NodeRelabel(g)
    edge_cnt=0
    white, grey, black, gandalf=range(4)
    for color, *item in tf.breadth_first_search(cg, cg.nodes()[0]):
        if len(item) is 1:
            node=item[0]
            if color is grey:
                forest.set_subnet(structure, node)
                forest.add_local_subnet(node, translated)
            elif color is black:
                forest.add_fused_subnet(node, translated)
            elif color is gandalf:
                forest.forget_subnet(node)
                translated.lose_labels(node)
            else:
                assert(False)
                raise RuntimeError('invariant broken nodes')
        elif len(item) is 2:
            edge=item
            if color is grey:
                forest.associate(edge[0], edge[1])
            elif color is black:
                pass
            else:
                assert(False)
                raise RuntimeError('invariant broken edges')
        else:
            assert(False)
            raise RuntimeError('more than three items')
        edge_cnt+=1
    logger.debug('nodes {0} edges {1}'.format(
            len(g.nodes()), len(g.edges())))
    logger.debug(forest.transition_fusion)

    translated.all_labels.sort()
    logger.debug('{0}'.format(', '.join(['{0}-{1},{2}'.format(
        x[0][0], x[0][1], x[1]) for x in translated.all_labels])))
    n_cnt, e_cnt=estimate(len(cg.nodes()), len(cg.edges()))
    assert(n_cnt==len(g.nodes()))
    assert(e_cnt==len(g.edges()))



def bfs():
    cg=contact_graph()
    white, grey, black, gandalf=range(4)
    node_color=dict()
    edge_color=dict()
    for color, *parent in tf.breadth_first_search(cg, 1):
        logger.debug('{0} {1}'.format(color, parent))
        if len(parent)==1:
            n=parent[0]
            if n in node_color:
                if node_color[n]==grey:
                    assert(color==black)
                elif node_color[n]==black:
                    assert(color==gandalf)
            else:
                assert(color==grey)
            node_color[n]=color
        if len(parent)==2:
            edge=tuple(sorted(parent))
            if edge in edge_color:
                assert(edge_color[edge]==grey)
                assert(color==black)
            else:
                assert(color==grey)
            edge_color[edge]=color



def generate_herd(contact_graph):

    g=nx.DiGraph()
    structure, transition_fusions, estimate=herd_sir()

    cg=contact_graph
    association=lambda n: cg.neighbors(n)
    place_fuse=lambda a: False
    forest=tf.PetriForest(association, place_fuse, transition_fusions)
    translated=tf.NodeRelabel(g)
    edge_cnt=0
    white, grey, black, gandalf=range(4)
    for color, *item in tf.breadth_first_search(cg, cg.nodes()[0]):
        if len(item) is 1:
            node=item[0]
            if color is grey:
                forest.set_subnet(structure, node)
                forest.add_local_subnet(node, translated)
            elif color is black:
                forest.add_fused_subnet(node, translated)
            elif color is gandalf:
                forest.forget_subnet(node)
                translated.lose_labels(node)
            else:
                assert(False)
                raise RuntimeError('invariant broken nodes')
        elif len(item) is 2:
            edge=item
            if color is grey:
                forest.associate(edge[0], edge[1])
            elif color is black:
                pass
            else:
                assert(False)
                raise RuntimeError('invariant broken edges')
        else:
            assert(False)
            raise RuntimeError('more than three items')
        edge_cnt+=1
    logger.debug('nodes {0} edges {1}'.format(
            len(g.nodes()), len(g.edges())))
    logger.debug(forest.transition_fusion)

    translated.all_labels.sort()
    logger.debug('{0}'.format(', '.join(['{0}-{1},{2}'.format(
        x[0][0], x[0][1], x[1]) for x in translated.all_labels])))
    n_cnt, e_cnt=estimate(len(cg.nodes()), len(cg.edges()))
    assert(n_cnt==len(g.nodes()))
    assert(e_cnt==len(g.edges()))
    return g, structure, translated.all_labels



def write_dot(filename, herd_size=10):
    '''
    dot -Tsvg -o blah.svg herd.dot

    For a single herd, the positions are, vertically,
    array([-138,  -69,    0,   69,  138]) for r, r0, i, i0, s.

    '''
    cg=erdos_renyi_giant(herd_size, 0.4)
    herd, single, translate=generate_herd(cg)
    label_single={ 1 : 's', 2 : 'i', 3 : 'r', 4 : 'i', 7 : 'i', 8 : 'r'}
    single_layout={ 1 : [0,1], 2 : [0,0], 3 : [0,-1], 4 :[0,.5], 8 : [0,-.5]}
    subgraphs=collections.defaultdict(list)
    attrs=dict()
    not_subgraph=list()
    for (cg_id, single_id), herd_id in translate:
        if single_id in single_layout:
            subgraphs[cg_id].append(herd_id)
        else:
            not_subgraph.append(herd_id)
        if single_id in [1,2,3]:
            shape='shape=ellipse, width=0.4, height=0.2'
        else:
            shape='shape=rect, width=0.5, height=0.3'
        if single_id in label_single:
            label='label={0}'.format(label_single[single_id])
        else:
            label='label={0}'.format(label_single[7])
        attrs[herd_id]=', '.join([shape, label])


    dotfile=open(filename, 'w')
    dotfile.write('digraph herd {'+os.linesep)
    for cg_node, subgraph_list in subgraphs.items():
        dotfile.write('subgraph cluster{0} {{'.format(cg_node))
        for sn in subgraph_list:
            dotfile.write('{0} [{1}]; '.format(sn, attrs[sn]))
        dotfile.write('};'+os.linesep)
    for other_node in not_subgraph:
        dotfile.write('{0} [{1}]; '.format(other_node, attrs[other_node]))
    for herd_edge in herd.edges():
        dotfile.write('{0} -> {1}; '.format(herd_edge[0], herd_edge[1]))
    dotfile.write('};'+os.linesep)





def write_dot_fixed(filename, herd_size=10, tol=0.1):
    '''
    dot -Tsvg -o blah.svg herd.dot

    For a single herd, the positions are, vertically,
    array([-138,  -69,    0,   69,  138]) for r, r0, i, i0, s.

    This is how I make the graphs:
    tft.write_dot_fixed('herd.dot', 100, 0.02)
    '''
    cg=spatial_contact_graph(herd_size, tol)
    cg2=erdos_renyi_giant(herd_size, 5/(herd_size*herd_size))
    for e in cg2.edges():
        if e not in cg.edges():
            logger.debug('adding long edge {0}'.format(e))
            cg.add_edge(e[0], e[1])
    cg_layout={ n : cg.node[n]['pos'] for n in cg.nodes()}
    main_scale=2
    scale_cg=4*main_scale*math.sqrt(herd_size)
    for k, v in cg_layout.items():
        cg_layout[k]=scale_cg*v
    herd, single, translate=generate_herd(cg)
    back_trans={herd_id : cg_single for cg_single, herd_id in translate}
    label_single={ 1 : 's', 2 : 'i', 3 : 'r', 4 : 'i', 7 : 'i', 8 : 'r'}
    single_layout={ 1 : [0,1], 2 : [0,0], 3 : [0,-1], 4 :[0,.5], 8 : [0,-.5]}
    for id in single_layout:
        single_layout[id]=np.array(single_layout[id])*main_scale
    subgraphs=collections.defaultdict(list)
    attrs=dict()
    not_subgraph=list()
    positions=dict()
    fixed=list()
    for (cg_id, single_id), herd_id in translate:
        attribs=list()
        if single_id in single_layout:
            subgraphs[cg_id].append(herd_id)
            attribs.append('pin=true')
            positions[herd_id]=cg_layout[cg_id]+single_layout[single_id]
            x, y=positions[herd_id]
            attribs.append('pos="{0},{1}"'.format(x, y))
            fixed.append(herd_id)
            logger.debug('pos {0} {1}'.format(cg_id, (x,y)))
        else:
            not_subgraph.append(herd_id)
        if single_id in [1,2,3]:
            attribs.append('shape=ellipse, width=0.4, height=0.2')
        else:
            attribs.append('shape=rect, width=0.5, height=0.3')
        if single_id in label_single:
            attribs.append('label={0}'.format(label_single[single_id]))
        else:
            attribs.append('label={0}'.format(label_single[7]))
        attrs[herd_id]=', '.join(attribs)

    for (cg_id, single_id), herd_id in translate:
        if single_id not in single_layout:
            neigh=list()
            cg_ids=set()
            for ie in herd.in_edges(herd_id):
                neigh.append(positions[ie[0]])
                cg_ids.add(back_trans[ie[0]][0])
            for oe in herd.out_edges(herd_id):
                neigh.append(positions[oe[1]])
                cg_ids.add(back_trans[oe[1]][0])
            logger.debug('cg_ids {0}'.format(cg_ids))
            assert(len(cg_ids)==2)
            assert((min(cg_ids),max(cg_ids)) in cg.edges())
            positions[herd_id]=np.average(np.vstack(neigh), axis=0)

    layout=herd_plot.fruchterman_reingold_layout(
            herd, pos=positions, fixed=fixed, k=0.2, temp=0.1
            )

    for k, loc in layout.items():
        a,b=loc!=positions[k]
        if math.sqrt(a*a+b*b)>0.01:
            logger.debug('{0} from {1} to {2}'.format(k, positions[k], loc))

    for (cg_id, single_id), herd_id in translate:
        if single_id not in single_layout:
            attrs[herd_id]=attrs[herd_id]+', pos="{0},{1}", pin=true'.format(
                layout[herd_id][0], layout[herd_id][1])
            logger.debug('({0},{1}) {2}: {3}'.format(cg_id, single_id,
                    herd_id, positions[herd_id]))

    dotfile=open(filename, 'w')
    dotfile.write('digraph herd {'+os.linesep)
    for cg_node, subgraph_list in subgraphs.items():
        dotfile.write('subgraph cluster{0} {{'.format(cg_node))
        for sn in subgraph_list:
            dotfile.write('{0} [{1}]; '.format(sn, attrs[sn]))
        dotfile.write('};'+os.linesep)
    for other_node in not_subgraph:
        dotfile.write('{0} [{1}]; '.format(other_node, attrs[other_node]))
    for herd_edge in herd.edges():
        dotfile.write('{0} -> {1}; '.format(herd_edge[0], herd_edge[1]))
    dotfile.write('};'+os.linesep)





def plot_complicated():
    contact_graph=erdos_renyi_giant(10, 0.4)
    herd, single, translate=generate_herd(contact_graph)
    import herd_plot

    cg_layout=nx.spring_layout(contact_graph, scale=1)
    single_layout=herd_plot.fruchterman_reingold_layout(
            single, scale=1/len(contact_graph))
    single_layout={ 1 : [0,1], 2 : [0,0], 3 : [0,-1], 4 :[0,.5], 8 : [0,-.5]}
    single_scale=1/math.sqrt(len(contact_graph))
    single_layout={ k : np.array(v)*single_scale for k,v in single_layout.items()}
    positions=dict()
    fixed=list()
    color=[0]*len(herd)
    for (cg_id, single_id), herd_id in translate:
        if single_id in single_layout:
            pos=cg_layout[cg_id]+single_layout[single_id]
            logger.debug('({0},{1}) {2}: {3}'.format(cg_id, single_id,
                    herd_id, pos))
            positions[herd_id]=pos
            fixed.append(herd_id)
        if single_id in (1,2,3):
            color[herd_id]='b'
        else:
            color[herd_id]='r'
    for (cg_id, single_id), herd_id in translate:
        if single_id not in single_layout:
            neigh=list()
            for ie in herd.in_edges(herd_id):
                neigh.append(positions[ie[0]])
            for oe in herd.out_edges(herd_id):
                neigh.append(positions[oe[1]])
            positions[herd_id]=np.average(np.vstack(neigh), axis=0)
            logger.debug('({0},{1}) {2}: {3}'.format(cg_id, single_id,
                    herd_id, positions[herd_id]))

    logger.debug('specified {0} positions of {1}'.format(
            len(positions), len(herd)))
    logger.debug('fixed {0}'.format(fixed))
    layout=herd_plot.fruchterman_reingold_layout(
        herd, pos=positions, fixed=fixed, scale=1.1
        )

    for k, loc in layout.items():
        a,b=loc!=positions[k]
        if math.sqrt(a*a+b*b)>0.01:
            logger.debug('{0} from {1} to {2}'.format(k, positions[k], loc))

    out_file='layout.json'
    serialize_layout=dict()
    for k, v in layout.items():
        serialize_layout[k]=(v[0], v[1])
    if os.path.exists(out_file):
        os.remove(out_file)
    json.dump((serialize_layout,color), open(out_file, 'w'))





def writing_test():
    herd, contact_graph, single, translate=generate_herd()
    nx.write_graphml(herd, 'herd.gml')



def suite():
    import unittest
    suite=unittest.TestSuite()
    suite.addTest(unittest.FunctionTestCase(stateful_test))
    suite.addTest(unittest.FunctionTestCase(breadth_test))
    suite.addTest(unittest.FunctionTestCase(bfs))
    suite.addTest(unittest.FunctionTestCase(writing_test))
    return suite




if __name__=='__main__':
    parser=argparse.ArgumentParser(description='Run stuff')
    parser.add_argument('-v', '--verbose', dest='verbose', action='store_true',
            default=False, help='print debug messages')
    parser.add_argument('-q', '--quiet', dest='quiet', action='store_true',
            default=False, help='print only exceptions')
    parser.add_argument('--test', help='run unit tests', action='store_true')
    parser.add_argument('--plot', help='plot herds', action='store_true')

    args=parser.parse_args()
    if args.verbose:
        log_level=logging.DEBUG
    elif args.quiet:
        log_level=logging.ERROR
    else:
        log_level=logging.INFO
    logging.basicConfig(level=log_level)

    if args.test:
        import unittest
        unittest.TextTestRunner(verbosity=2).run(suite())
        sys.exit(0)

    if args.plot:
        plot_complicated()
