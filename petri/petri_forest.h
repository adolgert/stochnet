#ifndef _PETRI_FOREST_H_
#define _PETRI_FOREST_H_ 1

#include <map>
#include <set>
#include <tuple>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "petri_graph.h"


namespace detail
{
  template<typename ForestKey>
  struct SingleFusion
  {
    std::vector<ForestKey> transitions;
    size_t propensity;
    ForestKey assigned;

    SingleFusion()=default;
    SingleFusion(std::vector<ForestKey> t, size_t p, ForestKey a)
      : transitions(t), propensity(p), assigned(a)
    {
      // Ensure the subnet ids are different.
      assert(get<0>(t[0])!=get<0>(t[1]));
    }
    friend std::ostream& operator<<(std::ostream& os,
      const SingleFusion<ForestKey>& sf)
    {
      os<<"SingleFusion(";
      for (const auto& t : sf.transitions)
      {
        os<<"("<<std::get<0>(t)<<","<<std::get<1>(t)<<")";
      }
      os<<","<<sf.propensity<<",("<<std::get<0>(sf.assigned)<<","
        <<std::get<1>(sf.assigned)<<"))";
      return os;
    }
  };
}


namespace afidd
{
  /*! Each subnet in a Forest has an SID. This creates a new space
   *  for vertex descriptors, which is (SID, subnet_vertex_descriptor).
   */
  template<typename Subnet>
  class PetriForest
  {
  public:
    using TransitionRule=std::map<std::pair<size_t,size_t>,size_t>;
    using SID=size_t; // Subnet ID.
    using vertex_descriptor=
        typename boost::graph_traits<Subnet>::vertex_descriptor;
    using ForestKey=std::tuple<SID,vertex_descriptor>;

  private:
    std::map<SID,Subnet> _subnet;
    TransitionRule _transition_fuse_rule;
    std::set<size_t> _can_fuse;
    std::map<SID,size_t> _subnet_extra_transitions;
    std::map<SID,std::vector<detail::SingleFusion<ForestKey>>> _fusions;

  public:
    PetriForest(TransitionRule transition_rule)
      : _transition_fuse_rule(transition_rule)
      {
        for (const auto& rule : transition_rule)
        {
          _can_fuse.insert(rule.first.first);
          _can_fuse.insert(rule.first.second);
          BOOST_LOG_TRIVIAL(debug) << "fusable " << rule.first.first
              << ", " << rule.first.second;
        }
      }
    ~PetriForest() {}


    void add_subnet(const Subnet& subnet, SID sid)
    {
      _subnet.emplace(std::make_pair(sid, subnet));
      // Assumes subnet vertex_descriptor is integer with no missing values.
      _subnet_extra_transitions[sid]=num_vertices(subnet);
      BOOST_LOG_TRIVIAL(debug) << "add_subnet "<<sid<<" verts start at "
        << num_vertices(subnet);
    }


    /*! Apply the transition rule to two subnets, creating fused transitions.
     *  These fused transitions are explicitly-specified in the key-space
     *  of the forest, meaning they have a (subnet,vertex_descriptor) pair.
     */
    void associate(SID s0, SID s1)
    {
      BOOST_LOG_TRIVIAL(debug) << "associate("<<s0<<", "<<s1<<")";
      const Subnet& sn0=_subnet.at(s0);
      const Subnet& sn1=_subnet.at(s1);

      using VertIter=typename boost::graph_traits<Subnet>::vertex_iterator;
      VertIter b0, e0, b1, e1;
      std::tie(b0, e0)=vertices(sn0);
      for (; b0!=e0; ++b0)
      {
        ForestKey key0=std::make_tuple(s0, *b0);
        std::tie(b1, e1)=vertices(sn1);
        for (; b1!=e1; ++b1)
        {
          ForestKey key1=std::make_tuple(s1, *b1);
          std::vector<std::vector<ForestKey>> ordered
              {{key0, key1}, {key1, key0}};

          ForestKey left, right;
          for (auto& trans_list : ordered)
          {
            left=trans_list[0];
            right=trans_list[1];
            size_t l_prop=_subnet.at(std::get<0>(left))
                [std::get<1>(left)].propensity;
            size_t r_prop=_subnet.at(std::get<0>(right))
                [std::get<1>(right)].propensity;
            auto search_pair=std::make_pair(l_prop, r_prop);
            auto assign=_transition_fuse_rule.find(search_pair);
            if (assign!=_transition_fuse_rule.end())
            {
              size_t new_propensity=assign->second;
              SID put_in_subnet=std::get<0>(left);
              auto vert_desc=_subnet_extra_transitions[put_in_subnet]++;
              ForestKey new_transition=std::make_tuple(put_in_subnet, vert_desc);
              detail::SingleFusion<ForestKey>
                  sf(trans_list, new_propensity, new_transition);
              _fusions[put_in_subnet].push_back(sf);
              BOOST_LOG_TRIVIAL(debug) <<sf;
            }
          }
        }
      }
      BOOST_LOG_TRIVIAL(debug) << "~associate("<<s0<<", "<<s1<<")";
    }


    /*! Add to the sink graph just the parts of the subnet that are real.
     *  By real, I mean those that are not transitions that need to be
     *  fused.
     */
    void add_local_subnet(SID sid, Subnet& sink,
        std::map<ForestKey,vertex_descriptor>& translate)
    {
      BOOST_LOG_TRIVIAL(debug) << "add_local() "<<sid;

      const auto& sn=_subnet.at(sid);
      using VertIter=typename boost::graph_traits<Subnet>::vertex_iterator;
      ForestKey cp_key;
      std::get<0>(cp_key)=sid;
      VertIter vb, ve;
      std::tie(vb, ve)=vertices(sn);
      for (; vb!=ve; ++vb)
      {
        auto found=_can_fuse.find(sn[*vb].propensity);
        if (found==_can_fuse.end())
        {
          std::get<1>(cp_key)=*vb;
          translate[cp_key]=add_vertex(sn[*vb], sink);
        }
      }

      using EdgeIter=typename boost::graph_traits<Subnet>::edge_iterator;
      using Edge=typename boost::graph_traits<Subnet>::edge_descriptor;
      Edge new_edge;
      bool added;
      ForestKey head_key{cp_key}, tail_key{cp_key};

      EdgeIter eb, ee;
      std::tie(eb, ee)=edges(sn);
      for (; eb!=ee; ++eb)
      {
        auto head_vert=source(*eb, sn);
        auto tail_vert=target(*eb, sn);
        BOOST_LOG_TRIVIAL(debug) << "add_local h "<<head_vert<<" "<<tail_vert;
        bool head=_can_fuse.find(sn[head_vert].propensity)==_can_fuse.end();
        bool tail=_can_fuse.find(sn[tail_vert].propensity)==_can_fuse.end();

        if (head && tail)
        {
          std::get<1>(head_key)=head_vert;
          std::get<1>(tail_key)=tail_vert;
          std::tie(new_edge, added)=
            add_edge(translate[head_key], translate[tail_key], sn[*eb], sink);
          assert(added);
          if (!added)
          {
            throw std::runtime_error("Could not add edge to sink graph.");
          }
        }
      }
      BOOST_LOG_TRIVIAL(debug) << "~add_local() "<<sid;
    }


    /*! Add fusable transitions of this subnet to the final Petri net.
     *  This assumes that the literal parts of the subnet were already
     *  added to the final net.
     */
    void add_fusions_of_subnet(SID sid, Subnet& sink,
        std::map<ForestKey,vertex_descriptor>& translate)
    {
      BOOST_LOG_TRIVIAL(debug) << "add_fusions_of_subnet() "<<sid;
      using InEdgeIter=typename boost::graph_traits<Subnet>::in_edge_iterator;
      using OutEdgeIter=typename boost::graph_traits<Subnet>::out_edge_iterator;

      const auto& subnet_fusions=_fusions.find(sid);
      if (subnet_fusions!=_fusions.end())
      {
        for (auto fusion : subnet_fusions->second)
        {
          auto ftrans=add_vertex(sink);
          sink[ftrans].propensity=fusion.propensity;
          sink[ftrans].color=PetriGraphColor::Transition;
          translate[fusion.assigned]=ftrans;

          for (auto forest_key : fusion.transitions)
          {
            auto sn_id=std::get<0>(forest_key);
            auto& sn=_subnet.at(sn_id);
            InEdgeIter ib, ie;
            std::tie(ib, ie)=in_edges(std::get<1>(forest_key), sn);
            for (; ib!=ie; ++ib)
            {
              auto source_in_subnet=source(*ib, sn);
              auto sink_source=translate[ForestKey(sn_id, source_in_subnet)];
              add_edge(sink_source, ftrans, sn[*ib], sink);
            }

            OutEdgeIter ob, oe;
            std::tie(ob, oe)=out_edges(std::get<1>(forest_key), sn);
            for (; ob!=oe; ++ob)
            {
              auto target_in_subnet=target(*ob, sn);
              auto sink_target=translate[ForestKey(sn_id, target_in_subnet)];
              add_edge(ftrans, sink_target, sn[*ob], sink);
            }
          }
        }
      }
      else
      {
        BOOST_LOG_TRIVIAL(debug) << "add_fusions_of_subnet() "<<sid
          << " not associated with any other subnet";
      }
      BOOST_LOG_TRIVIAL(debug) << "~add_fusions_of_subnet() "<<sid;
    }
  };

}

#endif // _PETRI_FOREST_H_
