#ifndef _SIR_STRUCTURE_H_
#define _SIR_STRUCTURE_H_ 1

#include <memory>
#include <type_traits>
#include "boost/range.hpp"
#include "boost/range/adaptor/map.hpp"


namespace afidd
{


template<typename StateType,typename TransitionType>
class SIRTransitionRange
{
  std::unique_ptr<TransitionType> _pt;
  size_t _idx;
public:
  using value_type=TransitionType;
  using reference_type=value_type&;
  using difference_type=size_t;

  SIRTransitionRange(StateType state)
  : _idx{0}
  {
    _pt=new TransitionType{};
  }

  ~SIRTransitionRange()
  {
  }

  SIRTransitionRange(SIRTransitionRange&) = delete;
  SIRTransitionRange& operator=(const SIRTransitionRange&) = delete;

  SIRTransitionRange(SIRTransitionRange&& other)
  : _idx{other._idx}
  {
    _pt=std::move(other._pt);
  }

  SIRTransitionRange& operator=(SIRTransitionRange&& other)
  {
    if (this!=other)
    {
      _pt=std::move(other._pt);
      _idx=other._idx;
    }
    return *this;
  }

  reference_type front() const
  {
    return *_pt;
  }

  bool empty() const
  {
    return _idx>0;
  }

  SIRTransitionRange& advance_begin(difference_type n)
  {
    ++_idx;
    return *this;
  }
};


enum class SIRState : char { unused, s, i, r, s0, i0, r0, s1, i1, r1};
enum class SIRStateKind : char { unused, s, i, r };
enum class SIRTransition : char { unused, infect0, infect1, recover0 };
enum class SIRTransitionKind : char { unused, infect, recover };

class SIRStructure
{
  std::vector<SIRState> _states { SIRState::s0, SIRState::i0, SIRState::r0 };
  using kind_map=std::map<SIRTransitionKind,std::vector<SIRTransition>>;
  kind_map _kinds;
  std::map<SIRState,std::vector<SIRTransition>> _transitions;
  std::map<SIRTransition,std::vector<SIRState>> _in_transitions;
  std::map<SIRTransition,std::vector<SIRState>> _out_transitions;

public:
  using place_descriptor=SIRState;
  using transition_descriptor=SIRTransition;
  using transition_range=boost::iterator_range<
      std::vector<SIRTransition>::const_iterator>;
  using in_place_range=boost::iterator_range<
      std::vector<SIRState>::const_iterator>;
  using out_place_range=boost::iterator_range<
      std::vector<SIRState>::const_iterator>;

  using transition_kind=SIRTransitionKind;
  using kind_range=boost::range_detail::select_first_range<kind_map>;


  SIRStructure()
  {
    _transitions[SIRState::s]={};
    _transitions[SIRState::i]={SIRTransition::recover0};
    _transitions[SIRState::r]={};
    _in_transitions[SIRTransition::recover0]={SIRState::i0};
    _out_transitions[SIRTransition::recover0]={SIRState::r0};
    _kinds[SIRTransitionKind::infect]={ };
    _kinds[SIRTransitionKind::recover]={ SIRTransition::recover0 };
  }


  transition_range out_transitions(place_descriptor place) const
  {
    const auto& tr=_transitions.at(place);
    return transition_range(tr.begin(), tr.end());
  }


  in_place_range in_places(transition_descriptor transition) const
  {
    const auto& pr=_in_transitions.at(transition);
    return in_place_range(pr.begin(), pr.end());
  }


  out_place_range out_places(transition_descriptor transition) const
  {
    const auto& pr=_out_transitions.at(transition);
    return out_place_range(pr.begin(), pr.end());
  }


  kind_range kinds() const
  {
    return _kinds | boost::adaptors::map_keys;
  }


  transition_range kind(SIRTransitionKind tk) const
  {
    const auto& k=_kinds.at(tk);
    return transition_range(k.begin(), k.end());
  }
};





class SIRInfectStructure
{
  std::vector<SIRState> _states { SIRState::s0, SIRState::i0, SIRState::i1 };
  using kind_map=std::map<SIRTransitionKind,std::vector<SIRTransition>>;
  kind_map _kinds;
  std::map<SIRState,std::vector<SIRTransition>> _transitions;
  std::map<SIRTransition,std::vector<SIRState>> _in_transitions;
  std::map<SIRTransition,std::vector<SIRState>> _out_transitions;

public:
  using place_descriptor=SIRState;
  using transition_descriptor=SIRTransition;
  using transition_range=boost::iterator_range<
      std::vector<SIRTransition>::const_iterator>;
  using in_place_range=boost::iterator_range<
      std::vector<SIRState>::const_iterator>;
  using out_place_range=boost::iterator_range<
      std::vector<SIRState>::const_iterator>;

  using transition_kind=SIRTransitionKind;
  using kind_range=boost::range_detail::select_first_range<kind_map>;


  SIRInfectStructure()
  {
    _transitions[SIRState::s0]={SIRTransition::infect0};
    _transitions[SIRState::i1]={SIRTransition::infect0, SIRTransition::infect1};
    _transitions[SIRState::i0]={SIRTransition::infect0, SIRTransition::infect1};
    _transitions[SIRState::s1]={SIRTransition::infect1};
    _in_transitions[SIRTransition::infect0]={SIRState::s0, SIRState::i1};
    _out_transitions[SIRTransition::infect0]={SIRState::i0, SIRState::i1};
    _in_transitions[SIRTransition::infect1]={SIRState::s1, SIRState::i0};
    _out_transitions[SIRTransition::infect1]={SIRState::i0, SIRState::i1};
    _kinds[SIRTransitionKind::infect]={ SIRTransition::infect0,
        SIRTransition::infect1 };
    _kinds[SIRTransitionKind::recover]={ };
  }


  transition_range out_transitions(place_descriptor place) const
  {
    const auto& pr=_transitions.at(place);
    return transition_range(pr.begin(), pr.end());
  }


  in_place_range in_places(transition_descriptor transition) const
  {
    const auto& tr=_in_transitions.at(transition);
    return in_place_range(tr.begin(), tr.end());
  }


  out_place_range out_places(transition_descriptor transition) const
  {
    const auto& tr=_out_transitions.at(transition);
    return out_place_range(tr.begin(), tr.end());
  }


  kind_range kinds() const
  {
    return _kinds | boost::adaptors::map_keys;
  }


  transition_range kind(SIRTransitionKind tk) const
  {
    const auto& k=_kinds.at(tk);
    return transition_range(k.begin(), k.end());
  }
};


/*
full fuse(full place)
{
  left, right=split(place);

  if (left==s)
  {
    for (edge_idx : coupled_contacts(right))
    {
      {place, {s0,place_cnt+edge_idx}};
    }
  }
  else if (left==i)
  {
    for (edge_idx, orient : coupled_contacts(right))
    {
      if (orient)
      {
        {place, {i0, place_cnt+edge_idx}};
      }
      else
      {
        {place, {i1, place_cnt+edge_idx}};
      }
    }
  }
  else if (left==r)
  {
    return { {place} };
  }
}


subnet_of(full_place)
{
  left,right=split(place);
  if (right<place_cnt)
  {
    return embed_net(SIRStructure, right);
  }
  else
  {
    return embed_net(SIRInfectStructure, right);
  }
}
*/

}

#endif
