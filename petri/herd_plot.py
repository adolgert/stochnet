import json
import networkx as nx
import networkx.drawing as draw
import logging
logger=logging.getLogger(__file__)
try:
    import matplotlib.pyplot as plt
    import matplotlib
except ImportError:
    logger.warn('Cannot plot without matplotlib')



def plot(G, input_layout=None, choose_color=None):
    if not input_layout:
        input_layout=nx.spring_layout(G)
    else:
        print('using input layout')
    nl=G.nodes()
    nl.sort()
    print(','.join([str(x) for x in nl]))
    print(','.join(choose_color))
    color_to_shape={'b' : 'c', 'r' : 's'}
    shape=[color_to_shape[x] for x in choose_color]
    print(','.join(shape))
    nx.draw_networkx(G, pos=input_layout, nodelist=nl,
            node_color=choose_color)
    plt.draw()
    plt.show()


def plot_name(filename, layout_name):
    (layout,color)=json.load(open(layout_name, 'r'))
    lay_int=dict()
    for k,v in layout.items():
        lay_int[int(k)]=v
    herd=nx.read_graphml('herd.gml', node_type=int)
    plot(herd, lay_int, color)



def _rescale_layout(pos,scale=1):
    # rescale to (0,pscale) in all axes

    # shift origin to (0,0)
    lim=0 # max coordinate for all axes
    for i in range(pos.shape[1]):
        pos[:,i]-=pos[:,i].min()
        lim=max(pos[:,i].max(),lim)
    # rescale to (0,scale) in all directions, preserves aspect
    for i in range(pos.shape[1]):
        pos[:,i]*=scale/lim
    return pos


def fruchterman_reingold_layout(G,dim=2,
                                pos=None,
                                fixed=None,
                                iterations=50,
                                weight='weight',
                                scale=1,
                                k=0.5,
                                temp=0.1):
    try:
        import numpy as np
    except ImportError:
        raise ImportError("fruchterman_reingold_layout() requires numpy: http://scipy.org/ ")
    if fixed is not None:
        nfixed=dict(zip(G,range(len(G))))
        fixed=np.asarray([nfixed[v] for v in fixed])
        logger.debug('fruchterman: fixed {0}'.format(
            ','.join([str(x) for x in fixed])))

    if pos is not None:
        logger.debug('fruchterman: pos')
        pos_arr=np.asarray(np.random.random((len(G),dim)))
        for i,n in enumerate(G):
            if n in pos:
                pos_arr[i]=np.asarray(pos[n])
            else:
                logger.debug('position not specified for {0}'.format(n))
    else:
        pos_arr=None

    if len(G)==0:
        return {}
    if len(G)==1:
        return {G.nodes()[0]:(1,)*dim}

    if len(G)<500:
        A=nx.to_numpy_matrix(G,weight=weight)
        pos=_fruchterman_reingold(A,dim,pos_arr,fixed,iterations,k,temp)
    else:
        try:
            # Sparse matrix
            A=nx.to_scipy_sparse_matrix(G,weight=weight)
            pos=_sparse_fruchterman_reingold(A,dim,pos_arr,fixed,iterations)
        except:
            A=nx.to_numpy_matrix(G,weight=weight)
            pos=_fruchterman_reingold(A,dim,pos_arr,fixed,iterations)
    if fixed is None:
        logger.debug('rescaling')
        pos=_rescale_layout(pos,scale=scale)
    return dict(zip(G,pos))


def _fruchterman_reingold(A, dim=2, pos=None, fixed=None, iterations=5000,
    k=0.5, t=0.1):
    # Position nodes in adjacency matrix A using Fruchterman-Reingold
    # Entry point for NetworkX graph is fruchterman_reingold_layout()
    try:
        import numpy as np
    except ImportError:
        raise ImportError("_fruchterman_reingold() requires numpy: http://scipy.org/ ")

    try:
        nnodes,_=A.shape
    except AttributeError:
        raise nx.NetworkXError(
            "fruchterman_reingold() takes an adjacency matrix as input")
    logger.debug('k {0} temp {1}'.format(k, t))

    A=np.asarray(A) # make sure we have an array instead of a matrix

    if pos==None:
        # random initial positions
        pos=np.asarray(np.random.random((nnodes,dim)),dtype=A.dtype)
    else:
        logger.debug('Found initial positions')
        # make sure positions are of same type as matrix
        pos=pos.astype(A.dtype)

    # optimal distance between nodes
    #k=0.05 #0.02*np.sqrt(1.0/nnodes)
    # the initial "temperature"  is about .1 of domain area (=1x1)
    # this is the largest step allowed in the dynamics.
    #t=0.004
    # simple cooling scheme.
    # linearly step down by dt on each iteration so last iteration is size dt.
    dt=t/float(iterations+1)
    delta = np.zeros((pos.shape[0],pos.shape[0],pos.shape[1]),dtype=A.dtype)
    # the inscrutable (but fast) version
    # this is still O(V^2)
    # could use multilevel methods to speed this up significantly
    for iteration in range(iterations):
        # matrix of difference between points
        for i in range(pos.shape[1]):
            delta[:,:,i]= pos[:,i,None]-pos[:,i]
        # distance between points
        distance=np.sqrt((delta**2).sum(axis=-1))
        # enforce minimum distance of 0.01
        distance=np.where(distance<0.001,0.001,distance)
        # displacement "force"
        displacement=np.transpose(np.transpose(delta)*\
                                  (k*k/distance**2-A*distance/k))\
                                  .sum(axis=1)
        # update positions
        length=np.sqrt((displacement**2).sum(axis=1))
        length=np.where(length<0.01,0.1,length)
        delta_pos=np.transpose(np.transpose(displacement)*t/length)
        if fixed is not None:
            # don't change positions of fixed nodes
            delta_pos[fixed]=0.0
        pos+=delta_pos
        # cool temperature
        t-=dt

    return pos


if __name__ == '__main__':
    plot_name('herd.gml', 'layout.json')
