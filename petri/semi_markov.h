#ifndef _SEMI_MARKOV_H_
#define _SEMI_MARKOV_H_ 1


#include <map>
#include "boost/math/distributions/exponential.hpp"


namespace afidd
{


/*! An abstract base class for distributions, so that we can store
 *  different distributions in the same container.
 */
class Distribution
{
public:
  virtual double quantile(double p)=0;
};




class ExponentialDistribution : Distribution
{
  boost::math::exponential_distribution<double> _distribution;
public:
  ExponentialDistribution(double lambda)
  : _distribution(lambda)
  {
  }

  ~ExponentialDistribution() {}


  virtual quantile(double p) const
  {
    return quantile(_distribution, p);
  }

};




class CompoundSemiMarkovTransition
{
  Distribution _distribution;
  double _enabling_time;
  size_t _id;

public:
  Transition() {}
  ~Transition() {}

  size_t id() const { return _id; }
  double enabling_time() const { return _enabling_time; }
  Distribution& distribution() { return _distribution; }
  const Distribution& distribution() const { return _distribution; }
};




class GSPNRepresentation
{
public:
};




template<typename Representation>
class CompoundSemiMarkov
{
  double _absolute_time;
  std::map<Transition> _transition;
  Representation _repr;
  std::vector<size_t> _affected_substates;

public:
  CompoundSemiMarkov(Representation representation)
  : _repr(representation)
  {}


  ~CompoundSemiMarkov() {}


  template<typename Functor>
  void on_transitions(Functor functor)
  {
    for (const auto& transition : _transition)
    {
      time_since_enabling=_absolute_time-transition.enabling_time();
      if (_repr.enabled(transition.id()))
      {
        functor(transition.id(), time_since_enabling,
            transition.distribution());
      }
    }
  }


  void fire(size_t transition_id, double time_shift)
  {
    _absolute_time+=time_shift;
    _repr.fire(transition_id);
  }
};


}


#endif // _SEMI_MARKOV_H_
