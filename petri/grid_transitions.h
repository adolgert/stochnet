#ifndef _GRID_TRANSITIONS_H_
#define _GRID_TRANSITIONS_H_ 1


#include <array>
#include <memory>
#include <initializer_list>
#include <algorithm>


namespace afidd
{


/*! An array of two indices to show a position on a 2d grid.
 *  This class provides a POD type with a print function and
 *  a get<n>() function.
 */
template<typename IndexType>
struct GridPlace
{
  using value_type=IndexType;
  std::array<IndexType,2> value;

  friend std::ostream& operator<<(std::ostream& os, const GridPlace& gp)
  {
    return os << std::get<0>(gp.value) << " " << std::get<1>(gp.value);
  }
};



template<int which,typename IndexType>
IndexType&
get(GridPlace<IndexType>& gp)
{
  return std::get<which>(gp.value);
}


//! An array of two indices to show a transition from one place to another.
template<typename IndexType>
struct GridTransitionPlace
{
  using value_type=IndexType;
  std::array<IndexType,2> value;

  GridTransitionPlace() = default;

  friend std::ostream& operator<<(std::ostream& os,
      const GridTransitionPlace& tp)
  {
    return os << get<0>(tp.value) << " "
        << get<1>(tp.value);
  }
};




template<int which,typename GridPlace>
GridPlace&
get(GridTransitionPlace<GridPlace>& gp)
{
  return std::get<which>(gp.value);
}



template<typename PlaceType>
class GridFourNeighborTransitionRange
{
  PlaceType _place;
  std::unique_ptr<GridTransitionPlace<PlaceType>> _transition;
  int _idx;
public:
  using value_type=GridTransitionPlace<PlaceType>;
  using reference_type=value_type&;
  using difference_type=size_t;

  GridFourNeighborTransitionRange() {}
  GridFourNeighborTransitionRange(PlaceType place)
  : _place(place), _idx{0}
  {
    advance_begin(0);
  }
  GridFourNeighborTransitionRange(GridFourNeighborTransitionRange&& gfn)
  : _place{}, _idx{0}
  {
    _place=gfn._place;
    _idx=gfn._idx;
    _transition=std::move(gfn._transition);
  }
  GridFourNeighborTransitionRange&
  operator=(GridFourNeighborTransitionRange&& other)
  {
    if (this!=other)
    {
      _place=other._place;
      _idx=other._idx;
      _transition=std::move(other._transition);
    }
    return *this;
  }


  reference_type front() const { return *_transition; }
  bool empty() const { return _idx>3; }
  GridFourNeighborTransitionRange& advance_begin(difference_type n)
  {
    constexpr int xdeltas[]={ 1, 0, -1, 0};
    constexpr int ydeltas[]={ 0, 1, 0, -1};
    _idx+=n;
    if (_idx>0 && _idx<4)
    {
      int dx=xdeltas[_idx];
      int dy=ydeltas[_idx];
      auto& from=get<0>(*_transition);
      from=_place;
      auto& to=get<0>(*_transition);
      get<0>(to)=get<0>(_place)+dx;
      get<1>(to)=get<1>(_place)+dy;
    }
    return *this;
  }
};



template<typename TransitionType>
class GridInPlaceRange
{
public:
  using value_type=typename TransitionType::value_type;
  using reference_type=value_type&;
  using difference_type=size_t;

private:
  std::unique_ptr<value_type> _place;
  int _idx;

public:
  GridInPlaceRange() {}
  GridInPlaceRange(TransitionType transition)
  : _idx{0}
  {
    *_place=get<0>(transition);
  }

  reference_type front() const
  {
    return *_place;
  }

  bool empty() const { return _idx!=0; }

  GridInPlaceRange& advance_begin(difference_type n)
  {
    _idx+=n;
    return *this;
  }
};




template<typename TransitionType>
class GridOutPlaceRange
{
public:
  using value_type=typename TransitionType::value_type;
  using reference_type=value_type&;
  using difference_type=size_t;

private:
  std::unique_ptr<value_type> _place;
  int _idx;

public:
  GridOutPlaceRange() {}
  GridOutPlaceRange(TransitionType transition)
  : _idx{0}
  {
    *_place=get<1>(transition);
  }

  reference_type front() const
  {
    return *_place;
  }

  bool empty() const { return _idx!=0; }

  GridOutPlaceRange& advance_begin(difference_type n)
  {
    _idx+=n;
    return *this;
  }
};



//! A petri structure for transitions on an infinite plane.
class GridTransitions
{
public:
  using place_descriptor=GridPlace<size_t>;
  using transition_descriptor=GridTransitionPlace<place_descriptor>;
  using transition_range=GridFourNeighborTransitionRange<place_descriptor>;
  using in_place_range=GridInPlaceRange<transition_descriptor>;
  using out_place_range=GridOutPlaceRange<transition_descriptor>;


  transition_range
  out_transitions(place_descriptor place) const
  {
    return transition_range(place);
  }


  in_place_range
  in_places(transition_descriptor transition) const
  {
    return in_place_range(transition);
  }


  out_place_range
  out_places(transition_descriptor transition) const
  {
    return out_place_range(transition);
  }
};



}


#endif
