#ifndef _PLACE_FUSION_H_
#define _PLACE_FUSION_H_ 1

namespace afidd
{
	

/*! Presents a set of petri nets and a fusion list as a single petri structure.
 *  PetriCollection is a set of petri nets.
 *  Fusion has a place as key and a set of places as the value.
 */
template<typename PetriCollection,typename Fusion>
class ExplicitPlaceFusion
{
public:
  typedef typename PetriStructure::place_descriptor place_descriptor;
  typedef typename PetriStructure::transition_descriptor transition_descriptor;

  typedef typename Petri::in_place_range in_place_range;
  typedef typename Petri::out_place_range out_place_range;
  typedef typename Petri::transition_range transition_range;

private:
  const PetriCollection& _subnet;
  const Fusion& _fusion;

public:
  ExplicitPlaceFusion(const std::vector<PetriStructure>& subnet,
    const Fusion& fusion)
  : _subnet(subnet), _fusion(fusion)
  {}


  transition_range
  out_transitions(place_descriptor place) const
  {
    // Find all places that are fused to this place (including the original)
    const auto& fused_places=_fusion(place);
    const auto& fused_places=get(place, _fusion);

    // For each place, return the out_transitions of its subnet.
    // subnets.find(place) -> subnet -> out_transitions(place)
    for (const auto& transition : )

    for (const auto& home_net : _subnet)
    {
      for (const auto& fused_place : fused_places)
      {
        if (home_net.has_place(place))
        {
          // it has out transitions
        }
      }
    }
  }


  in_place_range
  in_places(transition_descriptor transition) const
  {
    //subnets.find(transition)->subnet -> in_places(transition)
    subnet=subnet_collection.find(transition);
    subnet=get(transition, subnet_collection_transitions);
    return in_place_range(subnet, transition);
  }


  out_place_range
  out_places(transition_descriptor transition) const
  {
    return out_place_range(transition);
  }
};



}



#endif 

