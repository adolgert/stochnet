#ifndef _RANGE_H_
#define _RANGE_H_ 1


namespace afidd
{



template<typename Range>
struct range_traits
{
  typedef typename Range::value_type value_type;
  typedef typename Range::reference_type reference_type;
  typedef typename Range::difference_type difference_type;
};





template<typename Descriptor>
struct range_archetype
{
  typedef Descriptor value_type;
  typedef value_type& reference_type;
  typedef size_t difference_type;

  value_type *_pt;
  reference_type front() const {
    return *_pt;
  }
  bool empty() const { return true; }
  range_archetype& advance_begin(difference_type n)
  {
    return *this;
  }
};




template<typename X>
struct RangeCheck
{
  typedef range_traits<X> t;
  typedef typename t::value_type value_type;
  typedef typename t::difference_type difference_type;
  typedef typename t::reference_type reference_type;

  BOOST_CONCEPT_USAGE(RangeCheck)
  {
    X x;
    x.empty();
    X& xref=x.advance_begin(0);
    value_type& tv=x.front();
  }
private:
  X x;
  value_type v;
};



}


#endif

