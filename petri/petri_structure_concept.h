#ifndef _PETRI_CONCEPT_H_
#define _PETRI_CONCEPT_H_ 1

#include <map>
#include <vector>
#include "boost/concept/assert.hpp"
#include "boost/concept_check.hpp"
#include "boost/property_map/property_map.hpp"

#include "range.h"
#include "petri_structure.h"


namespace afidd
{





template<typename X>
struct PetriStructureCheck
{
private:
  typedef petri_structure_traits<X> t;
public:
  typedef typename t::place_descriptor place_descriptor;
  typedef typename t::transition_descriptor transition_descriptor;
  typedef typename t::in_place_range in_place_range;
  typedef typename t::out_place_range out_place_range;
  typedef typename t::transition_range transition_range;

  BOOST_CONCEPT_ASSERT((RangeCheck<in_place_range>));
  BOOST_CONCEPT_ASSERT((RangeCheck<out_place_range>));
  BOOST_CONCEPT_ASSERT((RangeCheck<transition_range>));

  BOOST_CONCEPT_USAGE(PetriStructureCheck)
  {
    transition_range tr=out_transitions(pld, x);
    in_place_range ipr=in_places(trd, x);
    out_place_range opr=out_places(trd, x);
  }

private:
  X x;
  place_descriptor pld;
  transition_descriptor trd;
};







class petri_structure_archetype
{
public:
  struct place_descriptor {};
  struct transition_descriptor {};
  typedef range_archetype<place_descriptor> in_place_range;
  typedef range_archetype<place_descriptor> out_place_range;
  typedef range_archetype<transition_descriptor> transition_range;

  transition_range out_transitions(place_descriptor place) const
  {
    return {};
  }

  in_place_range in_places(transition_descriptor transition) const
  {
    return {};
  }
  
  out_place_range out_places(transition_descriptor transition) const
  {
    return {};
  }
};



class place_container_archetype : petri_structure_archetype
{
public:
  typedef std::vector<place_descriptor> place_container;
  place_container places();
};



class place_kind_archetype : place_container_archetype
{
  typedef char kind;
  typedef std::map<kind, place_container> kind_base_map;
public:
  typedef boost::associative_property_map<kind_base_map> kind_map;
  kind_map place_kinds();
};



class transition_container_archetype : petri_structure_archetype
{
public:
  typedef std::vector<place_descriptor> transition_container;
  transition_container places();
};



class transition_kind_archetype : transition_container_archetype
{
  typedef char kind;
  typedef std::map<kind, transition_container> kind_base_map;
public:
  typedef boost::associative_property_map<kind_base_map> kind_map;
  kind_map transition_kinds();
};


}

#endif
