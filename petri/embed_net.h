#ifndef _EMBED_NET_H_
#define _EMBED_NET_H_ 1

#include <tuple>
#include "boost/range/adaptor/transformed.hpp"
#include "petri_structure.h"


namespace afidd
{


template<typename StrategyCompose>
struct CombineTransform
{
  typedef typename StrategyCompose::type result_type;
  typedef typename StrategyCompose::left_type Local;
  typedef typename StrategyCompose::right_type Global;
  Global _global;
  CombineTransform(Global global)
  : _global(global)
  {}

  result_type operator()(Local x) const
  {
    return StrategyCompose::apply(x, _global);
  }
};




template<typename PetriStructure,typename Global>
class EmbedNet
{
  typedef typename PetriStructure::place_descriptor local_place;
  typedef typename PetriStructure::transition_descriptor local_transition;
  typedef concatenate_compose<local_place,Global> PlaceStrategy;
  typedef concatenate_compose<local_transition,Global>
      TransitionStrategy;
  typedef CombineTransform<PlaceStrategy> PlaceTransform;
  typedef CombineTransform<TransitionStrategy> TransitionTransform;

public:
  using place_descriptor=typename PlaceStrategy::type;
  using transition_descriptor=typename TransitionStrategy::type;
  using transition_range=boost::transformed_range<TransitionTransform,
      typename PetriStructure::transition_range>;
  using in_place_range=boost::transformed_range<PlaceTransform,
      typename PetriStructure::in_place_range>;
  using out_place_range=boost::transformed_range<PlaceTransform,
      typename PetriStructure::out_place_range>;

  using kind_range=typename PetriStructure::kind_range;
private:
  const PetriStructure _subnet;
  Global _g;

public:
  EmbedNet(const PetriStructure& sub_space, Global g)
  : _subnet(sub_space), _g(g)
  {
  }


  transition_range out_transitions(place_descriptor place) const
  {
    return out_transitions(PlaceStrategy::local(place), _subnet) | 
        boost::adaptors::transformed(TransitionTransform(_g));
  }


  in_place_range in_places(transition_descriptor transition) const
  {
    return in_transitions(TransitionStrategy::local(transition), _subnet) |
        boost::adaptors::transformed(PlaceTransform(_g));
  }


  out_place_range out_places(transition_descriptor transition) const
  {
    return out_transitions(TransitionStrategy::local(transition), _subnet) |
        boost::adaptors::transformed(PlaceTransform(_g));
  }


  kind_range kinds() const
  {
    return _subnet.kinds();
  }


  transition_range kind(typename PetriStructure::transition_kind tk) const
  {
    return out_transitions(_subnet.kind(tk) |
      boost::adaptors::transformed(TransitionTransform(_g)));
  }
};





template<typename PetriStructure,typename Global>
class DynamicEmbedNet
{
  typedef typename PetriStructure::place_descriptor local_place;
  typedef typename PetriStructure::transition_descriptor local_transition;
  typedef concatenate_compose<local_place,Global> PlaceStrategy;
  typedef concatenate_compose<local_transition,Global>
      TransitionStrategy;
  typedef CombineTransform<PlaceStrategy> PlaceTransform;
  typedef CombineTransform<TransitionStrategy> TransitionTransform;

public:
  using place_descriptor=typename PlaceStrategy::type;
  using transition_descriptor=typename TransitionStrategy::type;
  using transition_range=boost::transformed_range<TransitionTransform,
      typename PetriStructure::transition_range>;
  using in_place_range=boost::transformed_range<PlaceTransform,
      typename PetriStructure::in_place_range>;
  using out_place_range=boost::transformed_range<PlaceTransform,
      typename PetriStructure::out_place_range>;

  using kind_range=typename PetriStructure::kind_range;
private:
  const PetriStructure _subnet;

public:
  DynamicEmbedNet(const PetriStructure& sub_space)
  : _subnet(sub_space)
  {
  }


  transition_range out_transitions(place_descriptor place,
      Global g) const
  {
    return out_transitions(PlaceStrategy::local(place), _subnet) | 
        boost::adaptors::transformed(TransitionTransform(g));
  }


  in_place_range in_places(transition_descriptor transition,
      Global g) const
  {
    return in_transitions(TransitionStrategy::local(transition), _subnet) |
        boost::adaptors::transformed(PlaceTransform(g));
  }


  out_place_range out_places(transition_descriptor transition,
      Global g) const
  {
    return out_transitions(TransitionStrategy::local(transition), _subnet) |
        boost::adaptors::transformed(PlaceTransform(g));
  }


  kind_range kinds() const
  {
    return _subnet.kinds();
  }


  transition_range kind(typename PetriStructure::transition_kind tk,
      Global g) const
  {
    return out_transitions(_subnet.kind(tk) |
      boost::adaptors::transformed(TransitionTransform(g)));
  }
};

}


#endif
