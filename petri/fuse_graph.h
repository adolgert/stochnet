#ifndef _FUSE_GRAPH_H_
#define _FUSE_GRAPH_H_ 1

#include <vector>
#include <map>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "model_graph.h"
#include "petri_forest.h"
#include "breadth_double_black.h"


namespace afidd
{

  template<typename ContactGraph>
  std::map<std::tuple<size_t,size_t>,size_t> same_graph(
      const PetriGraphType& sub,
      const std::map<std::pair<size_t,size_t>,size_t>& trans_map,
      const ContactGraph& cg,
      PetriGraphType& full)
  {
    BOOST_LOG_TRIVIAL(debug) << "same_graph()";
    using Forest=PetriForest<PetriGraphType>;
    Forest forest(trans_map);

    std::map<Forest::ForestKey,size_t> translate;

    BreadthDoubleBlack<ContactGraph> breadth(cg, *vertices(cg).first);
    using CGVert=typename boost::graph_traits<ContactGraph>::vertex_descriptor;
    std::set<CGVert> all_cg_verts;
    for (auto vi=vertices(cg); vi.first!=vi.second; ++vi.first)
    {
      all_cg_verts.insert(*vi.first);
    }
    int color;
    enum Color { White, Grey, Black, Gandalf };
    std::vector<CGVert> verts;
    while (!breadth.empty())
    {
      std::tie(color, verts)=breadth.front();
      if (verts.size()==1)
      {
        CGVert v=verts[0];
        all_cg_verts.erase(v);
        if (color==Grey)
        {
          forest.add_subnet(sub, v);
          forest.add_local_subnet(v, full, translate);
        }
        else if (color==Black)
        {
          forest.add_fusions_of_subnet(v, full, translate);
        }
        else if (color==Gandalf)
        {
          ; // forest.forget_subnet(v);
          // translated.lose_subnet(v);
        }
        else
        {
          assert(false);
          throw std::runtime_error("unknown color for vertex");
        }
      }
      else
      {
        assert(verts.size()==2);
        if (verts.size()!=2)
        {
          throw std::runtime_error("Too many vertices from breadth search");
        }
        if (color==Grey)
        {
          forest.associate(verts[0], verts[1]);
        }
      }
      breadth.advance_begin();
    }
    if (all_cg_verts.size()>0)
    {
      std::cout << "not in giant component " << all_cg_verts.size()<<std::endl;
      for (auto missing : all_cg_verts)
      {
        forest.add_subnet(sub, missing);
        forest.add_local_subnet(missing, full, translate);
      }
    }
    BOOST_LOG_TRIVIAL(debug) << "~same_graph()";
    return translate;
  }

}

#endif // _FUSE_GRAPH_H_
