#define BOOST_TEST_MODULE petri_test
#include <vector>
#include <map>
#include <type_traits>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/test/included/unit_test.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/property_map/vector_property_map.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "petri_structure_concept.h"
#include "grid_transitions.h"
#include "sir_structure.h"
#include "model_graph.h"
#include "embed_net.h"
#include "sir_fuse.h"
#include "petri_graph.h"
#include "contact_graph.h"
#include "fuse_graph.h"



using namespace afidd;



BOOST_AUTO_TEST_SUITE( petri_structure )

/*! Constructs a Petri net as a cross of contact graph and SIR.
 */
BOOST_AUTO_TEST_CASE( sir_petri_graph )
{
  using RNGen=boost::random::mt19937;
  using ERGraph=stochnet::ErdosRenyiContactGraph;
  using ContactGraph=ERGraph::GraphType;
  using VertexProperty=ERGraph::VertexIdProperty;
  using EdgeIdProperty=ERGraph::EdgeIdProperty;

  RNGen rn_gen(1);
  ContactGraph cg;
  VertexProperty cg_verts;
  EdgeIdProperty edge_to_idx;

  size_t individual_cnt=20;
  double average_neighbors=6;
  double probable_connection=average_neighbors/individual_cnt;
  std::tie(cg, cg_verts, edge_to_idx)=
      ERGraph::create(rn_gen, individual_cnt, probable_connection);

  auto b=sir_herd();

  PetriGraphType full;
  std::map<std::pair<size_t,size_t>,size_t> trans_map;
  trans_map[{4, 5}]=7;
  same_graph(b, trans_map, cg, full);

  size_t v_cnt=num_vertices(cg);
  size_t e_cnt=num_edges(cg);
  std::cout << "cgverts" << v_cnt << " cgedges " <<
      e_cnt << std::endl;
  size_t pred_v=5*v_cnt+2*e_cnt;
  size_t pred_e=5*v_cnt+8*e_cnt;
  std::cout << "predv " << pred_v << " prededge " << pred_e << std::endl;
  std::cout << "verts " << num_vertices(full) << " edges " <<
      num_edges(full) << std::endl;
  assert(pred_v==num_vertices(full));
  assert(pred_e==num_edges(full));
}



BOOST_AUTO_TEST_CASE( archetype )
{
  using PlaceRange=typename petri_structure_archetype::in_place_range;
  BOOST_CONCEPT_ASSERT((RangeCheck<PlaceRange>));
  BOOST_CONCEPT_ASSERT((PetriStructureCheck<petri_structure_archetype>));
  petri_structure_archetype ps;
}


BOOST_AUTO_TEST_CASE( grid_neighbor )
{
  BOOST_CONCEPT_ASSERT((PetriStructureCheck<GridTransitions>));
}


BOOST_AUTO_TEST_CASE( sir_structure )
{
  SIRStructure sir;
  SIRInfectStructure infect;
  GridPlace<size_t> gp{ 3, 7 };
  gp={4,8};
  auto val=get<0>(gp);
}


BOOST_AUTO_TEST_CASE( more_official_sir )
{
  auto res=sir_definition();
  using PropGraph=decltype(res);
  using IndiGraph=typename std::tuple_element<0,PropGraph>::type;
  using VertProp=typename std::tuple_element<1,PropGraph>::type;
  using EdgeProp=typename std::tuple_element<2,PropGraph>::type;

  // This is the contact graph, an undirected graph.
  std::map<size_t,std::vector<size_t>> assoc{
    { 0, {1, 3} },
    { 1, {0, 4} },
    { 2, { } },
    { 3, {0} },
    { 4, {1}}
  };

  MultiplyGraph<IndiGraph,VertProp, EdgeProp> mult_graph;

  cross_with_contact(res, assoc);
}



BOOST_AUTO_TEST_CASE( embed_sir )
{
  SIRStructure sir;
  EmbedNet<SIRStructure,std::string> embed(sir,"Chicago");
  DynamicEmbedNet<SIRStructure,std::string> dyn_embed(sir);
}



/*! Petri nets are stored as Boost Graph Library graphs,
 *  so places and transitions have the same node type,
 *  vertex_descriptor. Can we create a type that holds
 *  a vertex_descriptor but is strongly-typed as a place
 *  or transition?
 */
template<typename Base,int BiPartiteIndex>
struct PlaceD
{
  Base base; // It holds a value of the base class.
  PlaceD()=default; // It is POD, a Plain Old Datatype.
  PlaceD(Base base) : base(base) {} // Implicit conversion of base.
  operator Base() const { return base; } // Conversion to the base.
};


BOOST_AUTO_TEST_CASE( pod_naming )
{
  using vertex_descriptor=size_t;
  using Place=PlaceD<vertex_descriptor,0>;
  using Transition=PlaceD<vertex_descriptor,1>;
  static_assert(std::is_pod<Place>::value==true, "Derived Place is POD");
  vertex_descriptor vd0=3;
  Place p0;
  Place p1(vd0);
  vertex_descriptor vd1(p1);
  vd1=p1;
  Transition t0(vd0);
  // This won't compile: p0=t0;
}



  typedef boost::adjacency_list<
    boost::vecS, // VertexList container
    boost::vecS, // OutEdgeList container
    boost::bidirectionalS, // directionality
    boost::no_property, // Vertex property
    boost::property<boost::edge_index_t, size_t>, // Edge property
    boost::no_property, // Graph property
    boost::vecS // EdgeList container
    >
    IndexedGraphT;


BOOST_AUTO_TEST_CASE( beyond_graph_end )
{
  using GType=IndexedGraphT;
  GType g(2);
  auto v0=add_vertex(g);
  auto v1=add_vertex(g);
  auto e=add_edge(v1, 17, g);
  std::cout << num_vertices(g) << std::endl;
  assert(num_vertices(g)==18);
}


BOOST_AUTO_TEST_CASE( just_graph )
{
  enum SIRNode { s, i, r, infect0, infect1, recover, infect };
  using GType=IndexedGraphT;
    std::vector<int> places {
      s, i, r, infect0, infect1, recover
    };
    std::vector<bool> place_incomplete {
      false, false, false, true, true, false
    };

    std::map<std::pair<int,int>,int> fuse{
      { {infect0, infect1}, infect}
    };

    std::vector<int> props {
      s, infect0, -1,
      infect0, i, 1,
      i, infect1, -1,
      infect1, i, 1,
      i, recover, -1,
      recover, r, 1
    };

    typedef std::pair<size_t,size_t> Edge;
    GType g(places.size());

    using edge_d=boost::graph_traits<GType>::edge_descriptor;
    std::map<edge_d,int> edge_weight;
    boost::vector_property_map<size_t> place_idx;
    boost::vector_property_map<size_t> desc_to_place;
    boost::vector_property_map<bool> template_place;

    for (size_t pl_idx=0; pl_idx<places.size(); pl_idx++)
    {
      auto vp=add_vertex(g);
      size_t place_type=places[pl_idx];
      place_idx[place_type]=vp;
      desc_to_place[vp]=place_type;
      template_place[vp]=place_incomplete[place_type];
    }

    edge_d e;
    bool success;
    for (size_t p_idx=0; p_idx<props.size(); p_idx+=3)
    {
      auto v0=place_idx[props[p_idx]];
      auto v1=place_idx[props[p_idx+1]];
      std::tie(e, success)=add_edge(v0, v1, g);
      if (success)
      {
        edge_weight[e]=props[p_idx+2];
      }
      else
      {
        BOOST_LOG_TRIVIAL(error) << "could not add edge";
      }
    }


    // This is the contact graph, an undirected graph.
    std::map<size_t,std::vector<size_t>> assoc{
      { 0, {1, 3} },
      { 1, {0, 4} },
      { 2, { } },
      { 3, {0} },
      { 4, {1}}
    };

    std::map<size_t,std::map<size_t,size_t>> node_places;
    std::map<edge_d,int> full_edge_weight;
    boost::vector_property_map<size_t> kind;

    GType full;
    // First copy all of the non-template nodes and edges.
    for (size_t n_idx=0; n_idx<assoc.size(); n_idx++)
    {
      auto verts=vertices(g);
      for (; verts.first!=verts.second; ++verts.first)
      {
        if (!template_place[*verts.first])
        {
          const auto& v=add_vertex(full);
          kind[v]=desc_to_place[*verts.first];
          node_places[n_idx][v]=desc_to_place[*verts.first];
        }
      }

      auto edge=edges(g);
      for (; edge.first!=edge.second; ++edge.first)
      {
        auto v0=source(*edge.first, g);
        auto v1=target(*edge.first, g);

        if (!template_place[v0] && !template_place[v1])
        {
          auto ed=add_edge(node_places[n_idx][v0], node_places[n_idx][v1], g);
          full_edge_weight[std::get<0>(ed)]=edge_weight[*edge.first];
        }
      }
    }

    // Then add in the template nodes and edges.
    for (size_t n_add=0; n_add<assoc.size(); n_add++)
    {
      for (auto& neigh : assoc[n_add])
      {
        auto v0=vertices(g);
        for (; v0.first!=v0.second; ++v0.first)
        {
          if (template_place[*v0.first])
          {
            auto v0_kind=desc_to_place[*v0.first];

            auto v1=vertices(g);
            for (; v1.first!=v1.second; ++v1.first)
            {
              if (template_place[*v1.first])
              {
                auto v1_kind=desc_to_place[*v1.first];
                // look for v0,v1 in a list of associations.
                auto v01=std::make_pair(v0_kind, v1_kind);
                auto or01=fuse.find(v01);
                bool found=true;
                std::pair<decltype(*v0.first),decltype(*v0.first)> nodes;
                auto transition_kind=0;
                if (or01!=fuse.end())
                {
                  nodes.first=*v0.first;
                  nodes.second=*v1.first;
                  transition_kind=or01->second;
                }
                else {
                  auto v10=std::make_pair(v1_kind, v0_kind);
                  auto or10=fuse.find(v10);
                  if (or10!=fuse.end())
                  {
                    nodes.first=*v1.first;
                    nodes.second=*v0.first;
                    transition_kind=or10->second;
                  }
                  else
                  {
                    found=false;
                  }
                }

                if (found)
                {
                  // Create a single node with both edges.
                  auto nvert=add_vertex(full);
                  kind[nvert]=transition_kind;
                  auto t0=std::get<0>(nodes);
                  auto t1=std::get<1>(nodes);

                  auto inl=in_edges(t0, g);
                  for (; inl.first!=inl.second; inl.first++)
                  {
                    auto s=source(*inl.first, g);
                    auto ae=add_edge(s, nvert, full);
                    full_edge_weight[std::get<0>(ae)]=edge_weight[*inl.first];
                  }
                  auto inr=in_edges(t1, g);
                  for (; inr.first!=inr.second; inr.first++)
                  {
                    auto s=source(*inr.first, g);
                    auto ae=add_edge(s, nvert, full);
                    full_edge_weight[std::get<0>(ae)]=edge_weight[*inr.first];
                  }

                  auto outl=out_edges(t0, g);
                  for (; outl.first!=outl.second; outl.first++)
                  {
                    auto t=target(*outl.first, g);
                    auto ae=add_edge(nvert, t, full);
                    full_edge_weight[std::get<0>(ae)]=edge_weight[*outl.first];
                  }
                  auto outr=out_edges(t1, g);
                  for (; outr.first!=outr.second; outr.first++)
                  {
                    auto t=target(*outr.first, g);
                    auto ae=add_edge(nvert, t, full);
                    full_edge_weight[std::get<0>(ae)]=edge_weight[*outr.first];
                  }
                }
              }
            }
          }
        }
      }
    }

}



BOOST_AUTO_TEST_SUITE_END()
