import logging
logger=logging.getLogger(__file__)
import os
from collections import defaultdict
import networkx as nx


class NodeRelabel(object):
    def __init__(self, underlying_graph):
        self.G=underlying_graph
        self.max=-1
        self.relabels=defaultdict(dict)
        self.cnt=0
        self.all_labels=list()


    def nodes(self):
        return self.G.nodes()

    def edges(self):
        return self.G.edges()


    def subnet_relabels(self, s_id):
        names=tuple(sorted(self.relabels[s_id].keys()))
        return names


    def add_node(self, a, props):
        assert(len(a)==2 and isinstance(a[0], int) and isinstance(a[1], int))
        sn_a, n_a=a
        k_a=self.cnt
        self.cnt+=1
        self.relabels[sn_a][n_a]=k_a
        self.G.add_node(k_a, props)
        return k_a


    def add_edge(self, a, b, props):
        nodes=[a,b]
        local_nodes=list()
        for add_n in nodes:
            subnet, n=add_n
            if n not in self.relabels[subnet]:
                logger.debug('adding edge {1} node not made {0}'.format(
                        add_n, nodes))
                local_nodes.append(self.add_node(add_n, {}))
            else:
                local_nodes.append(self.relabels[subnet][n])
        self.G.add_edge(local_nodes[0], local_nodes[1], props)


    def lose_labels(self, sn_a):
        for k, v in self.relabels[sn_a].items():
            self.all_labels.append( ((sn_a, k), v) )
        del self.relabels[sn_a]



class PlaceFusion(object):
    def __init__(self):
        self.fusions=list()

    def add(self, places):
        logger.debug('fusing places {0}'.format(places))
        self.fusions.append(places)

    def fused(self, place):
        for f in self.fusions:
            if place in f:
                return f[0]
        return None

    def forget(self, sn_id):
        to_delete=set()
        for f in self.fusions:
            for sn, p in f:
                if sn_id==sn:
                    to_delete.add(f)
        for td in to_delete:
            self.fusions.remove(td)



class Trans(object):
    def __init__(self, transitions, id, kind):
        self.transitions=transitions
        self.id=id
        self.kind=kind

    def __str__(self):
        return 'Trans({0},{1},{2})'.format(self.transitions, self.id, self.kind)


class TransitionFusion(object):
    def __init__(self):
        self.fusions=defaultdict(list)

    def add(self, transitions, new_transition_id, key):
        trans_info=Trans(transitions, key, new_transition_id)
        sn, node=key
        self.fusions[sn].append(trans_info)
        logger.debug('fusing transitions {0} into {1} with kind {2}'.format(
                transitions, key, new_transition_id))

    def of_subnet(self, sn_id):
        return self.fusions[sn_id]

    def forget(self, sn_id):
        del self.fusions[sn_id]

    def __str__(self):
        by_side=list()
        for s_id in self.fusions.keys():
            by_side.append('{0}: {1}'.format(s_id,
                    os.linesep.join([str(x) for x in self.fusions[s_id]])))
        return os.linesep.join(by_side)



class PetriForest(object):
    '''
    A set of NodeRelabel objects, fusion rules, and a contact graph
    produce a single NodeRelabel with a new key space.
    
    A place fusion is an equivalence among places. It is defined by
    in inclusion rule, given one place, meaning, given n, N(n,o)=0 or 1.
    Here, the place is identified by its key in the nx.Graph. Each
    place equivalence resolves to the smallest key's place id, which
    is (contact graph index, place id).

    A transition fusion maps an ordered set of transitions to a new
    transition. N(t0, t1)->t2. There may be more than one transition
    created from a single t0, so the forest assigns it a transition id
    in the forest subnet of t0
    and gives each and every transition a property 'transition' equal
    to the original transition id.
    '''
    def __init__(self, association, place_fuse, transition_fuse):
        '''
        Starts with no subnets. Associations say which subnet can fuse with
        which other subnets. It is a map from subnet to neighbors.
        place_fuse is an equivalence relation among subnet keys.
        transition_fuse is an ordered multimap to a new transition id.
        '''
        self.subnets=dict()
        self.association=association
        # The rules are inclusion rules in the set of fusions.
        self.place_rule=place_fuse
        self.transition_rule=transition_fuse

        # The fusions are the sets of unique node ids that are fused.
        self.place_fusion=PlaceFusion()
        self.transition_fusion=TransitionFusion()
        # Each fused transition needs a unique ID. This is the list
        # of ids to add to a subnet.
        self.subnet_extra_transitions=defaultdict(int)
        self.can_fuse=set()
        for a, b in self.transition_rule.keys():
            self.can_fuse.update([a, b])

        self.associated=set()


    def add_subnet(self, subnet, assoc_id):
        '''
        When this adds a subnet to the forest, it uses the fusion rules
        in the subnet key space
        to create explicit fusion equivalences in the forest key space.
        '''
        self.subnets[assoc_id]=subnet
        self.subnet_extra_transitions[assoc_id]=max(subnet.nodes())+1
        present_neighbors=[n for n in self.association(assoc_id)
                if n in self.subnets]
        logger.debug('subnet {0} neighbors {1}'.format(
                assoc_id, present_neighbors))
        for neighbor_id in present_neighbors:
            self.associate(assoc_id, neighbor_id)


    def set_subnet(self, subnet, assoc_id):
        logger.debug('creating subnet {0}'.format(assoc_id))
        self.subnets[assoc_id]=subnet
        self.subnet_extra_transitions[assoc_id]=max(subnet.nodes())+1



    def associate(self, assoc_id, neighbor_id):
        association=tuple(sorted((assoc_id, neighbor_id)))
        if association in self.associated:
            logger.error('already made association {0}'.format(association))
            raise RuntimeError('already associated {0} {1}'.format(
                    assoc_id, neighbor_id))
        else:
            self.associated.add(association)
            logger.debug('association count {0} {1}'.format(
                    len(self.associated), association))

        subnet=self.subnets[assoc_id]
        sn_neighbor=self.subnets[neighbor_id]
        for s0 in subnet.nodes():
            for s1 in sn_neighbor.nodes():
                if self.place_rule([s0, s1]):
                    self.place_fusion.add(
                            [(neighbor_id, s1), (assoc_id, s0)])

                ab=((s0, assoc_id), (s1, neighbor_id))
                for (t0, sn0), (t1, sn1) in [ab, reversed(ab)]:
                    if (t0, t1) in self.transition_rule:
                        tid=self.subnet_extra_transitions[assoc_id]
                        self.subnet_extra_transitions[assoc_id]+=1
                        self.transition_fusion.add(
                            [(sn0, t0), (sn1, t1)],
                            self.transition_rule[(t0,t1)],
                            (assoc_id, tid)
                            )

    def forget_subnet(self, s_id):
        logger.debug('forget {0}'.format(s_id))
        del self.subnets[s_id]
        del self.subnet_extra_transitions[s_id]
        self.transition_fusion.forget(s_id)
        self.place_fusion.forget(s_id)


    def copy_subnet(self, s_id, full):
        '''
        This copies all places and transitions associated with
        a subnet into a new graph. It uses the forest key space
        (subnet id, node within subnet). It performs fusions
        on places and transitions as required before adding to the
        full net. Any fused place or transition is associated,
        arbitrarily, with exactly one of the subnets in the fusion.
        '''
        try:
            sn=self.subnets[s_id]
        except KeyError:
            logger.warn('subnet {0} not in breadth search'.format(s_id))
            return

        self.add_local_subnet(s_id, full)
        self.add_fused_subnet(s_id, full)


    def add_local_subnet(self, s_id, full):
        assert(isinstance(s_id, int))
        node_cnt, edge_cnt=(len(full.G.nodes()), len(full.G.edges()))
        sn=self.subnets[s_id]
        logger.debug('subnet {0} copy nodes'.format(s_id))
        for n in sn.nodes():
            place_fused=self.place_fusion.fused((s_id, n))
            if place_fused:
                if (s_id, n)==place_fused:
                    logger.debug('adding fused place {0}'.format((s_id,n)))
                    props={'name' : n}
                    props.update(sn.node[n])
                    full.add_node((s_id, n), props)
                else:
                    logger.debug('not adding fused place {0}-{1}'.format(
                            place_fused, (s_id, n)))
            elif n in self.can_fuse:
                logger.debug('not adding {0} b/c fusable'.format(n))
            else:
                logger.debug('adding node {0}'.format((s_id, n)))
                props={'name' : n}
                props.update(sn.node[n])
                full.add_node((s_id, n), props)

        logger.debug('subnet {0} copy edges'.format(s_id))
        for b, e in sn.edges():
            if b not in self.can_fuse and e not in self.can_fuse:
                if self.place_fusion.fused((s_id, b)):
                    bb=self.place_fusion.fused((s_id, b))
                else:
                    bb=(s_id, b)
                if self.place_fusion.fused((s_id, e)):
                    ee=self.place_fusion.fused((s_id, e))
                else:
                    ee=(s_id, e)
                full.add_edge(bb, ee, sn[b][e])
            else:
                logger.debug('not adding fuse edge {0} {1}'.format(b, e))
        fnode_cnt, fedge_cnt=(len(full.G.nodes()), len(full.G.edges()))
        logger.debug('local sid {0} node {1} edge {2}'.format(
            s_id, fnode_cnt-node_cnt, fedge_cnt-edge_cnt))


    def add_fused_subnet(self, s_id, full):
        node_cnt, edge_cnt=(len(full.G.nodes()), len(full.G.edges()))
        sn=self.subnets[s_id]
        logger.debug('subnet {0} copy transitions'.format(s_id))
        for trans in self.transition_fusion.of_subnet(s_id):
            full.add_node(trans.id, {'name' : trans.kind})
            for s0_id, t0 in trans.transitions:
                sn0=self.subnets[s0_id]
                for in_place, t in sn0.in_edges(t0):
                    ip=(s0_id, in_place)
                    if self.place_fusion.fused(ip):
                        ip=self.place_fusion.fused(ip)
                    full.add_edge(ip, trans.id, sn0[in_place][t])
                for t, out_place in sn0.out_edges(t0):
                    ip=(s0_id, out_place)
                    if self.place_fusion.fused(ip):
                        ip=self.place_fusion.fused(ip)
                    full.add_edge(trans.id, ip, sn0[t][out_place])
        fnode_cnt, fedge_cnt=(len(full.G.nodes()), len(full.G.edges()))
        logger.debug('fused sid {0} node {1} edge {2}'.format(
            s_id, fnode_cnt-node_cnt, fedge_cnt-edge_cnt))



def breadth_first_search(g, start):
    '''
    The networkx implementation returns in BFS order all edges
    which are part of the breadth first search. This will exclude
    edges which connect two children of a parent, for instance.
    It also doesn't make it easy to see which vertices are visited
    for the first or last times. So, sorry, but we have to do
    this again.

    This tracks both edges and nodes. It returns _every_ edge
    thrice, as grey, black, and gandalf. Gandalf means that
    every edge of the traversed node is black, so that all of
    its neighbors are black.
    '''
    white, grey, black, gandalf=range(4)
    color={ n : white for n in g.nodes() }
    degree={ n : g.degree(n) for n in g.nodes() }
    edge_color={ tuple(sorted(edge)) : white for edge in g.edges() }

    q=list([start])
    color[start]=grey
    yield grey, start
    while q:
        parent=q.pop()
        nodes_to_gandalf=list()
        for n in g.neighbors(parent):
            if color[n]==white:
                q.insert(0, n)
                color[n]=grey
                yield grey, n
            else:
                pass
            edge=tuple(sorted((parent, n)))
            logger.debug('p {0} n {1} c {2} e {3}'.format(
                    parent, n, color[n], edge_color[edge]))
            if edge_color[edge]==white:
                edge_color[edge]=grey
                parent_edges_gandalf=False
            elif edge_color[edge]==grey:
                edge_color[edge]=black
                degree[parent]-=1
                degree[n]-=1
                if degree[n]==0:
                    nodes_to_gandalf.append(n)
            else:
                assert(False)
                raise RuntimeError('edge color black {0}'.format(edge))
            yield edge_color[edge], edge[0], edge[1]

        color[parent]=black
        yield black, parent

        for n_to_g in nodes_to_gandalf:
            assert(degree[n_to_g]==0)
            color[n_to_g]=gandalf
            yield gandalf, n_to_g

        if degree[parent]==0:
            color[parent]=gandalf
            yield gandalf, parent
