#ifndef _SIR_FUSE_H_
#define _SIR_FUSE_H_ 1

#include <tuple>
#include <map>
#include <set>
#include <vector>

namespace afidd
{

namespace detail
{
typedef boost::adjacency_list<
  boost::vecS, // VertexList container
  boost::vecS, // OutEdgeList container
  boost::bidirectionalS, // directionality
  boost::no_property, // Vertex property
  boost::property<boost::edge_index_t, size_t>, // Edge property
  boost::no_property, // Graph property
  boost::vecS // EdgeList container
  >
  IndexedGraphT;
}

std::tuple
    <
    detail::IndexedGraphT,
    std::vector<int>,
    std::map<boost::graph_traits<detail::IndexedGraphT>::edge_descriptor,int>,
    std::map<std::pair<int,int>,int>
    >
sir_definition();




template<typename Graph, typename VertexProps, typename EdgeProps>
struct MultiplyGraph
{
  using FullG=detail::IndexedGraphT;
  using IndiVert=typename boost::graph_traits<Graph>::vertex_descriptor;
  using IndiEdge=typename boost::graph_traits<Graph>::edge_descriptor;

  using FullVert=typename boost::graph_traits<FullG>::vertex_descriptor;
  using FullEdge=typename boost::graph_traits<FullG>::edge_descriptor;

  void operator()(size_t n, const Graph& g, const VertexProps& vp, 
      const EdgeProps& ep)
  {
    FullG full(num_vertices(g)*n);

    boost::vector_property_map<size_t> individual;
    for (size_t add=0; add<n; add++)
    {
      std::map<IndiVert,FullVert> full_of_local;
      auto iverts=vertices(g);
      for (; iverts.first!=iverts.second; ++iverts.first)
      {
        auto vf=add_vertex(full);
        individual[vf]=add;
        full_of_local[vf]=*iverts.first;
      }

      auto iedges=edges(g);
      for (; iedges.first!=iedges.second; ++iedges.first)
      {
        auto v0=source(*iedges.first);
        auto v1=target(*iedges.first);
        auto fe=add_edge(full_of_local[v0], full_of_local[v1], full);
      }
    }
  }
};




template<typename PropGraph,typename Contact>
std::tuple
    <
    detail::IndexedGraphT,
    std::vector<int>,
    std::map<boost::graph_traits<detail::IndexedGraphT>::edge_descriptor,int>
    >
cross_with_contact(const PropGraph& individual, const Contact& contact)
{
  const auto& g=std::get<0>(individual);
  const auto& kinds=std::get<1>(individual);
  const auto& weight=std::get<2>(individual);
  const auto& fuse=std::get<3>(individual);
  using IndiGraph=typename std::tuple_element<0,PropGraph>::type;
  using IndiEdge=typename boost::graph_traits<IndiGraph>::edge_descriptor;
  using IndiVert=typename boost::graph_traits<IndiGraph>::vertex_descriptor;

  using kinds_type=typename std::tuple_element<1,PropGraph>::type;
  using kind_type=typename kinds_type::value_type;

  using FullT=detail::IndexedGraphT;
  using FullEdge=
      typename boost::graph_traits<detail::IndexedGraphT>::edge_descriptor;
  using FullVert=
      typename boost::graph_traits<detail::IndexedGraphT>::vertex_descriptor;

  FullT full;
  std::vector<int> kind; // Given a node, what kind is it?
  std::map<FullEdge,int> full_weight;

  // Which are the template nodes? The ones in the fusion list.
  std::set<IndiVert> templated;
  for (auto& fuse_pair : fuse)
  {
    auto pair=fuse_pair.first;
    auto becomes=fuse_pair.second;
    auto left=pair.first;
    auto right=pair.second;
    templated.insert(left);
    templated.insert(right);
  }

  // Copy all non-template nodes and edges.

  return std::make_tuple(full, kind, full_weight);
}

}

#endif
