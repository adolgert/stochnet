#include "model_graph.h"


namespace afidd
{


bool model_ok(const PetriGraphType& g)
{
  using Vert=boost::graph_traits<PetriGraphType>::vertex_descriptor;
  using Edge=boost::graph_traits<PetriGraphType>::edge_descriptor;
  using VertIter=boost::graph_traits<PetriGraphType>::vertex_iterator;
  using EdgeIter=boost::graph_traits<PetriGraphType>::edge_iterator;

  VertIter vb, ve;
  std::set<Vert> verts;
  std::tie(vb, ve)=vertices(g);
  for (; vb!=ve; ++vb)
  {
    assert(g[*vb].color==PetriGraphColor::Place ||
      g[*vb].color==PetriGraphColor::Transition);
    size_t p=g[*vb].propensity;
    verts.insert(*vb);
  }

  for (size_t vidx=0; vidx<num_vertices(g); vidx++)
  {
    assert(verts.find(vidx)!=verts.end());
  }

  EdgeIter eb, ee;
  std::tie(eb, ee)=edges(g);
  for (; eb!=ee; ++eb)
  {
    assert(g[*eb].weight>-3 && g[*eb].weight<3);
  }

  return true;
}

  enum SIRState { s, i, r, i0, i1, i2, r0, i3 };

const size_t InfectWithin::which=i0;
const size_t InfectNeighbor::which=i3;
const size_t Recover::which=r0;


/*! SIR model for herd with simple tokens, so separate places for S, I, R.
 */
PetriGraphType sir_herd()
{
  PetriGraphType g(r0+1);
  std::vector<std::vector<int>> edges=
      {
        {s, i0, -1},
        {i, i0, -1},
        {i0, i, 2},
        {s, i1, -1},
        {i1, i, 1},
        {i, i2, -1},
        {i2, i, 1},
        {i, r0, -1},
        {r0, r, 1}
      };

  PetriGraphEdgeProperty edge_property;
  boost::graph_traits<PetriGraphType>::edge_descriptor edge;
  bool success;
  for (const auto& e : edges)
  {
    edge_property.weight=e[2];
    std::tie(edge, success)=add_edge(e[0], e[1], edge_property, g);
    assert(success);
    if (!success)
    {
      throw std::runtime_error("Could not add edge");
    }
  }

  PetriGraphVertexProperty vertex_property;
  for (auto place : std::vector<int>{s, i, r})
  {
    vertex_property.color=PetriGraphColor::Place;
    vertex_property.propensity=place;
    g[place]=vertex_property;
  }

  for (auto transition : std::vector<int>{i0, i1, i2, r0})
  {
    vertex_property.color=PetriGraphColor::Transition;
    vertex_property.propensity=transition;
    g[transition]=vertex_property;
  }

  model_ok(g);
  return g;
}




  struct FullVertexWriter
  {
    std::map<size_t, std::string> _name;
    FullVertexWriter(
      const std::map<std::tuple<size_t,size_t>,size_t>& translate)
    {
      for (const auto& entry : translate)
      {
        std::stringstream name;
        name << entry.second << ":" << get<0>(entry.first) << ":"
          << get<1>(entry.first);
        _name[entry.second]=name.str();
      }
    }

    template<typename VertexOrEdge>
    void operator()(std::ostream& out, const VertexOrEdge& v) const
    {
      out << " [label=\"" << _name.at(v) << "\"]";
    }
  };


  void save_full_graph(const std::string& name,
      const PetriGraphType& net,
      const std::map<std::tuple<size_t,size_t>,size_t>& translate)
  {
    
    std::ofstream full_graph_file(name);
    write_graphviz(full_graph_file, net, FullVertexWriter(translate));
  }

}
