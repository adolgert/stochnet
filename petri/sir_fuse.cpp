#include <vector>
#include <map>
#include <array>
#include <tuple>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/property_map/vector_property_map.hpp"
#include "sir_fuse.h"


namespace afidd
{


template<typename Node,typename Edge>
std::tuple
    <
    detail::IndexedGraphT,
    std::map<boost::graph_traits<detail::IndexedGraphT>::edge_descriptor,int>
    >
description_to_graph(const Node& node, const Edge& edge)
{
  using GType=detail::IndexedGraphT;

  using node_type=typename Node::value_type;
  using vertex_descriptor=boost::graph_traits<GType>::vertex_descriptor;
  using edge_descriptor=boost::graph_traits<GType>::edge_descriptor;
  std::map<node_type,vertex_descriptor> node_vertex;
  std::map<vertex_descriptor,node_type> vertex_node;
  std::map<edge_descriptor,int> edge_weight;

  GType g(node.size());
  for (auto& n : node)
  {
    auto v=add_vertex(g);
    vertex_node[v]=n;
    node_vertex[n]=v;
  }

  for (auto& edge_weighted : edge)
  {
    auto& edge=std::get<0>(edge_weighted);
    auto weight=std::get<1>(edge_weighted);
    auto v0=node_vertex[std::get<0>(edge)];
    auto v1=node_vertex[std::get<1>(edge)];
    edge_descriptor e;
    bool success;
    std::tie(e,success)=add_edge(v0, v1, g);
    if (!success)
    {
      assert(success);
      BOOST_LOG_TRIVIAL(error) << "could not add edge";
    }
    edge_weight[e]=weight;
  }

  return std::make_tuple(g, edge_weight);
}



std::tuple
    <
    detail::IndexedGraphT,
    std::vector<int>,
    std::map<boost::graph_traits<detail::IndexedGraphT>::edge_descriptor,int>,
    std::map<std::pair<int,int>,int>
    >
sir_definition()
{
  enum SIRNode { s, i, r, infect0, infect1, recover, infect };

  std::vector<int> nodes {
    s, i, r, infect0, infect1, recover
  };

  std::map<std::pair<int,int>,int> fuse{
    { {infect0, infect1}, infect}
  };

  using Edge=std::array<size_t,2>;
  using Weight=int;
  std::vector<std::tuple<Edge,Weight>> props {
    std::make_tuple(Edge{s, infect0}, -1),
    std::make_tuple(Edge{infect0, i}, 1),
    std::make_tuple(Edge{i, infect1}, -1),
    std::make_tuple(Edge{infect1, i}, 1),
    std::make_tuple(Edge{i, recover}, -1),
    std::make_tuple(Edge{recover, r}, 1)
  };

  auto res=description_to_graph(nodes, props);
  auto g=std::get<0>(res);
  auto weight=std::get<1>(res);

  return std::make_tuple(g, nodes, weight, fuse);
}




}
