#ifndef _MODEL_GRAPH_H_
#define _MODEL_GRAPH_H_ 1

#include <memory>
#include <string>
#include <sstream>
#include <fstream>
#include <algorithm>
#include <vector>
#include <map>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/mpl/vector.hpp"
#include "boost/mpl/for_each.hpp"
#include "boost/graph/graphviz.hpp"
#include "petri_graph.h"
#include "contact_graph.h"
#include "fuse_graph.h"


namespace afidd
{


  enum SIRHerdParameters { unused, alpha, beta, gamma };



    struct InfectWithin
    {
      static const size_t which;

      template<typename Mark, typename Param>
      static double propensity(const Mark& mark, const Param& param)
      {
        BOOST_LOG_TRIVIAL(debug) << "propensity iw " << get(mark,0) << " " 
          << get(mark,1) << " " << get(param, SIRHerdParameters::beta);      
        return get(param, SIRHerdParameters::beta)
            *get(mark,0).value*get(mark, 1).value;
      }
    };



    struct InfectNeighbor
    {
      static const size_t which;

      template<typename Mark, typename Param>
      static double propensity(const Mark& mark, const Param& param)
      {
        BOOST_LOG_TRIVIAL(debug) << "propensity in " << get(mark,0) << " " 
          << get(mark,1) << " " << get(param, SIRHerdParameters::gamma);      
        return get(param, SIRHerdParameters::gamma)
            *get(mark,0).value*get(mark, 1).value;
      }
    };




    struct Recover
    {
      static const size_t which;

      template<typename Mark, typename Param>
      static double propensity(const Mark& mark, const Param& param)
      {
        BOOST_LOG_TRIVIAL(debug) << "propensity r " << get(mark,0) << " " 
          << get(param, SIRHerdParameters::alpha);      
        return get(param, SIRHerdParameters::alpha)*get(mark,0).value;
      }
    };



    class SIRHerdReactions
    {
    public:
      using PropFunc=boost::mpl::vector<InfectWithin,InfectNeighbor,Recover>;
    };


    /*! Iterate over a type list and construct a map of functions from it.
     *  CreateFunctionPointers<ParamPMap,RolePMap>::PropensityFunctionMap pfm;
        CreateFunctionPointers<ParamPMap,RolePMap> cfp(pfm);
        mpl::for_each<ReactionDescription::PropFunc>(cfp);
        std::cout << pfm.size() << " calls created" << std::endl;

        double val=pfm[Transition::infect](param_pmap, role_pmap);
     */
    template<typename Mark, typename Param>
    struct CreateFunctionPointers
    {
      using PropensityFunc=std::function<double(const Mark&, const Param&)>;
      using PropensityFunctionMap=std::map<size_t,PropensityFunc>;
      PropensityFunctionMap& _f;

      CreateFunctionPointers(PropensityFunctionMap& f) : _f(f) {}


      // When MPL library runs this, it acts on lots of copies.
      // That's why the map has to be a reference.
      CreateFunctionPointers(const CreateFunctionPointers& other)
      : _f(other._f) {}


      CreateFunctionPointers& operator=(const CreateFunctionPointers& other)
      {
        _f=other._f;
        return *this;
      }

      template<typename PropFunc>
      void operator()(const PropFunc& pf)
      {
        _f.emplace(std::make_pair(PropFunc::which, PropensityFunc(
          [](const Mark& mark, const Param& param)->double {
            return PropFunc::propensity(mark, param);
          })));
      }
    };


    /*! Turns a typelist of template functors into a list of function pointers.
     *  The functors have template operator() methods, as in:
     *
     *      struct Recover
     *      {
     *          static const size_t which;
     *
     *          template<typename Mark, typename Param>
     *          static double propensity(const Mark& mark, const Param& param)
     *          {
     *              return get(param, SIRHerdParameters::alpha)*get(mark,0);
     *          }
     *      };
     *
     *  A series of these then go into a typelist of the form
     *
     *      boost::mpl::vector<InfectWithin,InfectNeighbor,Recover>;
     *
     *  This function then returns a map from the member variables
     *  called which to std::functions that call the templates
     *  with the required types.
     */
    template<typename FunctionTypes,typename Mark, typename Param>
    std::map<size_t,std::function<double(const Mark&,const Param&)>>
    propensities_to_function_list()
    {
      using CreateFP=CreateFunctionPointers<Mark,Param>;
      typename CreateFP::PropensityFunctionMap pfm;
      CreateFP cfp(pfm);
      boost::mpl::for_each<typename FunctionTypes::PropFunc>(cfp);

      return pfm;
    }



    template<typename Mark, typename Param>
    std::map<size_t,std::function<double(const Mark&,const Param&)>>
    sir_herd_propensities()
    {
        return propensities_to_function_list<
                SIRHerdReactions,Mark,Param>();
    }

  PetriGraphType sir_herd();


  void save_full_graph(const std::string& name,
      const PetriGraphType& net,
      const std::map<std::tuple<size_t,size_t>,size_t>& translate);


  /*! Create Petri net of SIR individuals on random contact graph.
   */
  template<typename RNGen>
  std::tuple<PetriGraphType,std::map<std::tuple<size_t,size_t>,size_t>>
  sir_spatial(RNGen& rn_gen,
    size_t individual_cnt=20, double average_neighbors=6)
  {
    BOOST_LOG_TRIVIAL(debug) << "sir_spatial()";
    using ERGraph=stochnet::ErdosRenyiContactGraph;
    using ContactGraph=ERGraph::GraphType;
    using VertexProperty=ERGraph::VertexIdProperty;
    using EdgeIdProperty=ERGraph::EdgeIdProperty;

    ContactGraph cg;
    VertexProperty cg_verts;
    EdgeIdProperty edge_to_idx;

    double probable_connection=average_neighbors/individual_cnt;
    if (probable_connection<1)
    {
    std::tie(cg, cg_verts, edge_to_idx)=
        ERGraph::create(rn_gen, individual_cnt, probable_connection);
    }
    else
    {
      // Make a complete graph.
      using Vertex=boost::graph_traits<ContactGraph>::vertex_descriptor;
      std::vector<Vertex> verts(individual_cnt);
      for (size_t vidx=0; vidx<individual_cnt; vidx++)
      {
        verts[vidx]=add_vertex(cg);
      }
      // Make a complete graph.
      for (size_t i=0; i<individual_cnt-1; i++)
      {
        for (size_t j=i+1; j<individual_cnt; j++)
        {
          add_edge(verts[i], verts[j], cg);
        }
      }
    }
#ifndef NDEBUG
    std::ofstream graph_out("contact_graph.graphviz");
    write_graphviz(graph_out, cg);
#endif // NDEBUG
    auto b=sir_herd();

    PetriGraphType full;
    std::map<std::pair<size_t,size_t>,size_t> trans_map;
    trans_map[{4, 5}]=7;
    auto translate=same_graph(b, trans_map, cg, full);

    size_t v_cnt=num_vertices(cg);
    size_t e_cnt=num_edges(cg);
    std::cout << "cgverts" << v_cnt << " cgedges " <<
        e_cnt << std::endl;
    size_t pred_v=5*v_cnt+2*e_cnt;
    size_t pred_e=5*v_cnt+8*e_cnt;
    std::cout << "predv " << pred_v << " prededge " << pred_e << std::endl;
    std::cout << "verts " << num_vertices(full) << " edges " <<
        num_edges(full) << std::endl;
    assert(pred_v==num_vertices(full));
    assert(pred_e==num_edges(full));
#ifndef NDEBUG
    save_full_graph("full_graph.graphviz", full, translate); 
#endif // NDEBUG

    BOOST_LOG_TRIVIAL(debug) << "~ir_spatial()";
    return std::make_tuple(full, translate);
  }
}



#endif // _MODEL_GRAPH_H_
