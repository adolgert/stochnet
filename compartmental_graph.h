#ifndef _COMPARTMENTAL_GRAPH_H_
#define _COMPARTMENTAL_GRAPH_H_ 1

#include <vector>
#include <exception>
#include <algorithm>
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/graphviz.hpp"
#include "boost/graph/erdos_renyi_generator.hpp"
#include "boost/mpl/print.hpp"


namespace stochnet {


struct EdgeIndex {
  size_t idx;
};
struct MetaPopulationId {
  int meta_population_id;
};

struct MetaPopIndex : public MetaPopulationId
{
  size_t idx;
};

class CompartmentalGraph
{
  // We need to order the edges so that we can tag their propensities.
  // Therefore, we specify a vecS for the EdgeList, which stores an
  // EdgeList in a vector to make its edges() iterator RandomAccess. We also
  // store that index in the graph as a property of the edge.
  typedef boost::adjacency_list<
                    boost::vecS, // VertexList container
                    boost::vecS, // OutEdgeList container
                    boost::undirectedS, // Directed/bidirectional
                    MetaPopulationId, // Vertex property
                    EdgeIndex, // Edge property
                    boost::no_property, // Graph property
                    boost::vecS // EdgeList container
                    > IndexedGraph;
  typedef typename boost::graph_traits<IndexedGraph>::out_edge_iterator
    EdgeIterator;
  typedef std::vector<unsigned char> Compartment;
  Compartment _compartment;
  IndexedGraph _graph;
  double _time;

public:
  template<class InputGraph>
  CompartmentalGraph(const InputGraph& graph)
  {
    auto vertex_cnt=boost::num_vertices(graph);
    _graph=IndexedGraph(vertex_cnt);

    typename boost::graph_traits<InputGraph>::edge_iterator begin, end;
    for (std::tie(begin, end)=boost::edges(graph); begin!=end; begin++)
    {
      size_t source=graph[boost::source(*begin, graph)].idx;
      size_t target=graph[boost::target(*begin, graph)].idx;
      boost::add_edge(source, target, _graph);
    }
    std::cout << "copied edges" << std::endl;

    size_t edge_idx=0;
    for ( auto eit=boost::edges(_graph); eit.first!=eit.second; eit.first++)
    {
      _graph[*eit.first].idx=edge_idx++;
    }
    auto eit=boost::vertices(_graph);
    auto fit=boost::vertices(graph);
    for ( ; eit.first!=eit.second; eit.first++, fit.first++)
    {
      _graph[*eit.first].meta_population_id=
                  graph[*fit.first].meta_population_id;
    }
    std::cout << "copied id tags" << std::endl;

    _compartment.resize(vertex_cnt);
    std::fill(_compartment.begin(), _compartment.end(), 0);
  }


  template<typename Token>
  size_t transition(Token transition_id)
  {
    int reaction=transition_id.get<0>().get<0>();
    size_t which=transition_id.get<0>().get<1>();
    _time+=transition_id.get<1>();

    size_t affected_vertex;
    size_t vertex_cnt=boost::num_vertices(_graph);
    switch (reaction)
    {
      case 0:
        _compartment[which]=2;
        affected_vertex=which;
        break;

      case 1:
        {
          // If the graph's EdgeSet template parameter is a list,
          // then this edge iterator will be Order(Edges).
          auto edge_set=boost::edges(_graph);
          std::advance(edge_set.first, which);

          auto source=boost::source(*edge_set.first, _graph);
          auto target=boost::target(*edge_set.first, _graph);

          if (_compartment[target]==0) {
            _compartment[target]=1;
            affected_vertex=target;

          } else if (_compartment[source]==0) {
            _compartment[source]=1;
            affected_vertex=source;
          } else {
            throw std::runtime_error("one should be susceptible");
          }
          std::cout << "set " << affected_vertex << " to 1" << std::endl;
        }
        break;

      default:
        throw std::runtime_error("Unknown transition type.");
    }
    return affected_vertex;
  }



  //! How many transitions should be tracked for a transition of given type.
  /*! Type 0 is the vertex itself. Type 1 includes first neighbors.
   *  That means type 0 counts nodes, type 1 counts edges.
   */
  size_t transition_order(int transition_type)
  {
    size_t order=0;
    switch (transition_type)
    {
      case 0:
        order=boost::num_vertices(_graph);
        break;
      case 1:
        order=boost::num_edges(_graph);
        break;
      default:
        throw std::runtime_error("transition_order: Unknown transition type");
    }
    return order;
  }



  template<class Updater>
  void update_zero(size_t vertex, Updater& updater) const
  {
    std::vector<std::tuple<size_t,size_t>> updates;
    size_t recovery_value=(_compartment[vertex]==1) ? 1 : 0;
    updates.push_back(std::make_tuple(vertex, recovery_value));
    updater.set(updates);
  }



  template<class Updater>
  void update_one(size_t vertex, Updater& updater) const
  {
    std::vector<std::tuple<size_t,size_t>> updates;
    for (auto oe=out_edges(vertex, _graph); oe.first!=oe.second; oe.first++)
    {
      auto source=boost::source(*oe.first, _graph);
      auto target=boost::target(*oe.first, _graph);
      unsigned char sval=_compartment[source];
      unsigned char tval=_compartment[target];
      size_t trans_value=0;
      if (sval==0 && tval==1 || sval==1 && tval==0) {
        trans_value=1;
      }
      size_t edge_idx=_graph[*oe.first].idx;
      updates.push_back(std::make_tuple(edge_idx, trans_value));
    }
    updater.set(updates);
  }


private:
  std::vector<EdgeIterator> _edge;
  std::map<EdgeIterator, size_t> _edge_idx;
};


template<class State>
struct LoneStateInitializer
{
  typedef typename State::Transition Transition;
  Transition _all;
  Transition _lone;
  LoneStateInitializer(Transition all, Transition lone)
  : _all(all), _lone(lone)
  {
  }
  template<typename ForwardIterator>
  void operator()(ForwardIterator begin, ForwardIterator end) {
    while (begin!=end) {
      *begin++=_all;
    }
  }
};

}
#endif // _COMPARTMENTAL_GRAPH_H_
