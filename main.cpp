#include <iostream>
#include <typeinfo>
#include <map>
#include "boost/tuple/tuple.hpp"
#include "boost/version.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "boost/random/poisson_distribution.hpp"
#include "boost/random/binomial_distribution.hpp"
#include "boost/random/uniform_int_distribution.hpp"
#pragma message "Boost lib version: " BOOST_LIB_VERSION

//#include "direct.h"
#include "compartmental_graph.h"
#include "binary_prefix.h"
#include "rate_adjusted.h"
#include "linear_search.h"
#include "state_machine.h"


using namespace stochnet;
using namespace std;
using namespace boost;

typedef boost::mt19937 RNGen;


// We make the out edgelist a set so that there is only one edge
// between any two vertices. Make sure the EdgeList is a list so
// that it will not slow adding edges. Because the EdgeList is a set,
// the vertex_descriptor is no longer a size_t but is now a void*.
// That means we store the index in MetaPopIndex instead.
typedef boost::adjacency_list<
                  boost::vecS, // VertexList container
                  boost::setS, // OutEdgeList container
                  boost::undirectedS, // Directed/bidirectional
                  MetaPopIndex, // Vertex property
                  boost::no_property, // Edge property
                  boost::no_property, // Graph property
                  boost::listS // EdgeList container
                  > ConstructGraph;

typedef std::map<size_t,typename ConstructGraph::vertex_descriptor> VertexIndex;

VertexIndex add_metapop(size_t node_cnt, double edge_fraction,
    int metapop, ConstructGraph& graph, RNGen& gen)
{
  typedef boost::sorted_erdos_renyi_iterator<RNGen, ConstructGraph> ERGen;

  ConstructGraph addition(ERGen(gen, node_cnt, edge_fraction),
      ERGen(), node_cnt);
  auto verts=boost::vertices(addition);
  size_t init_idx=0;
  for (; verts.first!=verts.second; verts.first++)
  {
    addition[*verts.first].idx=init_idx++;
  }

  size_t assign_vertex_idx=boost::num_vertices(graph);
  MetaPopIndex mpi;
  mpi.meta_population_id=metapop;

  VertexIndex vertex_index;
  for (size_t add_idx=0; add_idx<node_cnt; add_idx++)
  {
    mpi.idx=assign_vertex_idx++;
    vertex_index[add_idx]=boost::add_vertex(mpi, graph);
  }

  typename boost::graph_traits<ConstructGraph>::edge_iterator begin, end;
  for (std::tie(begin, end)=boost::edges(addition); begin!=end; begin++)
  {
    size_t source=addition[boost::source(*begin, addition)].idx;
    size_t target=addition[boost::target(*begin, addition)].idx;
    boost::add_edge(vertex_index[source], vertex_index[target], graph);
  }

  return vertex_index;
}



typename graph_traits<ConstructGraph>::vertex_descriptor
choose_and_remove(RNGen& gen, std::list<size_t>& side,
    VertexIndex& vertex_index)
{
  boost::random::uniform_int_distribution<size_t> uniform(0, side.size()-1);
  auto left_iter=side.begin();
  std::advance(left_iter, uniform(gen)); // This is the slow line.
  auto vertex=vertex_index[*left_iter];
  side.erase(left_iter);
  return vertex;
}



int main(int argc, char* argv[])
{
  size_t node_cnt=5000;
  double edge_fraction=0.3;
  double C=2.90156063;

  RNGen gen; // This is the single random number generator for everything.

  ConstructGraph graph;

  boost::random::poisson_distribution<> poisson(C);
  int meta_idx=0;
  VertexIndex left_vertex_index=add_metapop(node_cnt, edge_fraction,
        meta_idx++, graph, gen);
  for (; meta_idx<5; meta_idx++)
  {
    VertexIndex right_vertex_index=add_metapop(node_cnt, edge_fraction,
          meta_idx, graph, gen);

    std::list<size_t> left;
    std::list<size_t> right;
    for (size_t node_idx=0; node_idx<node_cnt; node_idx++)
    {
      int k=poisson(gen);
      boost::random::binomial_distribution<> binomial(k, edge_fraction);
      int left_cnt=binomial(gen);
      for (int li=0; li<left_cnt; li++)
      {
        left.push_back(node_idx);
      }
      size_t right_cnt=binomial(gen);
      for (int li=0; li<right_cnt; li++)
      {
        right.push_back(node_idx);
      }
    }

    size_t connection_cnt=std::min(left.size(), right.size());    
    for (size_t make_idx=0; make_idx<connection_cnt; make_idx++)
    {
      auto left_vertex=choose_and_remove(gen, left, left_vertex_index);
      auto right_vertex=choose_and_remove(gen, right, right_vertex_index);
      boost::add_edge(left_vertex, right_vertex, graph);
    }

    left_vertex_index=right_vertex_index;
    std::cout << "made " << meta_idx << std::endl;
  }

  CompartmentalGraph compartment(graph);

  StateMachine<RNGen,CompartmentalGraph> machine(gen, compartment);

  //machine.next();
  size_t step_cnt=10;
  for (size_t step_idx=0; step_idx<step_cnt; step_idx++) {
    auto token=machine.next();
  }

  return 0;
}
