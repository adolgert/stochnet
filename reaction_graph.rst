*********************************
Reaction Graph Structure
*********************************


Petri Structure
==================================

A Petri Structure is a bipartite graph, connecting places and transitions.
The places and transitions from this graph are unique identifiers.

Traits
--------
- petri_traits<PetriNet>::place_descriptor

- petri_traits<PetriNet>::transition_descriptor

- petri_traits<PetriNet>::transition_iterator


Free functions
-----------------

- transitions(place, petri_net), all transitions for which this place is an input

- in_places(transition, petri_net), all input places of a transition

- out_places(transition, petri_net), all output places of a transition


Place Container
====================
Orthogonal to the Petri structure is whether it _contains_ places that
are in the Petri structure. The basic place container is just
an std::container, with begin(), end(), and size().

- places(), returns a container of all places


Place Kind Container
======================
This place container has places which have different kinds. Each kind
has a unique key.

- kinds(), returns a property map of containers of places with kind as key.


Reaction Net
======================

Depends on Petri Net.
This has functions associated with transitions.

- property_map<Net, PropensityType>::type
get(Propensity, petri_net)

- propensity(transition, petri_net)

The function accepts a property map whose keys are the input places
of the transition. Somehow it accepts parameters, too, although that
seems extraneous given what has been defined so far.

Speed Net
===============
A speed of an edge.


Net with Subnets
=======================

Depends on Petri Net.

Free functions
----------------
- subnets(place, petri_net). Is the subnet representing an individual
  privileged compared to the subnet representing a transition connecting
  individual subnets? So I have to say this place belongs to individual 17
  and to infection reaction 17-52?

- subnets(transition, petri_net)


Denumerated Transitions
===========================
Depends on Petri Net.

Traits
---------

- index(transition, petri_net), maybe a traits::get<> thing.

Free functions
----------------
- counts(petri_net), returns a map from index kinds to their counts.
  This is a size() function. How do we make that work with or without
  partitions?


Partitioned Transitions
=======================

Depends on Petri Net.
This is a net that has separate classes of transitions.

- partition(transition, petri_net), maybe a traits::get<> thing.

