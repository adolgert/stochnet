#ifndef _STATE_MACHINE_H_
#define _STATE_MACHINE_H_ 1

#include "boost/shared_ptr.hpp"
#include "boost/random/uniform_int_distribution.hpp"
#include "direct.h"
#include "rate_adjusted.h"

namespace stochnet
{

template<typename RNGen, typename State>
class StateMachine {
  typedef LinearSearch<RateAdjusted<size_t>> Propensities;
  State& _state;
  Propensities _propensities;
  RNGen& _gen;
  typedef Direct<RNGen, Propensities> Dynamics;
  Dynamics _dynamics;

public:
  typedef typename Dynamics::Token Token;

  StateMachine(RNGen& gen, State& state)
  : _gen(gen), _state(state), _dynamics(gen, _propensities)
  {
    std::cout << "making propensities" << std::endl;
    auto recovery_propensity=boost::make_shared<RateAdjusted<size_t>>(
        0, _state.transition_order(1), 1.0);

    auto infection_propensity=boost::make_shared<RateAdjusted<size_t>>(
        1, _state.transition_order(0), 1.0);

    _propensities.add(recovery_propensity);
    _propensities.add(infection_propensity);

    // Set one to infected.
    boost::random::uniform_int_distribution<size_t>
      dist(0,_state.transition_order(0)-1);

    std::cout << "made propensities" << std::endl;

    auto t=boost::make_tuple(boost::make_tuple(1, dist(gen)), 0);
    auto affected_vertex=_state.transition(t);
    _propensities.update(affected_vertex, _state);

    std::cout << "done initialization" << std::endl;
  }


  /*! Get next token.
   *  This is an InputIterator.
   */
  Token next() {
    std::cout << "choose next" << std::endl;
    auto result=_dynamics.next();
    auto transition=result.get<0>();
    auto dt=result.get<1>();
    std::cout << "effect transition" << std::endl;
    auto affected_vertex=_state.transition(result);
    std::cout << "update propensities" << std::endl;
    _propensities.update(affected_vertex, _state);
    return transition;
  }
  
};

}

#endif // _STATE_MACHINE_H_
