import logging
import collections

logger=logging.getLogger(__file__)

s=1
i=2
r=3

class Reaction(object):
    def __init__(self, name, ins, outs):
        self.name=name
        self.ins=ins
        self.outs=outs

    def __str__(self):
        return self.name

    def __repr__(self):
        return self.name


local=[
    Reaction('ir', [(i,0,-1)], [(r,0,+1)])
    ]

remote=[
    Reaction('si', [(s,0,-1), (i,1,0)], [(i,0,1), (i,1,0)])
    ]

rx_by_name={'ir': local[0], 'si':remote[0]}

class Reactant(object):
    def __init__(self, kind, idx):
        self.kind=kind
        self.idx=idx


def neighbor_prep():
    matches=collections.defaultdict(list)
    for place in [s,i,r]:
        for rx in local+remote:
            for idx, rct in enumerate(rx.ins):
                if rct[0]==place:
                    matches[place].append([rx, idx])
    print(matches)
    return matches


def neighbors(matches, pairs, edge_idx, rct):
    nlist=list()
    for rx, ins_idx in matches[rct.kind]:
        rx_mult=len(rx.ins)
        if rx_mult==1:
            nlist.append([rx.name, rct.idx])
        elif rx_mult==2:
            for fellow in pairs[rct.idx]:
                idx=2*edge_idx[(rct.idx,fellow)]+ins_idx
                nlist.append([rx.name, idx])
        else:
            print('multiplicity over two?')

    return nlist


def reaction_places(rx, to_nodes):
    name, idx=rx
    rx=rx_by_name[name]
    if len(rx.ins)==1:
        individuals=[idx]
        ins_idx=0
    else:
        individuals=to_nodes[idx//2]
        ins_idx=idx%2
        if ins_idx!=0:
            individuals=[individuals[1], individuals[0]]

    roles_places=list()
    for place, ind_idx, delta in rx.ins+rx.outs:
        roles_places.append([place, individuals[ind_idx], delta])
    return roles_places


# What would an s match?
def fuse():
    matches=neighbor_prep()
    contacts=collections.defaultdict(set)
    edge_idx={(0,3):0, (0,7):1, (0,2):2, (7,0):1, (7,5):3, (7,9):4}
    contacts[0].update([3,7,2])
    contacts[7].update([0,5,9])
    print(neighbors(matches, contacts, edge_idx, Reactant(s,0)))
    print(neighbors(matches, contacts, edge_idx, Reactant(i,0)))
    print(neighbors(matches, contacts, edge_idx, Reactant(r,0)))

    to_nodes={0:(0,3), 1:(0,7), 2:(0,2), 3:(5,7), 4:(7,9)}
    print(reaction_places(['si',0], to_nodes))
    print(reaction_places(['si',1], to_nodes))
    print(reaction_places(['si',2], to_nodes))
    print(reaction_places(['si',3], to_nodes))
    print(reaction_places(['si',4], to_nodes))
    print(reaction_places(['si',5], to_nodes))
    print(reaction_places(['ir',0], to_nodes))

if __name__=='__main__':
    fuse()
