#ifndef _INDIVIDUAL_NET_H_
#define _INDIVIDUAL_NET_H_ 1

#include <map>
#include <type_traits>


namespace stochnet
{



template<typename Place>
class RolesPlaces
{
public:
  using ReactantId=std::tuple<int,size_t>; // kind, individual
private:
  std::map<std::string,Place> _reactants;
  std::map<std::string,Place> _products;
public:
  void add_role(const std::string& role, bool in, Place val)
  {
    if (in)
    {
      _reactants.emplace(std::make_pair(role,val));
    }
    else
    {
      _products.emplace(std::make_pair(role,val));
    }
  }
};


/*!
 * The reaction graph knows the number of individuals. It decides
 * how to count them. Hence the IndividualIdx template parameter.
 */
template<typename Reaction>
class IndividualTemplate
{
public:
  using StateStore=int;  // S,I,R specified as ints.
  using Space=IndividualSpace<StateStore>;
  // A local convenience.
  using ReactionId=typename Reaction::ReactionId;
  using Place=typename Space::Place;
  // Fill in the roles and places for a given reaction.
  void reaction(Space& state, RolesPlaces<Place>& rp, size_t ind_idx,
      const ReactionId& rx_id)
  {
    bool input=true;
    bool output=!input;

    auto kind=std::get<0>(rx_id);
    if (kind==0)
    {
      if (std::get<1>(rx_id)==ind_idx)
      {
        rp.add_role("S0", input, state[1]);
        rp.add_role("I0", output, state[2]);
      }
      else
      {
        rp.add_role("I1", input, state[2]);
        rp.add_role("I1", output, state[2]);
      }
    }
    else if (kind==1)
    {
      rp.add_role("I0", input, state[2]);
      rp.add_role("R0", output, state[3]);
    }
    else
    {
      assert(false);
    }
  }
};



template<typename IndividualIdx>
class SmallReaction
{
public:
  using Kind=int;
  using ReactantId=std::tuple<Kind,IndividualIdx>;
  using ReactionId=std::tuple<Kind,IndividualIdx,IndividualIdx>;
  using PropensityType=int;
};



class RGIndieNet
{
public:
  // How do we count individuals?
  using IndividualIdx=size_t;
  // The reaction is the primary thing.
  using Reaction=SmallReaction<IndividualIdx>;
  // From the reaction, we can create an individual template type.
  using Individual=IndividualTemplate<Reaction>;

  // And making short versions of these
  using Kind=typename Reaction::Kind;
  using ReactionId=typename Reaction::ReactionId;
  using ReactantId=typename Reaction::ReactantId;
  using PropensityType=typename Reaction::PropensityType;

  // We use this locally.
  using Reactants=std::vector<ReactantId>;
private:
  Individual _individual;
public:
  struct State {
    std::vector<typename Individual::Space> individual;
  };


  State create_state()
  {
    State s;
    s.individual.reserve(10);
  }


  Reactants fire(const ReactionId& rx_id, State& state)
  {
    auto kind=std::get<0>(rx_id);
    std::vector<size_t> pieces = { 2, 1 };

    using Roles=RolesPlaces<typename Individual::Space::Place>;
    Roles rp;
    if (pieces[kind]==1)
    {
      auto ind_idx=std::get<1>(rx_id);
      _individual.reaction(state.individual[ind_idx], rp, ind_idx, rx_id);
    }
    else
    {
      auto ind_idx=std::get<1>(rx_id);
      _individual.reaction(state.individual[ind_idx], rp, ind_idx, rx_id);
      ind_idx=std::get<2>(rx_id);
      _individual.reaction(state.individual[ind_idx], rp, ind_idx, rx_id);
    }

    //InfectionReaction rx(rx_id, rp);
  }


  std::vector<ReactionId> neighbors(const Reactants& reactants)
  {}

  std::tuple<ReactionId,PropensityType>
  propensity(const ReactionId& rx_id, const State& state)
  {}
};



BOOST_AUTO_TEST_CASE( rg )
{
  using State=RGIndieNet::State;
  RGIndieNet rg;
  auto state=rg.create_state();
  RGIndieNet::ReactionId rx_id{1,3,7};
  rg.fire(rx_id, state);
}


	
}

#endif
