#ifndef _PLACE_FUSION_H_
#define _PLACE_FUSION_H_ 1

#include "stochnet.h"



namespace stochnet
{



template<typename PlaceKind, typename ReactionKind, typename IndividualName,
    typename RoleKind>
struct PlaceFusionEdge
{
  PlaceFusionEdge()=default;
  PlaceFusionEdge(PlaceKind pk, ReactionKind rk, IndividualName in0,
      IndividualName in1, RoleKind r)
  : pk(pk), rk(rk), in0(in0), in1(in1), r(r) {}
  PlaceKind pk;
  ReactionKind rk;
  IndividualName in0, in1;
  RoleKind r;
};



template<typename RoleKind>
struct RGEdgeProp
{
  RGEdgeProp()=default;
  RGEdgeProp(RoleKind r, int delta) : role(r), delta(delta) {}
  RoleKind role;
  int delta;
};



template<typename ReactionDescription, typename ContactGraph,
    typename IndividualNameMap>
class PlaceFusionGraph
{
  const ContactGraph& cg;
  const IndividualNameMap& _individual;

public:
  using ReactionKind=typename ReactionDescription::RxKind;
  using PlaceKind=typename ReactionDescription::PlaceKind;
  struct ReactionId
    : public std::tuple<ReactionKind,IndividualName,IndividualName>
  {
    ReactionId()=default;
    ReactionId(ReactionKind k, IndividualName i0, IndividualName i1)
    : kind(k)
  };
  using ReactionId=std::tuple<ReactionKind,IndividualName,IndividualName>;
  using ReactantId=std::tuple<PlaceKind,IndividualName>;
  using IndividualName=
      typename boost::property_traits<IndividualNameMap>::value_type;
  using RoleKind=typename ReactionDescription::RoleKind;


  PlaceFusionGraph(ReactionDescription rd,
    const ContactGraph& contact_graph, const IndividualNameMap& individual_map)
  : _cg(contact_graph), _individual(individual_map),
    _name_to_ref_pmap(_name_to_ref)
  {
    _rx_cnt[ReactionDescription::infect]=2*boost::num_edges(_cg);
    _rx_cnt[ReactionDescription::recover]=boost::num_vertices(_cg);

    // Initialize the map from names to contact graph ids.
    for (auto cg_id: boost::make_iterator_range(boost::vertices(_cg)))
    {
      boost::put(_name_to_ref_pmap, boost::get(individual_map, cg_id), cg_id);
    }
  }



  const std::map<ReactionKind,size_t>& kinds() const
  {
    BOOST_LOG_TRIVIAL(trace) << "ReactionGraph::kinds() begin";
    return _rx_cnt;
  }



  std::vector<PlaceFusionEdge>
  out_edges(const ReactantId& rct_id) const
  {
    auto rct_kind=std::get<0>(rct_id);
    std::vector<PlaceFusionEdge> nlist;

    if (rct_kind==PlaceKind::s)
    {
      for (auto neighbor: _paired(subnet(rct_id)))
      {
        nlist.emplace_back(
            PlaceFusionEdge(rct_kind, ReactionDescription::infect,
                subnet(rct_id), neighbor, RoleKind::s0, -1));
      }
    }
    else if (rct_kind==PlaceKind::i)
    {
      for (auto neighbor: _paired(subnet(rct_id)))
      {
        nlist.emplace_back(
            PlaceFusionEdge(rct_kind, ReactionDescription::infect,
                neighbor, subnet(rct_id), RoleKind::i1, 0));
      }
      nlist.emplace_back(
        PlaceFusionEdge(rct_kind, ReactionDescription::recover,
                subnet(rct_id), subnet(rct_id), RoleKind::i0, -1));
    }
    else if (rct_kind==PlaceKind::r)
    {
      ; // Nothing to add.
    }
    return nlist;
  }



  // All the edges associated with a reaction.
  std::vector<PlaceFusionEdge>
  inout_edges(const ReactionId& rx_id) const
  {
    auto rx_kind=std::get<0>(rx_id);
    std::vector<PlaceFusionEdge> nlist;

    if (rx_kind==ReactionKind::infect)
    {
      nlist.emplace_back(
        PlaceFusionEdge(PlaceKind::s, rx_kind, subnet1(rx_id),
          subnet2(rx_id), RoleKind::s0, -1));
      nlist.emplace_back(
        PlaceFusionEdge(PlaceKind::i, rx_kind, subnet1(rx_id),
          subnet2(rx_id), RoleKind::i1, 0));
      nlist.emplace_back(
        PlaceFusionEdge(PlaceKind::i, rx_kind, subnet2(rx_id),
          subnet2(rx_id), RoleKind::i0, +1));
    }
    else if (rx_kind==ReactionKind::recover)
    {
      nlist.emplace_back(
        PlaceFusionEdge(PlaceKind::i, rx_kind, subnet1(rx_id),
          subnet1(rx_id), RoleKind::i0, -1));
      nlist.emplace_back(
        PlaceFusionEdge(PlaceKind::r, rx_kind, subnet1(rx_id),
          subnet1(rx_id), RoleKind::r0, +1));
    }
    return nlist;
  }



  ReactantId rct_target(const PlaceFusionEdge& e)
  {
    if (e.rk==ReactionKind::infect)
    {
      if (e.r==RoleKind::s0) {
        return ReactantId{PlaceKind::s, e.in0};
      }
      else if (e.r==RoleKind::i0)
      {
        return ReactantId{PlaceKind::i, e.in0};
      }
      else if (e.r==RoleKind::i1)
      {
        return ReactantId{PlaceKind::i, e.in1}
      }
      else
      {
        assert(false);
      }
    }
    else if (e.rk==ReactionKind::recover)
    {
      if (e.r==RoleKind::i0)
      {
        return ReactantId{PlaceKind::i, e.in0};
      }
      else if (e.r==RoleKind::r0)
      {
        return ReactantId{PlaceKind::r, e.in0};
      }
      else
      {
        assert(false);
      }
    }
    else
    {
      assert(false);
    }
    return ReactantId{e.pk, in0};
  }


  ReactionId rx_target(const PlaceFusionEdge& e)
  {
    return ReactionId{e.rk, in0, in1};
  }


  const RGEdgeProp&
  operator[](const PlaceFusionEdge& e)
  {
    return {e.r, e.delta};
  }


  // reactions()
  // Want to return an iterator that does a search.

private:
  IndividualName subnet(const ReactantId& rct_id)
  { return std::get<0>(rct_id); }

  IndividualName subnet1(const ReactionId& rx_id)
  { return std::get<0>(rx_id); }

  IndividualName subnet2(const ReactionId& rx_id)
  { return std::get<0>(rx_id); }

  //! Which individuals are paired with this one?

  using OutEdgeIter=boost::graph_traits<ContactGraph>::out_edge_iterator;
  using EdgeRange=boost::iterator_range<OutEdgeIter>;
  using CGEdge=typename boost::graph_traits<ContactGraph>::edge_descriptor;
  using CGVertex=typename boost::graph_traits<ContactGraph>::vertex_descriptor;
  using EdgeName=std::function<IndividualName(const CGEdge&)>;
  using ToName=boost::transformed_range<EdgeName,EdgeRange>;

  ToName _paired(const IndividualName& individual) const
  {
    EdgeRange oe(boost::out_edges(_cg,
        boost::get(_name_to_ref_pmap, individual)));

    EdgeName to_name=[&](const CGEdge& edge)->IndividualName
    {
      return boost::target(edge, _cg);
    }
    return oe | boost::adaptors::transformed(to_name);
  }


  std::map<ReactionKind,size_t> _rx_cnt;
  std::map<IndividualName,CGVertex> _name_to_ref;
  boost::associative_property_map<std::map<IndividualName,CGVertex>>
      _name_to_ref_pmap;
};


}


#endif
