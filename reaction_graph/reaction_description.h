#ifndef _REACTION_DESCRIPTION_H_
#define _REACTION_DESCRIPTION_H_ 1

#include "stochnet.h"
#include <vector>
#include <map>
#include <functional>
#include "boost/property_map/property_map.hpp"
#include "boost/mpl/vector.hpp"
#include "boost/mpl/for_each.hpp"



namespace stochnet
{


/*! This is the basic definition of a reaction, including the
 *  input roles, the reaction kind, a change set, and whether
 *  a reactant is an input or an output.
 */
template<typename Species, typename Reactions, typename Parameters,
    typename RoleKind>
struct TemplateReaction
{
public:
  using Roles=RoleKind;
  using RxKind=Reactions;
  using RoleTuple=std::tuple<Roles,int,bool>;
  using RoleVec=std::vector<RoleTuple>;

private:
  Reactions _id;
  RoleVec _roles;

public:
  TemplateReaction() =default;

  TemplateReaction(Reactions id, const RoleVec& roles)
  : _id(id), _roles(roles)
  {}

  ~TemplateReaction() {}

  TemplateReaction& operator=(const TemplateReaction& other)
  {
    _id=other._id;
    _roles=other._roles;

    return *this;
  }

  Reactions id() const { return _id; }

  std::vector<std::tuple<Roles,int,bool>> roles() const
  {
    return _roles;
  }


  friend
  std::ostream& operator<<(std::ostream& os, const TemplateReaction& prop)
  {
    os << "TemplateReaction: id "<<int(prop.id()) << " ";
    for (const auto& r : prop.roles())
    {
      os << "(r"<<int(std::get<0>(r))<<", d"<<std::get<1>(r)
          << ", i"<<std::get<2>(r)<<") ";
    }
    return os;
  }
};



/*! This assigns a reaction to a particular place in a subnet, meaning
 *  it assigns a place to each role of the reaction. There may be more
 *  than one assignment for each reaction.
 */
template<typename ReactionType, typename RoleKind, typename PlaceKind>
class ReactionAssignment
{
  const ReactionType& _r;
  std::vector<std::tuple<RoleKind,PlaceKind,int>> _assign;

public:
  ReactionAssignment(const ReactionType& r)
  : _r(r)
  {}
  ReactionAssignment& equate(RoleKind role, PlaceKind place,
      int order)
  {
    _assign.emplace_back(std::make_tuple(role, place, order));
    return *this;
  }

  ReactionType kind() const
  {
    return _r;
  }

  const std::tuple<RoleKind,PlaceKind, int>& equation(RoleKind role) const
  {
    auto which=std::find_if(_assign.begin(), _assign.end(),
      [&role](const std::tuple<RoleKind,PlaceKind, int>& rpo)
      {
        return std::get<0>(rpo)==role;
      }
      );
    if (which==_assign.end())
    {
      BOOST_LOG_TRIVIAL(error) << "Could not find assignment " << int(role)
          << " in reaction " << int(_r);
      throw std::exception();
    }
    return *which;
  }


  bool local() const
  {
    for (const auto& rpo : _assign)
    {
      if (std::get<2>(rpo)>0)
      {
        return false;
      }
    }
    return true;
  }

  friend
  std::ostream& operator<<(std::ostream& os,
    const ReactionAssignment<ReactionType,RoleKind,PlaceKind> &ra)
  {
    os << "ReactionAssignment "<<int(ra.kind())<<": ";
    for (const auto& ba : ra._assign)
    {
      os << "(r" << int(std::get<0>(ba)) << ", p" << int(std::get<1>(ba))
          << ", o" << std::get<2>(ba) << ") ";
    }
    return os;
  }
};


namespace
{

  template<typename PlaceKind, typename RoleKind>
  class BuildingAssignment
  {
    PlaceKind _place;
    RoleKind _role;
    int _order;
    int _delta;
    bool _inout;
  public:
    BuildingAssignment(PlaceKind place, RoleKind role, int order, int delta,
        bool inout)
    : _place(place), _role(role), _order(order), _delta(delta), _inout(inout) {}

    ~BuildingAssignment() {}

    PlaceKind place() const
    {
      return _place;
    }
    RoleKind role() const
    {
      return _role;
    }
    int order() const
    {
      return _order;
    }
    int delta() const
    {
      return _delta;
    }
    bool in() const
    {
      return _inout;
    }

    friend std::ostream& operator<<(std::ostream& os,
        const BuildingAssignment<PlaceKind,RoleKind>& ba)
    {
      return os << "BA(p" << int(ba._place) << ", r"<<int(ba._role)
          << ", o"<<ba._order << ", d" << ba._delta <<", i"<<ba._inout<<")";
    }
  };


  template<typename RxKind, typename Assignment>
  class BuildingReaction
  {
    RxKind _kind;
    std::vector<Assignment> _assign;
  public:
    BuildingReaction(RxKind kind, const std::vector<Assignment>& assign)
    : _kind(kind), _assign(assign) {}

    const std::vector<Assignment>& assignments() const
    {
      return _assign;
    }

    RxKind kind() const
    {
      return _kind;
    }

    friend std::ostream& operator<<(std::ostream& os,
        const BuildingReaction<RxKind,Assignment>& br)
    {
      os << "BuildingReaction "<<int(br._kind);
      for (const auto& a : br._assign)
      {
        os << a << " ";
      }
      return os;
    }
  };




template<typename Param, typename Prop, typename Transition>
struct EvalPropensity
{
  const Param& param;
  const Prop& prop;
  const Transition transition;
  double& rvalue;

  EvalPropensity(const Param& pa, const Prop& pr, Transition t, double& rv)
  : param(pa), prop(pr), transition(t), rvalue(rv)
  {}

  template<typename PropFunc>
  void operator()(const PropFunc& pf)
  {
    if (PropFunc::which==this->transition)
    {
      this->rvalue=pf.propensity(this->param, this->prop);
    }
  }
};


} // anonymous namespace




/*! This is the single interface between the template reactions and 
 *  the reaction graph. It represents all necessary information about
 *  the reactions to form the reaction graph.
 */
template<typename Species, typename ReactionType, typename Assignment,
    typename Params, typename Propensities>
class ReactionBuilder
{
public:
  using RoleKind=typename ReactionType::Roles;
  using PlaceKind=Species;
  using RxKind=typename ReactionType::RxKind;
  using Reaction=ReactionType;
  using Parameter=Params;
private:
  using BA=BuildingAssignment<PlaceKind,RoleKind>;
  using BR=BuildingReaction<RxKind,BA>;

  std::vector<BR> _local;
  std::vector<BR> _shared;

public:
  ReactionBuilder(const std::vector<Species>& species,
    const std::map<RxKind,ReactionType>& reactions,
    const std::vector<Assignment>& assign)
    : _species(species), _reactions(reactions), _assign(assign)
  {

    for (const auto& assign : _assign)
    {
      auto rx_kind=assign.kind();
      auto rx=reactions.at(rx_kind);

      std::vector<BA> full_assign;
      for (const auto & role_delta : rx.roles())
      {
        auto role=std::get<0>(role_delta);
        auto delta=std::get<1>(role_delta);
        bool in=std::get<2>(role_delta);
        const auto& role_species_order=assign.equation(role);
        auto species=std::get<1>(role_species_order);
        auto order=std::get<2>(role_species_order);
        full_assign.push_back(BA(species, role, order, delta, in));
      }
      BR br(rx_kind, full_assign);

      if (assign.local())
      {
        _local.push_back(br);
      }
      else // !assign.local()
      {
        _shared.push_back(br);
      }
    }
  }



  const std::vector<BR>& local_reactions() const
  {
    return _local;
  }



  const std::vector<BR>& shared_reactions() const
  {
    return _shared;
  }



  const Reaction& reaction(RxKind kind) const
  {
    try
    {
      return _reactions.at(kind);
    }
    catch (std::exception& e)
    {
      BOOST_LOG_TRIVIAL(error) << "Could not find reaction kind "
          << int(kind) << " in ReactionBuilder::reaction";
      throw;
    }
  }


  template<typename Role, typename Param>
  double propensity(RxKind kind, const Role& role, const Param& param) const
  {
    double result;
    EvalPropensity<Param,Role,RxKind> ep(param, role, kind, result);
    boost::mpl::for_each<Propensities>(ep);
    return result;
  }


  const std::vector<Species>& species() const
  {
    return _species;
  }


private:
  const std::map<RxKind,ReactionType> _reactions;
  const std::vector<Assignment> _assign;
  const std::vector<Species> _species;
};


}


#endif
