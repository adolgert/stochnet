#ifndef _SEIR_H_
#define _SEIR_H_ 1

#include <map>
#include <vector>
#include <functional>
#include "boost/mpl/vector.hpp"
#include "reaction_description.h"


namespace mpl=boost::mpl;

namespace stochnet
{



namespace SEIR
{
  enum class Species : char { unused, s, e, i, r };
  enum class Reactions : char { unused, expose, infect, recover };
  enum class Parameters : char { unused, alpha, beta, gamma };
  enum class Roles : char { unused, s0, e0, i0, r0, s1, e1, i1, r1 };

  struct Expose
  {
    static const Reactions which=Reactions::expose;

    template<typename Param, typename Prop>
    static double propensity(const Param& param, const Prop& prop)
    {
      return get(param, Parameters::beta)*get(prop, Roles::s0)
          *get(prop, Roles::i1);
    }
  };

  struct Infect
  {
    static const Reactions which=Reactions::infect;

    template<typename Param, typename Prop>
    static double propensity(const Param& param, const Prop& prop)
    {
      return get(param, Parameters::beta)*get(prop, Roles::e0);
    }
  };


  struct Recover
  {
    static const Reactions which=Reactions::recover;

    template<typename Param, typename Prop>
    static double propensity(const Param& param, const Prop& prop)
    {
      return get(param, Parameters::beta)*get(prop, Roles::i0);
    }
  };


  using Propensities=mpl::vector<Expose,Infect,Recover>;


  using SEIRPropensity=TemplateReaction<Species,Reactions,Parameters,Roles>;

  using SEIRReactionAssign=ReactionAssignment
    <
      Reactions,
      typename SEIRPropensity::Roles,
      Species
     >;
}

ReactionBuilder
  <
  SEIR::Species,
  SEIR::SEIRPropensity,
  SEIR::SEIRReactionAssign,
  SEIR::Parameters,
  SEIR::Propensities
  >
seir();



}


#endif
