// contact_graph_test.cpp
#include <iterator>
#include "demangle.h"
#include "boost/test/unit_test.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "contact_graph.h"


using namespace stochnet;




BOOST_AUTO_TEST_SUITE( contact_graph )

BOOST_AUTO_TEST_CASE( erdos_renyi )
{
  // Build a contact graph.
  using RNGen=boost::random::mt19937;
  using ContactGraph=ErdosRenyiContactGraph::GraphType;
  using VertexProperty=ErdosRenyiContactGraph::VertexIdProperty;
  using EdgeIdProperty=ErdosRenyiContactGraph::EdgeIdProperty;

  RNGen rn_gen;
  rn_gen.seed(1);

  ContactGraph cg;
  VertexProperty verts;
  EdgeIdProperty edge_to_idx;

  size_t individual_cnt=10;
  double average_neighbors=4;
  double probable_connection=average_neighbors/individual_cnt;
  std::tie(cg, verts, edge_to_idx)=
      ErdosRenyiContactGraph::create(rn_gen, individual_cnt,
      probable_connection);

  for (size_t e_idx=0; e_idx<boost::num_edges(cg); ++e_idx)
  {
    auto edge_iter=boost::edges(cg).first;
    std::advance(edge_iter, e_idx);
    BOOST_CHECK(e_idx==boost::get(edge_to_idx, *edge_iter));
  }

}


BOOST_AUTO_TEST_SUITE_END()
