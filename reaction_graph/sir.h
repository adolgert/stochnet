#ifndef _SIR_H_
#define _SIR_H_ 1

#include "boost/mpl/vector.hpp"
#include "reaction_description.h"

namespace stochnet
{


namespace SIR
{
  enum class States : int { unused, s, i, r };
  enum class Transitions : char { unused, infect, recover };
  enum class Parameters : char { unused, beta, gamma };
  enum class Roles : char { unused, s0, i0, r0, s1, i1, r1 };


  struct Infect
  {
    static const Transitions which=Transitions::infect;

    template<typename Param, typename Prop>
    static double propensity(const Param& param, const Prop& prop)
    {
      return get(param, Parameters::beta)*get(prop, Roles::s0)
          *get(prop, Roles::i1);
    }
  };


  struct Recover
  {
    static const Transitions which=Transitions::recover;

    template<typename Param, typename Prop>
    static double propensity(const Param& param, const Prop& prop)
    {
      return get(param, Parameters::beta)*get(prop, Roles::i0);
    }
  };


  using Propensities=boost::mpl::vector<Infect,Recover>;

  using SIRReaction=TemplateReaction<States,Transitions,Parameters,Roles>;
  using SIRReactionAssign=ReactionAssignment
    <
      Transitions,
      typename SIRReaction::Roles,
      States
    >;
}


ReactionBuilder
  <
  SIR::States,
  SIR::SIRReaction,
  SIR::SIRReactionAssign,
  SIR::Parameters,
  SIR::Propensities
  >
sir();


}

#endif
