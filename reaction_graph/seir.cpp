#include "stochnet.h"
#include "seir.h"

namespace stochnet
{


using namespace SEIR;

ReactionBuilder
  <
  Species,
  SEIRPropensity,
  SEIRReactionAssign,
  Parameters,
  Propensities
  >
seir()
{
  using Roles=SEIRPropensity::Roles;

  std::vector<Species> species={Species::s, Species::e, Species::i, Species::r};
  std::map<Reactions,SEIRPropensity> reactions;
  reactions[Reactions::expose]=SEIRPropensity(Reactions::expose,
      {     std::make_tuple(Roles::s0,-1,true),
            std::make_tuple(Roles::i1,0,true),
            std::make_tuple(Roles::e0,+1,false),
            std::make_tuple(Roles::i1,0,false)}
      );
  reactions[Reactions::infect]=SEIRPropensity(Reactions::infect,
    {std::make_tuple(Roles::e0,-1,true),
        std::make_tuple(Roles::i0,+1,false)}
    );
  reactions[Reactions::recover]=SEIRPropensity(Reactions::recover,
    {std::make_tuple(Roles::i0,-1,true),
            std::make_tuple(Roles::r0,+1,false)}
    );

  using Roles=SEIRPropensity::Roles;
  using ReactionAssign=ReactionAssignment<Reactions,Roles,Species>;
  std::vector<ReactionAssign> assignments;
  assignments.push_back(ReactionAssign(Reactions::expose)
      .equate(Roles::s0, Species::s, 0)
      .equate(Roles::e0, Species::e, 0)
      .equate(Roles::i1, Species::i, 1)
      );
  assignments.push_back(ReactionAssign(Reactions::infect)
      .equate(Roles::e0, Species::e, 0)
      .equate(Roles::i0, Species::i, 0)
      );
  assignments.push_back(ReactionAssign(Reactions::recover)
      .equate(Roles::i0, Species::i, 0)
      .equate(Roles::r0, Species::r, 0)
      );

  BOOST_LOG_TRIVIAL(trace) << "species ";
  for (const auto& ps : species)
  {
    BOOST_LOG_TRIVIAL(trace) << int(ps) << " ";
  }

  for (const auto& kv : reactions)
  {
    BOOST_LOG_TRIVIAL(trace) << std::get<1>(kv) << std::endl;
  }

  for (const auto& ra : assignments)
  {
    BOOST_LOG_TRIVIAL(trace) << ra << std::endl;
  }

  using ReactionDescription=
      ReactionBuilder<Species,SEIRPropensity,ReactionAssign,
                      Parameters,Propensities>;
  ReactionDescription rd(species, reactions, assignments);


  BOOST_LOG_TRIVIAL(trace) << "species ";
  for (const auto& ps : rd.species())
  {
    BOOST_LOG_TRIVIAL(trace) << int(ps) << " ";
  }

  BOOST_LOG_TRIVIAL(trace) << "local" << std::endl;
  for (const auto& kv : rd.local_reactions())
  {
    BOOST_LOG_TRIVIAL(trace) << kv;
  }

  BOOST_LOG_TRIVIAL(trace) << "shared" << std::endl;
  for (const auto& kv : rd.shared_reactions())
  {
    BOOST_LOG_TRIVIAL(trace) << kv;
  }

  return rd;
};

}
