#include <vector>
#include <map>
#include "sir.h"



namespace stochnet
{

using namespace SIR;


ReactionBuilder
  <
  SIR::States,
  SIR::SIRReaction,
  SIR::SIRReactionAssign,
  SIR::Parameters,
  SIR::Propensities
  >
sir()
{
  std::vector<States> species={States::s, States::i, States::r};
  std::map<Transitions,SIRReaction> reactions;

  bool in=true;
  bool out=!in;

  reactions[Transitions::infect]=
      SIRReaction(Transitions::infect,
        {
          std::make_tuple(Roles::s0,-1,in),
          std::make_tuple(Roles::i1,0,in),
          std::make_tuple(Roles::i0,+1,out),
          std::make_tuple(Roles::i1,0,out)
        }
      );

  reactions[Transitions::recover]=
      SIRReaction(Transitions::recover,
        {
          std::make_tuple(Roles::i0,-1,in),
          std::make_tuple(Roles::r0,+1,out),
        }
      );
  using RA=ReactionAssignment<Transitions,Roles,States>;
  std::vector<RA> assignments;
  assignments.push_back
    (
      RA(Transitions::infect)
        .equate(Roles::s0, States::s, 0)
        .equate(Roles::i0, States::i, 0)
        .equate(Roles::i1, States::i, 1)
    );
  assignments.push_back
    (
      RA(Transitions::recover)
        .equate(Roles::i0, States::i, 0)
        .equate(Roles::r0, States::r, 0)
    );


  using ReactionDescription=ReactionBuilder
    <
    SIR::States,
    SIR::SIRReaction,
    SIR::SIRReactionAssign,
    SIR::Parameters,
    SIR::Propensities
    >;
  ReactionDescription rd(species, reactions, assignments);
  return rd;
}

}