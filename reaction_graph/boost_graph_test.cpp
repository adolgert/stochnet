#define BOOST_TEST_MODULE boost_graph_test
#include "boost/test/included/unit_test.hpp"
#include "boost/bind.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "boost/array.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/graphviz.hpp"
#include "boost/graph/erdos_renyi_generator.hpp"
#include "contact_graph.h"
#include "reaction_description.h"
#include "reaction_graph.h"
#include "seir.h"


using namespace std;
using namespace boost::unit_test;
using namespace boost;
using namespace stochnet;

BOOST_AUTO_TEST_SUITE( bgl )

template<class GraphType>
void count_edges()
{
  GraphType g(5);
  add_edge(0, 3, g);
  assert(num_edges(g)==1);
  auto el=edges(g);
  assert(std::distance(el.first, el.second)==1);
}

BOOST_AUTO_TEST_CASE( edge_count )
{
  typedef adjacency_list<> Graph;
  count_edges<Graph>();

  typedef adjacency_list<vecS,vecS,undirectedS> Undirected;
  count_edges<Undirected>();

  typedef adjacency_list<vecS,vecS,bidirectionalS> Bidirectional;
  count_edges<Bidirectional>();
}


BOOST_AUTO_TEST_CASE( contact_graph )
{
  using RNGen=boost::random::mt19937;
  RNGen rn_gen;
  using GraphType=ErdosRenyiContactGraph::GraphType;
  using VertexProperty=ErdosRenyiContactGraph::VertexIdProperty;
  using EdgeIdProperty=ErdosRenyiContactGraph::EdgeIdProperty;

  GraphType cg;
  VertexProperty verts;
  EdgeIdProperty edge_to_idx;

  std::tie(cg, verts, edge_to_idx)=
      ErdosRenyiContactGraph::create(rn_gen, 10, 0.2);

  using VertexIter=typename boost::graph_traits<GraphType>::vertex_iterator;
  VertexIter b,e;
  for (std::tie(b,e)=boost::vertices(cg); b!=e; ++b)
  {
    std::cout << *b << " " << boost::get(verts, *b) << std::endl;
  }

  size_t edge_idx=0;
  for (auto ei=edges(cg); ei.first!=ei.second; ei.first++)
  {
    std::cout << *ei.first << " " << source(*ei.first, cg) << " "
        << target(*ei.first, cg) << " " <<
        boost::get(edge_to_idx, *ei.first) << std::endl;
  }
}




BOOST_AUTO_TEST_CASE( description )
{
  auto rd=seir();
  using ReactionDescription=decltype(rd);
  
  // Now use that reaction description.
  size_t contact_cnt=10;
  // Build a contact graph.
  using RNGen=boost::random::mt19937;
  using ContactGraph=ErdosRenyiContactGraph::GraphType;
  using VertexProperty=ErdosRenyiContactGraph::VertexIdProperty;
  using EdgeIdProperty=ErdosRenyiContactGraph::EdgeIdProperty;

  // Make a reaction graph from the contact graph.
  using PlaceKind=ReactionDescription::PlaceKind;
  using RGraph=ReactionGraph<ReactionDescription,ContactGraph,VertexProperty>;

  RNGen rn_gen;
  ContactGraph cg;
  VertexProperty verts;
  EdgeIdProperty edge_to_idx;

  size_t individual_cnt=contact_cnt;
  double average_neighbors=4;
  double probable_connection=average_neighbors/individual_cnt;
  std::tie(cg, verts, edge_to_idx)=
      ErdosRenyiContactGraph::create(rn_gen, individual_cnt, probable_connection);


  RGraph reaction_graph(rd, cg, verts);
}

BOOST_AUTO_TEST_SUITE_END()
