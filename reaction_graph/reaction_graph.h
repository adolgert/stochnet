#ifndef _REACTION_GRAPH_H_
#define _REACTION_GRAPH_H_ 1

#include <tuple>
#include <vector>
#include <map>
#include <set>
#include <type_traits>
#include <array>
#include <iterator>
#include <functional>
#include "stochnet.h"
#include "boost/functional.hpp"
#include "boost/graph/adjacency_list.hpp"
#include "boost/graph/breadth_first_search.hpp"
#include "boost/property_map/property_map.hpp"
#include "boost/property_map/vector_property_map.hpp"
#include "boost/iterator/iterator_adaptor.hpp"
#include "boost/iterator/transform_iterator.hpp"
#include "boost/range/iterator_range.hpp"
#include "boost/range/adaptor/transformed.hpp"
#include "boost/range/adaptor/filtered.hpp"
#include "reaction_description.h"
#include "demangle.h"


namespace stochnet
{

namespace
{

	//! S0, S1, I0, I1
	template<typename RoleType>
	struct Role
	{
		Role()=default; // Makes this a POD type.
		Role(RoleType r, int d, bool io) : role(r), delta(d), inout(io) {}
		RoleType role;
		int delta;
		bool inout;
	};



	/*! This constructs an explicit reaction graph from a contact graph.
	 *  It is a visitor pattern for a breadth first search.;
	 *  The trick is never to ask whether you've seen a vertex or edge
	 *  but to know whether you've seen it according to the breadth-first
	 *  search algorithm.
	 *
	 *  There are two equivalent approaches. Either wait for tree edges,
	 *  in which case the target vertex needs to be created on the spot,
	 *  or only add non-tree edges, for which the vertex has already
	 *  been found during an examination.
	 */
	template<typename ReactionGraph, typename Description,
			typename NameProperty, typename VertexSpeciesProperty,
			typename CGEdge>
	class RGBuilder : public boost::default_bfs_visitor
	{
		ReactionGraph& _rg;
		using GraphType=typename ReactionGraph::IndexedGraph;
		Description _description;
		using VertexDescriptor=
				typename boost::graph_traits<GraphType>::vertex_descriptor;
		enum { VertexCnt, EdgeCnt, RxCnt };
		std::shared_ptr<std::vector<size_t>> _cnt;

		using NameType=typename boost::property_traits<NameProperty>::value_type;
		NameProperty _individual;
		VertexSpeciesProperty _species;
		CGEdge& _cg_edge;
		using RoleKind=typename Description::RoleKind;
		using PlaceKind=typename Description::PlaceKind;
		using RxKind=typename Description::RxKind;
		using ReactionId=typename ReactionGraph::ReactionId;
		using ReactantId=typename ReactionGraph::ReactantId;
	public:

		RGBuilder(ReactionGraph& rg, Description desc, NameProperty name,
			VertexSpeciesProperty species, std::shared_ptr<std::vector<size_t>> cnt,
			CGEdge& cg_edge)
		: _rg(rg), _description(desc), _individual(name), _species(species),
		  _cnt(cnt), _cg_edge(cg_edge)
		{
			BOOST_LOG_TRIVIAL(debug) << "RGBuilder()";
		}

		void _add_individual(NameType u) const
		{
			BOOST_LOG_TRIVIAL(trace) << "_add_individual("<<u<<") begin";

			std::map<PlaceKind,VertexDescriptor> places;
			for (auto s : _description.species())
			{
				auto v=add_vertex(_rg._rg);
				boost::put(_rg._reactant_map, ReactantId{s, u}, v);
				boost::put(_rg._ref_to_reactant, v, ReactantId{s, u});
				places[s]=v;
			}

			// A local reaction needs a) kind b) places in c) places out.
			for (const auto& rx: _description.local_reactions())
			{
				auto vrx=add_vertex(1, _rg._rg);
				boost::put(_rg._reaction_map, ReactionId{rx.kind(), {u, u}}, vrx);
				boost::put(_rg._ref_to_reaction,
						vrx, ReactionId{rx.kind(), {u, u}});

				for (const auto& assignment : rx.assignments())
				{
					if (assignment.in())
					{
						add_edge(places[assignment.place()], vrx,
							Role<RoleKind>(assignment.role(), assignment.delta(), true),
							_rg._rg);
					}
					else
					{
						add_edge(vrx, places[assignment.place()],
							Role<RoleKind>(assignment.role(), assignment.delta(), false),
							_rg._rg);
					}
				}
			}

			boost::put(_species, u, places);
			BOOST_LOG_TRIVIAL(trace) << "_add_individual("<<u<<") end";
		}


		template<typename Edge, typename Graph>
		void examine_edge(Edge e, const Graph& g) const
		{
			BOOST_LOG_TRIVIAL(trace) << "examine_edge " << e << " ("
					<< boost::source(e,g) << "," << boost::target(e,g) << ")";
			auto source=boost::source(e, g);
			auto target=boost::target(e, g);
			auto key=std::make_tuple(std::min(source, target), std::max(source, target));
			if (_cg_edge[key]) return;

			_cg_edge[key]=true;

			std::vector<typename boost::property_traits<VertexSpeciesProperty>::value_type>
				inds;
			auto from_name=boost::get(_individual, boost::source(e,g));
	  	inds.push_back(boost::get(_species, from_name));
	  	auto to_name=boost::get(_individual, boost::target(e,g));
			BOOST_LOG_TRIVIAL(trace) << "examine_edge from " << from_name << " to " 
					<< to_name;
	  	inds.push_back(boost::get(_species, to_name));

			std::array<decltype(to_name),2> names{from_name, to_name};

			// A local reaction needs a) kind b) places in c) places out.
			for (const auto& rx: _description.shared_reactions())
			{
				int ord[]={0, 1};

				for (size_t flip=0; flip<2; flip++)
				{
					auto vrx=add_vertex(1, _rg._rg);
					boost::put(_rg._reaction_map, ReactionId{rx.kind(),
							{names[ord[0]], names[ord[1]]}}, vrx);
					boost::put(_rg._ref_to_reaction,
							vrx, ReactionId{rx.kind(), {names[ord[0]], names[ord[1]]}});

					for (const auto& assignment : rx.assignments())
					{
						if (assignment.in())
						{
							add_edge(inds[ord[assignment.order()]][assignment.place()], vrx,
							Role<RoleKind>(assignment.role(), assignment.delta(), true),
							_rg._rg);
						}
						else
						{
							add_edge(vrx, inds[ord[assignment.order()]][assignment.place()],
							Role<RoleKind>(assignment.role(), assignment.delta(), false),
							_rg._rg);
						}
					}
					ord[0]=1;
					ord[1]=0;
				}
			}
		}


		template<typename Edge, typename Graph>
		void non_tree_edge(Edge e, const Graph& g) const
		{
			BOOST_LOG_TRIVIAL(trace) << "non_tree_edge" << "(" << boost::source(e,g)
					<< "," << boost::target(e,g) << ")";
		}


		template<typename Edge, typename Graph>
		void tree_edge(Edge e, const Graph& g) const
		{
			BOOST_LOG_TRIVIAL(trace) << "tree_edge " << "(" << boost::source(e,g)
					<< "," << boost::target(e,g) << ")";
		}



		template<typename Vertex, typename Graph>
		void initialize_vertex(Vertex v, const Graph& g) const
		{
			BOOST_LOG_TRIVIAL(trace) << "initialize_vertex " << v;
			auto from_name=boost::get(_individual, v);
	  	_add_individual(from_name);
		}




		template<typename Vertex, typename Graph>
		void examine_vertex(Vertex v, const Graph& g) const
		{
			BOOST_LOG_TRIVIAL(trace) << "examine_vertex " << v;
			//auto from_name=boost::get(_individual, v);
	  	//_add_individual(from_name);
		}



		template<typename Vertex, typename Graph>
		void discover_vertex(Vertex e, const Graph& g) const
		{
			BOOST_LOG_TRIVIAL(trace) << "discover_vertex " << e;
		}

	};



	template<typename AKind,typename IdType>
	struct RxIdType
	{
		typedef AKind Kind;
		typedef IdType ID;
		typedef RxIdType<AKind,IdType> type;
		RxIdType()=default;
		RxIdType(const AKind& k, const IdType& id)
		: kind(k), id(id) {}
		AKind kind;
		IdType id;


		friend inline
		std::ostream&
		operator<<(std::ostream& os, const RxIdType<AKind,IdType>& rx_id)
		{
			return os << "(" << int(rx_id.kind) << " " << rx_id.id << ")";
		}

		bool operator==(const type& other)
		{
			return other.kind==kind && other.id==id;
		}
	};



	template<typename AKind,typename IdType>
	bool operator<(const RxIdType<AKind,IdType>& a, const RxIdType<AKind,IdType>& b)
	{
		return (a.kind<b.kind) || (a.kind==b.kind && a.id<b.id);
	}


	template<typename AKind,typename IdType>
	std::ostream& operator<<(std::ostream& os, const RxIdType<AKind,IdType>& rx_id)
	{
		return os << "(" << int(rx_id.kind) << " " << rx_id.id << ")";
	}


	template<typename PlaceType, typename Individual>
	struct RctIdType
	{
		typedef PlaceType Place;
		typedef Individual ID;
		typedef RctIdType<PlaceType,Individual> type;

		RctIdType()=default;
		RctIdType(const Place& place, const ID& subnet)
		: place(place), subnet(subnet) {}
		Place place;
		ID subnet;


		bool operator==(const type& other)
		{
			return other.place==place && other.subnet==subnet;
		}

		friend inline
		std::ostream& operator<<(std::ostream& os,
				const RctIdType<PlaceType,Individual>& rx_id)
		{
			return os << "(" << int(rx_id.place) << " " << rx_id.subnet << ")";
		}
	};

	template<typename AKind,typename IdType>
	bool operator<(const RctIdType<AKind,IdType>& a, const RctIdType<AKind,IdType>& b)
	{
		return (a.place<b.place) || (a.place==b.place && a.subnet<b.subnet);
	}




	/*! This wraps an array of node names so that it can be typed
	 *  and printed without clobbering someone's ostream of arrays.
	 */
	template<typename Name>
	struct ReactionGroup : std::array<Name,2>
	{
	  template<typename... T>
	  ReactionGroup(T&& ... t) : std::array<Name,2>{ {std::forward<T>(t)...} } {}
	  friend std::ostream& operator<<(std::ostream& os, const ReactionGroup& rg)
	  { return os << std::get<0>(rg) << " " << std::get<1>(rg); }
	};

} // end of anonymous namespace


template<typename ReactionDescription, typename ContactGraph,
		typename IndividualNameMap>
class ReactionGraph
{
	using IndexedGraph=boost::adjacency_list<
	    boost::vecS, // VertexList container
	    boost::vecS, // OutEdgeList container
	    boost::undirectedS, // Directed/bidirectional
	    char, // Vertex property. 0-reactant, 1-reaction.
	    Role<typename ReactionDescription::RoleKind>, // Edge property
	    boost::no_property, // Graph property
	    boost::vecS // EdgeList container
	    >;

public:
	using IndividualName=
			typename boost::property_traits<IndividualNameMap>::value_type;
	using PlaceKind=typename ReactionDescription::PlaceKind;
	//using ReactionId=std::tuple<typename ReactionDescription::RxKind,
	//		IndividualName,IndividualName>;
	using ReactionKind=typename ReactionDescription::RxKind;
	using ReactionId=RxIdType<ReactionKind,ReactionGroup<IndividualName>>;
	using ReactantId=RctIdType<typename ReactionDescription::PlaceKind,
			IndividualName>;
	using RoleKind=typename ReactionDescription::RoleKind;

	using EdgeType=typename boost::graph_traits<IndexedGraph>::edge_descriptor;

private:
	const ContactGraph& _cg;
	IndexedGraph _rg;
	const ReactionDescription& _description;
	using RctNode=typename boost::graph_traits<IndexedGraph>::vertex_descriptor;
	using RGEdge=typename boost::graph_traits<IndexedGraph>::edge_descriptor;
	using CGVert=typename boost::graph_traits<ContactGraph>::vertex_descriptor;
	using CGEdge=std::tuple<CGVert,CGVert>;

	// This can be a vector_property_map because the contact graph
	// is defined so that its vertex_descriptior is an unsigned.
	boost::vector_property_map<ReactantId> _ref_to_reactant;
	std::map<ReactantId,RctNode> _find_reactant;
	boost::associative_property_map<std::map<ReactantId,RctNode>> _reactant_map;
	boost::vector_property_map<ReactionId> _ref_to_reaction;
	std::map<ReactionId,RctNode> _find_reaction;
	boost::associative_property_map<std::map<ReactionId,RctNode>> _reaction_map;
	std::map<ReactionKind,size_t> _rx_cnt;

	std::vector<ReactionId> _reaction_id;

  using IndividualStore=
  		std::map<PlaceKind,
  				typename boost::graph_traits<ContactGraph>::vertex_descriptor>;
	using RGBuild=RGBuilder<
			ReactionGraph<ReactionDescription,ContactGraph,IndividualNameMap>,
			ReactionDescription,
			IndividualNameMap,
			boost::associative_property_map<std::map<IndividualName, IndividualStore>>,
			std::map<CGEdge,bool>
			>;

	friend RGBuild;
public:
	ReactionGraph(ReactionDescription rd,
		const ContactGraph& contact_graph, const IndividualNameMap& individual_map)
	: _cg(contact_graph),
		_reactant_map(_find_reactant), _reaction_map(_find_reaction),
		_description(rd)
	{

		std::map<IndividualName, IndividualStore> vertex_species;
		boost::associative_property_map<std::map<IndividualName, IndividualStore>>
				vertex_species_property(vertex_species);

		std::map<CGEdge,bool> edge_color;
		for (auto eic=boost::edges(contact_graph); eic.first!=eic.second; ++eic.first)
		{
			auto source=boost::source(*eic.first, contact_graph);
			auto target=boost::target(*eic.first, contact_graph);
			auto key=std::make_tuple(std::min(source, target), std::max(source, target));
			edge_color[key]=false;
		}

		auto count_ptr=std::make_shared<std::vector<size_t>>(3);

		// This should constuct the find_reactant and find_reaction maps!
		RGBuild builder(*this, rd, individual_map, vertex_species_property, 
				count_ptr, edge_color);
		auto first_vertex=*vertices(contact_graph).first;
		BOOST_LOG_TRIVIAL(trace) << "first_vertex=" << first_vertex;
		boost::breadth_first_search(contact_graph, first_vertex, 
				boost::visitor(builder));

		using VIter=typename boost::graph_traits<IndexedGraph>::vertex_iterator;
		VIter vb, ve;
		std::tie(vb, ve)=boost::vertices(_rg);
		for ( ; vb!=ve; ++vb)
		{
			if (_rg[*vb]==1)
			{
				auto kind=get(_ref_to_reaction, *vb).kind;
				auto cnt_iter=_rx_cnt.find(kind);
				if (cnt_iter!=_rx_cnt.end())
				{
					++(cnt_iter->second);
				}
				else
				{
					_rx_cnt.insert(std::make_pair(kind, 1));
				}
			}
		}

		for (const auto& id_ref : _find_reaction)
		{
			_reaction_id.push_back(std::get<0>(id_ref));
		}
	}



	const std::map<ReactionKind,size_t>& kinds() const
	{
		BOOST_LOG_TRIVIAL(trace) << "ReactionGraph::kinds() begin";
		return _rx_cnt;
	}


private:
	using VertexDescriptor=
			typename boost::graph_traits<IndexedGraph>::vertex_descriptor;
	using EdgeDescriptor=
			typename boost::graph_traits<IndexedGraph>::edge_descriptor;
	using OutEdgeIter=typename boost::graph_traits<IndexedGraph>::out_edge_iterator;
	using OutEdgeRange=boost::iterator_range<OutEdgeIter>;
	using NotIn=std::function<bool(const EdgeDescriptor e)>;
	using FilteredRange=boost::filtered_range<NotIn,OutEdgeRange>;
	//		typename boost::iterator_traits<OutEdgeRange>::iterator>;
	using TransFunc=std::function<ReactionId(const EdgeDescriptor)>;
	using TransRange=boost::transformed_range<TransFunc,FilteredRange>;
public:

	FilteredRange
	out_edges(const ReactantId& rct_id) const
	{

		std::function<bool(const EdgeDescriptor e)> only_out=
				[&](const EdgeDescriptor e)->bool
				{ return _rg[e].inout==true; };

		std::function<ReactionId(const EdgeDescriptor)> to_rx=
				[&](const EdgeDescriptor e)->ReactionId
				{
					auto v=boost::target(e, _rg);
					return boost::get(_ref_to_reaction, v);
				};

		OutEdgeRange ir=boost::make_iterator_range(
				boost::out_edges(_find_reactant.at(rct_id), _rg));
		FilteredRange outs=ir|boost::adaptors::filtered(only_out);
 		TransRange rx=outs | boost::adaptors::transformed(to_rx);
		return outs;
	}


	OutEdgeRange inout_edges(const ReactionId& rx_id) const
	{
		return boost::make_iterator_range(
				boost::out_edges(_find_reaction.at(rx_id), _rg));
	}


	ReactantId rct_target(EdgeDescriptor e) const
	{
		return boost::get(_ref_to_reactant, boost::target(e, _rg));
	}


	ReactionId rx_target(EdgeDescriptor e) const
	{
		return boost::get(_ref_to_reaction, boost::target(e, _rg));
	}


	const Role<typename ReactionDescription::RoleKind>&
	operator[](EdgeDescriptor e) const
	{
		return _rg[e];
	}


	template<typename Role, typename Param>
	double propensity(typename ReactionDescription::RxKind kind,
		const Role& role, const Param& param) const
	{
		return _description.propensity(kind, role, param);
	}


	const typename ReactionDescription::Reaction&
	propensity(typename ReactionDescription::RxKind kind) const
	{
		return _description.reaction(kind);
	}



	using ReactionIter=typename std::vector<ReactionId>::const_iterator;
	std::pair<ReactionIter,ReactionIter>
	reactions() const
	{
		return std::make_pair(_reaction_id.cbegin(), _reaction_id.cend());
	}

};



}



#endif // _REACTION_H_
