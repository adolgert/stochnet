#ifndef _ARCHETYPES_H_
#define _ARCHETYPES_H_ 1

#include <utility>
#include <tuple>


namespace stochnet
{

template<class State, class InputToken>
class output_function_archetype
{
public:
	struct OutputToken {};
	const OutputToken operator()(
		const State& state, const InputToken& input) const
	{
		return OutputToken{};
	}
};


class specification_archetype
{
public:

};


class dynamics_archetype
{
public:
	struct InputToken {};
	struct State {};
	void initialize(State& state) const {}
	void operator()(State& state, const InputToken& token) const {}
};


//! The SSA is responsible for choosing the next reaction.
/*! It will be a const object. The State that it declares
 *  is where it puts any cached information that it uses
 *  between calls. The random number generator, RNGen,
 *  is the only piece of outside state that it needs.
 */
template<class RNGen>
class ssa_archetype
{
public:
	struct RxChoiceId {};
	struct State {};
	using Duration=double;
	void initialize(State& state) const {}
	std::pair<RxChoiceId,double>
	  next(RNGen& rngen, State& state) {
	  	return{RxChoiceId(), 0.0};
	  }
	// This is perfect forwarding to the propensities within.
	template<class IterUpdates>
	void update(State& state, IterUpdates&& changes)
	{}
};



template<typename ReactionIdType>
class propensities_archetype
{
public:
  using RxChoiceId=ReactionIdType;
  template<typename Reactions>
  void initialize(const Reactions& reactions) {}
  double total() { return 0; }
  RxChoiceId choose(double) { return RxChoiceId(); }
  template<class IterUpdate>
  void update(IterUpdate& by_kind)
  {
  }
};



class reactions_archetype
{
public:
	struct State {};
	using ReactionId=size_t;
	using PropensityType=double;
	using ChangeSet=std::vector<
			std::tuple<ReactionId,PropensityType>>;

	ChangeSet fire(State& state, const ReactionId& reaction_id)
	{
		return ChangeSet();
	}
};

}

#endif
