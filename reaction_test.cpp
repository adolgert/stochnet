#include <type_traits>
#include <utility>
#include <boost/test/unit_test.hpp>
#include "boost/iterator/iterator_adaptor.hpp"
#include "boost/iterator/transform_iterator.hpp"
#include "boost/range/algorithm/find.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "archetypes.h"
#include "individual_space.h"
#include "reaction.h"
#include "direct.h"
#include "linear_search.h"
#include "rate_adjusted.h"
#include "finite_state_machine.h"
#include "ssa_dynamics.h"
#include "seir.h"
#include "constant_parameters.h"


using namespace stochnet;


BOOST_AUTO_TEST_SUITE( reaction )

BOOST_AUTO_TEST_CASE( archetype )
{
  reactions_archetype::State state;
  reactions_archetype ra;
  reactions_archetype::ReactionId rx_id;

  ra.fire(state, rx_id);
}


struct VertexName
{
  VertexName()=default;
  VertexName(size_t id) : idx(id) {}
  size_t idx;
};


typedef boost::adjacency_list<
                    boost::vecS, // VertexList container
                    boost::vecS, // OutEdgeList container
                    boost::undirectedS, // Directed/bidirectional
                    VertexName, // Vertex property
                    boost::no_property, // Edge property
                    boost::no_property, // Graph property
                    boost::vecS // EdgeList container
                    > ContactGraph;

BOOST_AUTO_TEST_CASE( starter )
{
  using RNGen=boost::random::mt19937;
  // Three things define the reaction graph
  // 1. The reactions themselves
  auto rd=seir();
  using SEIRDescription=decltype(rd);

  // The parameters for the reactions have a strategy.
  using Parameter=typename SEIRDescription::Parameter;
  using ParamMap=std::map<Parameter,double>;
  using ParamProp=boost::associative_property_map<ParamMap>;
  using ParamStrategy=ConstantParameters<ParamProp>;

  ParamMap params_map;
  ParamProp params(params_map);
  put(params, Parameter::alpha, 0.55);
  put(params, Parameter::beta, 0.55);
  put(params, Parameter::gamma, 0.55);

  ParamStrategy param_strategy(params);

  // 2. The contact graph
  ContactGraph cg;
  auto left=boost::add_vertex(VertexName(0), cg);
  auto right=boost::add_vertex(VertexName(1), cg);
  auto edge=boost::add_edge(left, right, cg);

  // 3. The names of nodes on the contact graph
  auto individual_map = boost::get(&VertexName::idx, cg);
  using NameType=boost::property_map<ContactGraph,size_t VertexName::*>::type;

  using RGraph=ReactionGraph<SEIRDescription,ContactGraph,NameType>;

  RGraph reaction_graph(rd, cg, individual_map);
  std::cout << "reaction_graph created" << std::endl;

  using Reactions=DirectReactions<RGraph,ParamStrategy>;
  using PropensityId=Reactions::PropensityId;

  Reactions reactions(reaction_graph, param_strategy);
  const auto k=reactions.kinds();
  for (auto r : k) {
    auto kind=std::get<0>(r);
    auto cnt=std::get<1>(r);
    std::cout << "reactions created " << int(kind) << " " << cnt << std::endl;
  }
  //
  // Have the reactions. Time to make a dynamics on them.
  //
  using ReactionKind=typename Reactions::ReactionKind;
  // Propensity type is int or double.
  using KindPropensity=RateAdjusted<typename Reactions::PropensityType>;
  using Propensities=LinearSearch<KindPropensity,ReactionKind>;
  using SSA=Direct<RNGen, Propensities>;


  SSA ssa;
  std::cout << "ssa created" << std::endl;
  std::cout << "Test just the direct state" << std::endl;
  {
    typename SSA::State ssa_state;
    const Reactions& cr_reactions=reactions;
    ssa.initialize(ssa_state, cr_reactions);
    std::cout << "End of test." << std::endl;
  }

  using Dynamics=typename stochnet::SSADynamics<SSA, Reactions, RNGen>;
  using State=typename Dynamics::State;

  Dynamics dynamics(ssa, reactions);
  std::cout << "dynamics created " << &dynamics << std::endl;
  {
    State s;
    std::cout << "Testing dynamics separately" << std::endl;
    const Dynamics& cr_dynamics=dynamics;
    cr_dynamics.initialize(s);
    std::cout << "Done testing dynamics separately" << std::endl;
  }

  std::cout << "dynamics the same " << &dynamics << std::endl;
  using OutputFunction=typename stochnet::output_function_archetype<State,
      typename Dynamics::InputToken>;

  OutputFunction output_function;

  using FSM=typename stochnet::FiniteStateMachine<Dynamics,OutputFunction>;

  // Place the state on the free store.
  auto state=std::make_shared<State>();
  State non_shared;
  std::cout << "state created" << std::endl;

  std::cout << "dynamics the same2 " << &dynamics << std::endl;
  FSM fsm(dynamics, output_function);
  std::cout << "fsm created" << std::endl;
  std::cout << "initialize non-shared" << std::endl;
  fsm.initialize(non_shared);
  std::cout << "initialize shared" << std::endl;
  fsm.initialize(*state);
  std::cout << "fsm initialized" << std::endl;

  auto out_token=fsm({});
}



BOOST_AUTO_TEST_CASE( test_rg_builder )
{
  using RNGen=boost::random::mt19937;
  // Three things define the reaction graph
  // 1. The reactions themselves
  auto rd=seir();
  using ReactionDescription=decltype(rd);

  // 2. The contact graph
  ContactGraph cg;
  using CGEdge=typename boost::graph_traits<ContactGraph>::edge_descriptor;
  using CGVert=typename boost::graph_traits<ContactGraph>::vertex_descriptor;
  CGVert left=boost::add_vertex(cg);
  CGVert right=boost::add_vertex(cg);
  std::cout << "left and right are "<<left << " and " << right << std::endl;
  CGEdge edge=std::get<0>(boost::add_edge(0, 1, cg));
  CGVert v0=boost::source(edge, cg);
  cg[v0]=VertexName(0);
  CGVert v1=boost::target(edge, cg);
  cg[v1]=VertexName(1);
  auto ei=boost::edges(cg);
  std::cout << "Created edge_cnt="
      << std::distance(std::get<0>(ei), std::get<1>(ei)) << std::endl;
  const auto& verts=boost::vertices(cg);
  std::cout << "Number of vertices=" << std::distance(std::get<0>(verts),
      std::get<1>(verts)) << std::endl;
  typename boost::graph_traits<ContactGraph>::out_edge_iterator ob, oe;
  auto start_vert=*boost::vertices(cg).first;
  std::tie(ob, oe)=boost::out_edges(start_vert, cg);
  for ( ; ob!=oe; ++ob)
  {
    std::cout << "Neighbor of left: " << *ob << std::endl;
    std::cout << "  source: " << boost::source(*ob, cg) << std::endl;
    std::cout << "  target: " << boost::target(*ob, cg) << std::endl;
  }

  // 3. The names of nodes on the contact graph
  auto individual_map = boost::get(&VertexName::idx, cg);
  using NameType=boost::property_map<ContactGraph,size_t VertexName::*>::type;

  using RGraph=ReactionGraph<ReactionDescription,ContactGraph,NameType>;
  RGraph reaction_graph(rd, cg, individual_map);

  using ReactantId=typename RGraph::ReactantId;
  using ReactionId=typename RGraph::ReactionId;

  size_t z{0}, o{1};

  auto s0=ReactantId{ReactionDescription::PlaceKind::s,z};
  auto i0=ReactantId{ReactionDescription::PlaceKind::i,z};
  auto r0=ReactantId{ReactionDescription::PlaceKind::r,z};
  auto s1=ReactantId{ReactionDescription::PlaceKind::s,o};
  auto i1=ReactantId{ReactionDescription::PlaceKind::i,o};
  auto r1=ReactantId{ReactionDescription::PlaceKind::r,o};

  auto si=ReactionId{ReactionDescription::RxKind::infect,{z,o}};
  auto is=ReactionId{ReactionDescription::RxKind::infect,{o,z}};
  auto ir0=ReactionId{ReactionDescription::RxKind::recover,{z,z}};
  auto ir1=ReactionId{ReactionDescription::RxKind::recover,{o,o}};

  std::function<ReactionId(RGraph::EdgeType)> edge_rct
      =[&reaction_graph](const RGraph::EdgeType& e)
      { return reaction_graph.rx_target(e); };

  auto out_edges=reaction_graph.out_edges(s0);
  BOOST_CHECK(boost::distance(out_edges)==1);
  auto out_rx=out_edges|boost::adaptors::transformed(edge_rct);
  BOOST_CHECK(boost::find(out_rx, si)!=boost::end(out_rx));

  out_edges=reaction_graph.out_edges(s1);
  BOOST_CHECK(boost::distance(out_edges)==1);
  out_rx=out_edges|boost::adaptors::transformed(edge_rct);
  BOOST_CHECK(boost::find(out_rx, is)!=boost::end(out_rx));

  out_edges=reaction_graph.out_edges(i0);
  BOOST_CHECK(boost::distance(out_edges)==2);
  out_rx=out_edges|boost::adaptors::transformed(edge_rct);
  BOOST_CHECK(boost::find(out_rx, ir0)!=boost::end(out_rx));

  out_edges=reaction_graph.out_edges(i1);
  BOOST_CHECK(boost::distance(out_edges)==2);
  out_rx=out_edges|boost::adaptors::transformed(edge_rct);
  BOOST_CHECK(boost::find(out_rx, ir1)!=boost::end(out_rx));
}



BOOST_AUTO_TEST_CASE( individual )
{
  // SIR as 1,2,3, so int for index.
  // Counts can be 0 or 1 so int for StoreType.
  using Space=IndividualSpace<int>;
  Space s;
  s[1]=1;
  int res=s;
  BOOST_CHECK(res==1);
  BOOST_CHECK(s[1]==1);
  s[2]=1;
  res=s;
  BOOST_CHECK(res==2);
  s[2]=0;
  res=s;
  BOOST_CHECK(res==0);
  s[1]=1;
  s[1]-=1;
  s[2]+=1;
  res=s;
  BOOST_CHECK(res==2);
  Space::Place sp(s[2]);
  sp=0;
  res=s;
  BOOST_CHECK(res==0);

  BOOST_CHECK(std::is_pod<Space>::value);
  static_assert(std::is_pod<Space>::value, "it is not a pod");

  const Space p{2};
  BOOST_CHECK(p[1]==0);
  BOOST_CHECK(p[2]==1);
}



BOOST_AUTO_TEST_CASE( ref_workshop )
{
  using Map=std::map<int,int>;

  class A
  {
    Map& _map;
  public:
    A(Map& map) : _map(map) {}
    void check() {
      for (auto kv : _map)
      {
        std::cout << kv.first << " " << kv.second << std::endl;
      }
    }
  };

  Map m;
  m[3]=7;
  m[2]=1;
  A a(m);
  a.check();
}


BOOST_AUTO_TEST_CASE( ids )
{
  using RxId=std::tuple<size_t,size_t>;
  enum class RxKind { none, infect, recover };
  using RID=RxIdType<RxKind,RxId>;
  enum class RctPlace { none, s, i, r };
  using ReactantId=RctIdType<RctPlace,std::string>;

  RID reaction_id{RxKind::infect, std::make_tuple(3,7)};
  ReactantId rct_id{RctPlace::s, "Chicago"};

  //RxKind kind=reaction_id.kind;

  auto k=RxIdType<int,double>{3,7.0}.kind;
  RxIdType<int,double> rit{3,7.4};
  rit.kind=3;

  RctPlace place=rct_id.place;
  std::string where=rct_id.subnet;
}

BOOST_AUTO_TEST_SUITE_END()
