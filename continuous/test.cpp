#define BOOST_TEST_MODULE petri_test
#include <map>
#include <tuple>
#include <memory>
#include <type_traits>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/test/included/unit_test.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "boost/property_map/property_map.hpp"
#include "mark_one_dynamics.h"
#include "ssa_dynamics.h"
#include "model_graph.h"
#include "model_parameters.h"
#include "service.h"
#include "hazards.h"
#include "direct.h"
#include "binary_prefix_cache.h"


using namespace afidd;

/*! The free put() and get() functions only work for const property maps.
 *  This was just trying to use them for non-const property maps.
 */
template <class Reference, class LvaluePropertyMap>
  struct put_get_non_const { };


  template <class PropertyMap, class Reference, class K>
  inline Reference
  get(put_get_non_const<Reference, PropertyMap>& pa, const K& k)
  {
    Reference v = static_cast<PropertyMap&>(pa).at(k);
    return v;
  }

  template <class PropertyMap, class Reference, class K>
  inline typename std::add_lvalue_reference<
    typename std::add_const<
      typename std::remove_reference<Reference>::type>::type>::type
  get(const put_get_non_const<Reference, PropertyMap>& pa, const K& k)
  {
    return static_cast<const PropertyMap&>(pa).at(k);
  }

  template <class PropertyMap, class Reference, class K, class V>
  inline void
  put(put_get_non_const<Reference, PropertyMap>& pa, K k, const V& v)
  {
    static_cast<PropertyMap&>(pa).emplace(k, v);
  }

template<typename From,typename To>
class MyMap :
    public std::map<From,To>,
    public put_get_non_const<To&, MyMap<From,To>> {};




BOOST_AUTO_TEST_SUITE( ssa_dynamics )

/*! A place marking type that encodes when the place was marked.
 *  This is a POD type.
 */
struct TimeMark
{
  TimeMark()=default;
  TimeMark(int value, double when) : value(value), when(when) {}
  int value;
  double when;

  friend inline std::ostream&
  operator<<(std::ostream& os, const TimeMark& tm)
  {
    return os << "TimeMark("<<tm.value<<", "<<tm.when<<")";
  }
};



/*! A ServicePolicy to convert TimeMarks to a true or false.
 *  Any value greater than one is returned as a one value.
 */
struct SingleTime
{
  using MarkFull=TimeMark;
  using MarkRestricted=TimeMark;
  MarkRestricted from_full(const MarkFull& f)
  {
    return {f.value>0 ? 1 : 0, f.when};
  }
  void set_full(const MarkRestricted& r, MarkFull& f)
  {
    f.value+=r.value;
    f.when=r.when;
  }
};



BOOST_AUTO_TEST_CASE( make_it )
{
  using RNGen=boost::random::mt19937;
  using Marking=std::map<size_t,TimeMark>;
  using Service=SingleService<Marking,TimeMark>;
  using ModelParameters=ConstantParameters;
  using MarkContainer=Service::MarkContainer;
  using ParamContainer=ModelParameters::ParameterContainer;
  using Hazards=ConstantHazards<Service,ModelParameters>;
  using PropensityCache=BinaryPrefixCache<double>;
  using Selection=Direct<RNGen,PropensityCache>;
  using Dynamics=SSADynamics<Marking, Selection, Hazards, RNGen>;
  using State=Dynamics::State;

  State state;
  state.rng=RNGen(1);

  ModelParameters parameters;
  parameters.put(1, 0.40);  // Recover
  parameters.put(2, 0.55);  // Internal infection
  parameters.put(3, 0.55);  // Infect neighbor

  BOOST_LOG_TRIVIAL(debug) << "Create the SIR model.";
  PetriGraphType gspn;
  std::map<std::tuple<size_t,size_t>,size_t> place_identifier;
  std::tie(gspn, place_identifier)=sir_spatial<RNGen>(state.rng, 2);
  BOOST_LOG_TRIVIAL(debug) << "Create propensities.";
  auto propensities=sir_herd_propensities<MarkContainer,ParamContainer>();
  for (const auto& prop_pair : propensities)
  {
    BOOST_LOG_TRIVIAL(debug) << prop_pair.first;
  }

  // Put a token in a single random place.
  MarkOneDynamics<State> mark_one(place_identifier);
  mark_one.initialize(state);
  mark_one(state, {});

  size_t isNonZero=0;
  for (const auto& v : state.marking)
  {
    if (v.second.value!=0)
    {
      isNonZero++;
    }
  }
  BOOST_LOG_TRIVIAL(debug) << "Number of nonzeros " << isNonZero;
  assert(isNonZero!=0);

  BOOST_LOG_TRIVIAL(debug) << "Create service.";
  Service service_policy(state.marking);
  BOOST_LOG_TRIVIAL(debug) << "Create hazards.";
  Hazards hazards(gspn, service_policy, parameters, propensities);

  BOOST_LOG_TRIVIAL(debug) << "Create dynamics.";
  Dynamics ssa(gspn, hazards);
  BOOST_LOG_TRIVIAL(debug) << "Initialize dynamics.";
  ssa.initialize(state);
  BOOST_LOG_TRIVIAL(debug) << "Call dynamics.";
  ssa(state, {});
}



BOOST_AUTO_TEST_CASE( put_get_map )
{
  MyMap<size_t,double> m;
  put(m, 3, 3.14);
  double approx=get(m,3);

  const MyMap<size_t,double>& mk(m);
  double out=get(mk,3);
}


/*! A const class can modify a non-const value at a const pointer.
 *  This is a reminder that a class can _own_ state in a unique_ptr
 *  and still modify it when the owning object is const. This 
 *  technique is used to create cache data in the stochastic
 *  algorithm.
 */
struct MyClass
{
  std::unique_ptr<double> val;
  MyClass() : val(new double()) {}
  void modify(double new_val) const
  {
    *val=new_val;
  }
};


BOOST_AUTO_TEST_CASE( pointer_to_non_const )
{
  MyClass m;
  const MyClass& mk(m);
  mk.modify(23);
}


BOOST_AUTO_TEST_SUITE_END()


