#ifndef _REACTION_H_
#define _REACTION_H_ 1

#include <vector>
#include <map>
#include "stochnet.h"
#include "reaction_graph.h"
#include "individual_space.h"


namespace stochnet
{


template<typename ReactionGraph, typename ParamStrategy>
class DirectReactions {
public:
	using ReactionKind=typename ReactionGraph::ReactionKind;
	using ReactionId=typename ReactionGraph::ReactionId;
	using PropensityType=double;
	// The first element of the reaction ID is the reaction kind.
	using PropensityId=std::tuple<ReactionKind,size_t>;
	using ChangeSet=std::map<ReactionKind,
			std::vector<std::tuple<PropensityId,PropensityType>>>;
	
	using PlaceKind=typename ReactionGraph::PlaceKind;
	using IndividualState=IndividualSpace<PlaceKind>;
	using IndividualName=typename ReactionGraph::IndividualName;
	struct State {
		// Choose a vector or a map depending on how the individuals are named.
		typedef typename std::conditional<
				std::is_unsigned<IndividualState>::value,
				std::vector<IndividualState>,
				std::map<IndividualName,IndividualState>
				>::type StateStorage;
		StateStorage _individual;
		IndividualState& individual(const IndividualName& ind_idx) {
			return _individual[ind_idx];
		}
	};
private:
	// Note that the reactants can be private to this.
	using ReactantId=typename ReactionGraph::ReactantId;
	const ReactionGraph& _reaction_graph;
	const ParamStrategy& _params;
	std::map<ReactionId,PropensityId> _to_idx;
	std::map<PropensityId,ReactionId> _to_rx_id;
	std::map<ReactionKind,size_t> _rx_cnt;

public:
	DirectReactions(const ReactionGraph& rg, const ParamStrategy& params)
	: _reaction_graph(rg), _params(params)
	{
		std::map<ReactionKind,size_t> rx_cnt;
		for (auto ks: _reaction_graph.kinds())
		{
			rx_cnt[std::get<0>(ks)]=0;
		}

		auto rx_iter=_reaction_graph.reactions();
		for ( ; rx_iter.first!=rx_iter.second; ++rx_iter.first)
		{
			auto kind=rx_iter.first->kind;
			auto prop_id=std::make_tuple(kind, rx_cnt[kind]);
			_to_idx[*rx_iter.first]=prop_id;
			_to_rx_id[prop_id]=*rx_iter.first;
			rx_cnt[kind]++;
		}

		for (auto k_count: rx_cnt)
		{
			BOOST_LOG_TRIVIAL(debug) << "Recorded " << std::get<1>(k_count)
					<< " ids for " << int(std::get<0>(k_count));
		}
	}


	std::vector<std::tuple<ReactionKind,size_t>> kinds() const
	{
		BOOST_LOG_TRIVIAL(trace) << "reaction.kinds() begin ";
		std::vector<std::tuple<ReactionKind,size_t>> by_kind;
		BOOST_LOG_TRIVIAL(trace) << "reaction.kinds by_kind gotten";
		std::map<ReactionKind,size_t> kind_set=_reaction_graph.kinds();
		BOOST_LOG_TRIVIAL(trace) << "reaction.kinds kind_set size="
				<< kind_set.size();

		for (const auto& kind_cnt : kind_set)
		{
			auto kind=std::get<0>(kind_cnt);
			size_t cnt=std::get<1>(kind_cnt);
			BOOST_LOG_TRIVIAL(trace) << "reaction.kinds kind="<<int(kind)<<std::endl
					<< "reaction.kinds cnt="<<cnt;
			by_kind.emplace_back(std::make_tuple(kind, cnt));
		}
		BOOST_LOG_TRIVIAL(trace) << "reaction.kinds() end";
		return by_kind;
	}


	ChangeSet fire(State& state, const PropensityId& propensity_id) const {
		BOOST_LOG_TRIVIAL(trace)<<"Reactions::fire begin";
		const auto& reaction_id=_to_rx_id.at(propensity_id);

		// First change the values.
		std::vector<ReactantId> changed;
		auto species_edges=_reaction_graph.inout_edges(reaction_id);
		for (auto edge : species_edges)
		{
			const auto& role=_reaction_graph[edge];
			auto rct_id=_reaction_graph.rct_target(edge);

			auto& space=state.individual(rct_id.subnet);
			space[rct_id.place]+=role.delta;
			changed.push_back(rct_id);
		}

		// Then determine which reactions depend on those changed species.
		size_t change_cnt=0;
		ChangeSet changes;
		for (const auto& rx_kind_cnt : _reaction_graph.kinds())
		{
			auto rx_kind=std::get<0>(rx_kind_cnt);
			std::vector<std::tuple<PropensityId,PropensityType>> c_change;
			for (auto rct_changed : changed)
			{
				for (const auto& oe : _reaction_graph.out_edges(rct_changed))
				{
					const auto& affected_rx_id=_reaction_graph.rx_target(oe);

					if (affected_rx_id.kind==rx_kind)
					{
						auto propensity=_propensity(state, affected_rx_id);
						c_change.push_back(
								std::make_tuple(_to_idx.at(affected_rx_id), propensity));
						change_cnt++;
					}
				}
			}
			changes[rx_kind]=c_change;
		}

		BOOST_LOG_TRIVIAL(trace)<<"Reactions::fire end change cnt="<<change_cnt;
		return changes;
	}


	double _propensity(State& state, const ReactionId& rx_id) const
	{
		using RoleKind=typename ReactionGraph::RoleKind;
		using RoleMap=std::map<RoleKind,int>;
		using RolePMap=boost::associative_property_map<RoleMap>;
		RoleMap role_to_value;


		std::vector<typename ReactionGraph::ReactantId::ID> subnets;
		for (auto edge : _reaction_graph.inout_edges(rx_id))
		{
			const auto& role=_reaction_graph[edge];
			auto rct_id=_reaction_graph.rct_target(edge);
			subnets.push_back(rct_id.subnet);

			auto& rct_space=state.individual(rct_id.subnet);
			role_to_value.emplace(std::make_pair(role.role,
					int(rct_space[rct_id.place])));
			BOOST_LOG_TRIVIAL(trace)<<"ReactionGraph::propensity role="
					<<int(role.role)<< " value="<<int(rct_space[rct_id.place]);
		}

		double prop_value=0;
		try {
			BOOST_LOG_TRIVIAL(trace) << "ReactionGraph::propensity evaluate props";
			const auto& params=_params.parameters(subnets[0]);
			// params: boost::associative_property_map<
			//   std::map<stochnet::Parameters, double>>
			RolePMap role_pmap(role_to_value);
			// role_pmap: boost::associative_property_map<std::map<stochnet::RoleKind,
			//     int>
			prop_value=_reaction_graph.propensity(rx_id.kind, role_pmap, params);
		}
		catch (std::out_of_range& oor)
		{
			BOOST_LOG_TRIVIAL(error) << "The propensity calculation for " << rx_id
					<< " uses a role that is not defined in the reaction graph, which "
					"has ";
			for (auto& k_p: role_to_value)
			{
				BOOST_LOG_TRIVIAL(error) << int(std::get<0>(k_p)) << " ";
			}
			throw;
		}
		BOOST_LOG_TRIVIAL(trace) << "ReactionGraph::propensity end "<<prop_value;
		return prop_value;
	}


	ChangeSet update_all(State& state) const
	{
		BOOST_LOG_TRIVIAL(trace) << "Reactions::update_all begin";
		ChangeSet changes;
		for (auto kind_size: _reaction_graph.kinds())
		{
			changes[std::get<0>(kind_size)]=
					std::vector<std::tuple<PropensityId,PropensityType>>();
		}
		BOOST_LOG_TRIVIAL(trace) << "Reactions::update_all changeset created";

		auto rx_iter=_reaction_graph.reactions();
		for ( ; rx_iter.first!=rx_iter.second; ++rx_iter.first)
		{
			const auto& rx_id=*rx_iter.first;
			auto propensity=_propensity(state, rx_id);
			BOOST_LOG_TRIVIAL(trace) << "kind="<<int(rx_id.kind)<<" rx_id="<<rx_id;
			auto prop_id=_to_idx.at(rx_id);
			changes.at(rx_id.kind).push_back(std::make_tuple(prop_id, propensity));
		}

		BOOST_LOG_TRIVIAL(trace) << "Reactions::update_all end";
		return changes;
	}

};


}

#endif // _REACTION_H_
