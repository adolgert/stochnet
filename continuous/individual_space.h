#ifndef _INDIVIDUAL_SPACE_H_
#define _INDIVIDUAL_SPACE_H_ 1


#include <type_traits>



namespace stochnet
{

template<typename StoreType>
class SpacePlace
{
public:
  using IndexType=StoreType;
  //using IndexType=typename std::remove_const<StoreType>::type;
private:
  StoreType& _val;
  IndexType _idx;
public:
  SpacePlace(StoreType& val, IndexType idx)
  : _val(val), _idx(idx) {}
  SpacePlace(const SpacePlace& other)
  : _val(other._val), _idx(other._idx)
  {}
  SpacePlace& operator=(const SpacePlace& other)
  {
    _val=other._val;
    _idx=other._idx;
  }

  operator int()
  {
    if (_val==_idx) return 1;
    else return 0;
  }

  operator StoreType() const
  {
    if (_val==_idx)
    {
      return 1;
    }
    else
    {
      return 0;
    }
  }
  void operator=(const int& val)
  {
    if (val!=0)
    {
      _val=_idx;
    }
    else
    {
      if (_val==_idx)
      {
        // Using 0 as an undefined state.
        _val=StoreType(0);
      }
      else
      {
        ;
      }
    }
  }
  bool operator==(const int& val) const
  {
    if (_val==_idx)
    {
      return val==1;
    }
    else
    {
      return val==0;
    }
  }
  SpacePlace& operator+=(const int& val)
  {
    int pseudo;
    if (_val==_idx)
    {
      pseudo=1;
    }
    else
    {
      pseudo=0;
    }
    pseudo+=val;
    this->operator=(pseudo);
    return *this;
  }


  SpacePlace& operator-=(const int& val)
  {
    StoreType pseudo;
    if (_val==_idx)
    {
      pseudo=1;
    }
    else
    {
      pseudo=0;
    }
    pseudo-=val;
    this->operator=(pseudo);
    return *this;
  }
};



template<typename StoreType>
class IndividualSpace
{
public:
  using IndexType=StoreType;
private:
  StoreType _val;

public:
  using Place=typename stochnet::SpacePlace<StoreType>;
  using ConstPlace=
      typename stochnet::SpacePlace<typename std::add_const<StoreType>::type>;

  IndividualSpace() =default;
  IndividualSpace(StoreType val) : _val(val) {}

  Place operator[](IndexType idx)
  {
    return Place(_val, idx);
  }

  ConstPlace operator[](IndexType idx) const
  {
    return {_val, idx};
  }

  operator StoreType() const
  {
    return _val;
  }
};

}

#endif
