#ifndef _INDEXED_PROPENSITY_ITERATOR_H_
#define _INDEXED_PROPENSITY_ITERATOR_H_ 1

#include <vector>
#include <utility>
#include <memory>
#include <boost/iterator/iterator_facade.hpp>


namespace stochnet
{


template<class ReactionId>
class IndexedPropensityIterator
: public boost::iterator_facade<
  IndexedPropensityIterator<ReactionId>,
  ReactionId const,
  boost::forward_traversal_tag
  >
{
public:
  using RXVec=std::vector<ReactionId>;
private:
  size_t _idx;
  std::shared_ptr<RXVec> _val;

public:
  IndexedPropensityIterator(std::shared_ptr<RXVec> vals, bool end=false)
  : _idx(0), _end(end), _val(vals)
  {
  }

  IndexedPropensityIterator(const IndexedPropensityIterator& other)
  : _idx(0), _end(other._end), _val(other._val)
  {
  }

  ~IndexedPropensityIterator()
  {
  }


private:
  friend class boost::iterator_core_access;

  void increment() {
    ++_idx;
    if (_idx==_val->size())
    {
      _end=true;
    }
  }

  bool equal(const IndexedPropensityIterator& other) const
  {
    return (_end && other._end) ||
           ((!_end && !other._end) && (_idx==other._idx));
  }

  ReactionId const& dereference() const
  {
    return _val->at(_idx);
  }

  bool _end;
};



template<class SubIter>
class LooseContainer
{
  SubIter& _a;
  SubIter& _b;
public:
  LooseContainer(SubIter& a, SubIter& b)
  : _a(a), _b(b)
  {
  }

  SubIter& begin()
  {
    return _a;
  }

  SubIter& end()
  {
    return _b;
  }
};

}
#endif // _INDEXED_PROPENSITY_ITERATOR_H_
