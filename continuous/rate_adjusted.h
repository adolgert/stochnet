#ifndef _RATE_ADJUSTED_H_
#define _RATE_ADJUSTED_H_ 1

#include <exception>
#include <functional>
#include <vector>
#include <utility>

#include "binary_prefix.h"



namespace afidd
{
/*! RateAdjusted is responsible for calculating the total of an array of rates.
 *  Collaborates with BinaryPrefix search tree to calculate the total rate.
 *  Template argument: SearchValue indexes the reactions. Maybe an int or size_t.
 */
template<typename SearchValue>
class RateAdjusted
{
  private:
    double _scaling;
    using TreeType=BinaryPrefix<SearchValue>;
    TreeType _raw_rate;
  public:
    using IndexType=typename TreeType::IndexType;
    RateAdjusted(size_t propensity_cnt, double scaling)
      : _scaling(scaling), _raw_rate(propensity_cnt)
      {}
    ~RateAdjusted() {};


    double total()
    {
      return _raw_rate.total()/_scaling;
    }


    IndexType choose(double val)
    {
      return std::get<0>(_raw_rate.choose(val*_scaling));
    }


    template<class IdIter>
    void update(IdIter&& changes)
    {
      _raw_rate.update(std::forward<IdIter>(changes));
    }
};


}

#endif // _RATE_ADJUSTED_H_
