#ifndef _HAZARDS_H_
#define _HAZARDS_H_ 1

#include <map>
#include <vector>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "petri/petri_graph.h"



namespace afidd
{

  namespace detail
  {


  }


  template<typename Service, typename ModelParameters>
  class ConstantHazards
  {
    const PetriGraphType& _net;
    Service& _serve_marking;
    const ModelParameters& _parameters;
    using PropensityMap=std::map<size_t,std::function<double(
        const typename Service::MarkContainer&,
        const typename ModelParameters::ParameterContainer&)>>;
    PropensityMap _propensity_funcs;
    std::map<size_t,size_t> _place_to_id;
    std::vector<size_t> _id_to_place;

  public:
    using PropensityList=std::vector<std::tuple<size_t,double>>;

    ConstantHazards(const PetriGraphType& net, Service& service,
        const ModelParameters& parameters, const PropensityMap& prop_funcs)
    : _net(net), _serve_marking(service), _parameters(parameters),
      _propensity_funcs(prop_funcs)
    {
      using VertexIter=boost::graph_traits<PetriGraphType>::vertex_iterator;
      using VertexType=boost::graph_traits<PetriGraphType>::vertex_descriptor;
      VertexIter vb, ve;
      size_t transition_cnt=0;
      bool all_zero=true;
      for (tie(vb, ve)=vertices(_net); vb!=ve; ++vb)
      {
        if (is_transition(*vb, _net))
        {
          VertexType transition=*vb;
          _place_to_id.emplace_hint( _place_to_id.end(),
              std::make_pair(transition, transition_cnt));
          _id_to_place.push_back(transition);
          transition_cnt++;
        }
      }
    }


    size_t size() const
    {
      size_t cnt=0;
      using VertexIter=boost::graph_traits<PetriGraphType>::vertex_iterator;
      VertexIter vb, ve;
      for (tie(vb, ve)=vertices(_net); vb!=ve; ++vb)
      {
        if (is_transition(*vb, _net))
        {
          ++cnt;
        }
      }
      return cnt;
    }


    template<typename Selection>
    PropensityList propensity_all(Selection& select) const
    {
      BOOST_LOG_TRIVIAL(debug) << "ConstantHazards propensity_all()";
      PropensityList propensity_list;
      typename Service::MarkContainer mark;
      typename ModelParameters::ParameterContainer parameters;

      using VertexIter=boost::graph_traits<PetriGraphType>::vertex_iterator;
      using VertexType=boost::graph_traits<PetriGraphType>::vertex_descriptor;
      VertexIter vb, ve;
      size_t transition_cnt=0;
      bool all_zero=true;
      for (tie(vb, ve)=vertices(_net); vb!=ve; ++vb)
      {
        if (is_transition(*vb, _net))
        {
          VertexType transition=*vb;
          _serve_marking.read(mark, transition, _net);
          _parameters(parameters, transition, _net);
          size_t propensity_function_id=propensity(transition, _net);
          const auto& prop_func=_propensity_funcs.at(propensity_function_id);

          auto p=prop_func(mark, parameters);
          if (p>0)
          {
            all_zero=false;
            BOOST_LOG_TRIVIAL(debug) << "propensity_all " << transition
                << " " << p << " transition id" << propensity_function_id;
          }
          propensity_list.push_back(std::make_tuple(
              _place_to_id.at(transition), p));
          transition_cnt++;
        }
        else
        {
          // It's a place. Move on.
        }
      }
      if (all_zero)
      {
        BOOST_LOG_TRIVIAL(warning) << "ConstantHazards all transition "
            "probabilities are zero";
      }
      select.update(propensity_list);
      BOOST_LOG_TRIVIAL(debug) << "ConstantHazards ~propensity_all() "
        << transition_cnt << " transitions";
      return propensity_list;
    }



    void fire(size_t transition) const
    {
      size_t transition_id=_id_to_place[transition];
      auto affected_transitions=_serve_marking.fire(transition_id, _net);
    }



    template<typename Selection>
    PropensityList propensity_changed(Selection& select) const
    {
      PropensityList propensity_list;
      return propensity_list;
    }
};

}

#endif // _HAZARDS_H_
