#ifndef _BINARY_PREFIX_CACHE_H_
#define _BINARY_PREFIX_CACHE_H_ 1

#include <algorithm>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "binary_prefix.h"


namespace afidd
{
  template<typename ValueType>
  class BinaryPrefixCache
	{
    BinaryPrefix<ValueType> _prefix;

  public:
    BinaryPrefixCache() {}
    ~BinaryPrefixCache() {}



    template<typename Hazards>
    void initialize(const Hazards& hazards)
    {
      _prefix.set_size(hazards.size());
      hazards.propensity_all(*this);
    }




    double total() const
    {
      return _prefix.total();
    }




    std::tuple<size_t,double> choose(double uniform_variate) const
    {
      return _prefix.choose(uniform_variate);
    }




    template<class IdIter>
    void update(IdIter&& changes)
    {
      BOOST_LOG_TRIVIAL(debug) << "BinaryPrefixCache::update()";
      _prefix.update(std::forward<IdIter>(changes));
      BOOST_LOG_TRIVIAL(debug) << "BinaryPrefixCache::~update() total "
        << _prefix.total();
    }
  };
}

#endif
