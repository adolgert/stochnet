#ifndef _BINARY_PREFIX_H_
#define _BINARY_PREFIX_H_ 1

#include <vector>
#include <set>
#include <algorithm>
#include <tuple>
#include <cmath>
#include "stochnet.h"
#include "boost/log/core.hpp"


namespace afidd
{

//! A searchable cumulative sum of elements, stored in a dense binary tree.
/*
 *  Internally, indices in the binary tree number from 1, not zero,
 *  so that the first node is 1 and children are 2*n and 2*n+1. The
 *  parent of a child is n/2.
 *   1 - depth 0, 2^0 entries
 *   2  3 - depth 1, 2^1 entries
 *   4  5  6  7 - depth 2, 2^2 entries
 *   8  9 10 11 12 13 14 15 - depth 3, 2^3 entries
 */
template<typename ValueType>
class BinaryPrefix
{
  /*!< Depth of binary tree. Changing this to unsigned will result in
       a bug in a decrementing for loop below.*/
  int _depth; 
  std::vector<ValueType> _b; /*!< Array to store binary tree, starting at 1. */
public:
  using IndexType=size_t;

  BinaryPrefix() {}
  BinaryPrefix(size_t cnt) {
    this->set_size(cnt);
  }
  ~BinaryPrefix() {}


  void set_size(size_t cnt) {
    BOOST_LOG_TRIVIAL(debug) << "BinaryPrefix::set_size() to " << cnt;
    _depth=std::lround(std::ceil(std::log2(cnt)))+1;
    _b.resize(std::pow(2, _depth));
    _offset=std::pow(2, _depth-1);
  }

  //! Find minimum index such that prefix is greater than the given value.
  /*! Precondition: The value must be strictly less than the total
   * for the tree.
   * \param val : double, is a random number to choose one of the entries.
   * \return tuple(index of found entry, remainder of the value passed in)
   */
  std::tuple<size_t,double> choose(double val) const
  {
    ValueType val_typed=static_cast<ValueType>(val);
    size_t idx=1;
    for (int level=0; level<_depth-1; level++)
    {
      size_t left_child=2*idx;
      if (_b[left_child]>val_typed)
      {
        idx=left_child;
      }
      else
      {
        idx=left_child+1;
        val_typed-=_b[left_child];
      }
    }
    return std::make_tuple(idx-_offset, _b[idx]-val);
  }


  /*! The sum of all the entries in the tree. */
  ValueType total() const
  {
    return _b[1];
  }


  /*! Modifies entries in the list using a sparse format.
   * \param sparse_update has a input iterator over a tuple of (index,value).
   *        Each value is accessed with iter->get<0>() and iter->get<1>().
   */
  template<class UpdateList>
  void update(const UpdateList& sparse_update)
  {
    typedef std::set<size_t> Modifieds;
    Modifieds modify;
    for (const auto& mod_iter : sparse_update)
    {
      const auto& reaction_idx=std::get<0>(mod_iter);
      if (reaction_idx>=_offset)
      {
        BOOST_LOG_TRIVIAL(debug) << "offset "<<_offset<<" transition "
          <<reaction_idx;
        assert(reaction_idx<_offset);
      }
      size_t mod_idx=reaction_idx+_offset;
      auto new_val=std::get<1>(mod_iter);
      if (new_val!=_b[mod_idx]) {
        _b[mod_idx]=new_val;
        modify.insert(mod_idx/2);
      } else {
        ; // ignore a same value.
      }
    }

    Modifieds parents;
    Modifieds& change=modify;
    Modifieds& store=parents;
    for (int parent_depth=_depth-2; parent_depth>=0; --parent_depth)
    {
      for (const size_t& node_idx : change )
      {
        _b[node_idx]=_b[2*node_idx]+_b[2*node_idx+1];
        store.insert(node_idx/2);
      }
      change.clear();
      std::swap(change, store);
    }
  }

private:
  size_t _offset;
};


}

#endif // _BINARY_PREFIX_H_
