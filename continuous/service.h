#ifndef _SERVICE_H_
#define _SERVICE_H_ 1

#include <memory>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/property_map/vector_property_map.hpp"


namespace afidd
{
	
  template<typename Marking, typename ServeType>
  class SingleService
  {
    Marking* _mark;

  public:
    SingleService(Marking& mark) : _mark(&mark) {}
    using MarkContainer=boost::vector_property_map<ServeType>;

    void read(MarkContainer& in_out_marks, size_t transition,
      const PetriGraphType& net) const
    {
      size_t mark_idx=0;
      using InEdgeIter=boost::graph_traits<PetriGraphType>::in_edge_iterator;
      InEdgeIter eb, ee;
      bool all_positive=true;
      for (tie(eb, ee)=in_edges(transition, net); eb!=ee; eb++)
      {
        const auto& m=(*_mark)[source(*eb, net)];

        if (m.value==0)
        {
          all_positive=false;
        }
        in_out_marks[mark_idx]=ServeType(m.value>0? 1 : 0, m.when);
        mark_idx++;
      }
      if (all_positive)
      {
        BOOST_LOG_TRIVIAL(debug) << "SingleService all inputs positive for "
          << transition << " mark_idx " << mark_idx << " transition id "
          << propensity(transition, net);
      }
      using OutEdgeIter=boost::graph_traits<PetriGraphType>::out_edge_iterator;
      OutEdgeIter ob, oe;
      for (tie(ob, oe)=out_edges(transition, net); ob!=oe; ob++)
      {
        const auto& m=(*_mark)[source(*ob, net)];

        in_out_marks[mark_idx]=ServeType(m.value>0? 1 : 0, m.when);
        mark_idx++;
      }
    }



    std::vector<size_t>
    fire(size_t transition, const PetriGraphType& net) const
    {
      std::vector<size_t> affected_transitions;

      using InEdgeIter=boost::graph_traits<PetriGraphType>::in_edge_iterator;
      InEdgeIter eb, ee;
      bool all_positive=true;
      for (tie(eb, ee)=in_edges(transition, net); eb!=ee; eb++)
      {
        auto& m=(*_mark)[source(*eb, net)];
        assert(m.value>0);
        int edge_weight=net[*eb].weight;
        assert(edge_weight<=0);
        m.value+=edge_weight;
      }

      using OutEdgeIter=boost::graph_traits<PetriGraphType>::out_edge_iterator;
      OutEdgeIter ob, oe;
      for (tie(ob, oe)=out_edges(transition, net); ob!=oe; ob++)
      {
        auto& m=(*_mark)[source(*ob, net)];
        int edge_weight=net[*ob].weight;
        assert(edge_weight>=0);
        m.value+=edge_weight;
      }

      return affected_transitions;
    }
  };

}

#endif // _SERVICE_H_
