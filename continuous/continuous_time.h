#ifndef _CONTINUOUS_TIME_H_
#define _CONTINUOUS_TIME_H_ 1


#include <vector>
#include "stochastic_variable.h"


namespace afidd
{

namespace detail
{
  struct CTFunctor
  {
    Container& _container;

    CTFunctor() {}
    ~CTFunctor() {}

    void operator()(size_t idx, double interval, Distribution dist)
    {
      auto variate=boost::math::quantile(dist, interval);
      put(std::tuple(idx, variate), _container);
    }
  };
}



template<class RNGen>
class ContinuousTime
{
  using StochVec=std::vector<StochasticVariable>;
  StochVec _variable;

public:
  ContinuousTime() {}
  ~ContinuousTime() {};


  template<typename Transitions>
  void initialize(const Transitions& transitions) const
  {

  }


  std::tuple<size_t, double, bool> next(RNGen& gen) const
  {
    _transition->make_current(gen, heap);

    return heap->minimum_element();

    StochVec::const_iterator first=_variable.begin();
    StochVec::const_iterator last=_variable.end();

    if (first==last)
    {
      return tuple(0, 0.0, false);
    }

    size_t min_transition;
    double min_time;

    std::tie(min_transition, min_time)=first->value(gen);

    while (++first!=last)
    {
      size_t transition;
      size_t absolute_time;

      std::tie(transition, absolute_time)=first->value(gen);
      if (absolute_time<min_time)
      {
        min_transition=transition;
        min_time=absolute_time;
      }

    }

    return std::tuple(min_transition, min_time);
  }

};


}




#endif // _CONTINUOUS_TIME_H_
