#ifndef _SSA_DYNAMICS_H_
#define _SSA_DYNAMICS_H_ 1

#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "petri_graph.h"

namespace afidd
{

template<typename MarkType, typename SelectionPolicy,
    typename Probabilities, typename RNGen>
class SSADynamics
{
  public:
    struct InputToken {};
    struct State {
      RNGen rng;
      MarkType marking;
      bool running;
      double time;
    };
  private:
    const PetriGraphType& _net;
    SelectionPolicy _selection;
    Probabilities _probabilities;

  public:
    SSADynamics(const PetriGraphType& net, const Probabilities& probabilities)
    : _net(net), _probabilities(probabilities) {}


    void initialize(State& state) const
    {
      BOOST_LOG_TRIVIAL(debug) << "SSADynamics::initialize() begin";
      state.running=true;
      _selection.initialize(_probabilities);
      //typename Reactions::ChangeSet changes=_reactions.update_all(state.reactions);
      //_selection.update(state.ssa, changes);
    }


    void operator()(State& state, const InputToken& x) const
    {
      double duration=1.0;
      size_t reaction_id;
      if (state.running)
      {
/*        changed_probabilities=_probabilities.make_current(state.marking);
        probability_to_fire=_selection.choose(changed_probabilities);
        transition_to_fire=probability.which_transition(probability_to_fire);
        _service.fire(transition_to_fire, marking);
        probabilities.invalidate(changed_marking);
*/
        std::tie(reaction_id, duration, state.running)=
            _selection.next(state.rng);
        state.time+=duration;
        _probabilities.fire(reaction_id);
      }
    }
};


}

#endif // _SSA_DYNAMICS_H_
