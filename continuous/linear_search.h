#ifndef _LINEAR_SEARCH_H_
#define _LINEAR_SEARCH_H_ 1

#include <exception>
#include <map>
#include <memory>
#include <utility>
#include "stochnet.h"


namespace afidd
{

/*! This is a simple search strategy over propensities.
 */
template<class Searchable, typename RxKey>
class LinearSearch
{
  using SubMap=std::map<RxKey,std::shared_ptr<Searchable>>;
  SubMap _b;
public:
  using Key=RxKey;
  //! So an RxChoiceId might be <"infection","dallas-chicago">
  /*! or it might be <2,377394>
   */
  using RxChoiceId=std::tuple<Key,typename Searchable::IndexType>;

  LinearSearch() {}
  ~LinearSearch() {}


  template<typename Reactions>
  void initialize(const Reactions& reactions)
  {
    BOOST_LOG_TRIVIAL(debug) << "linear_search::initialize() begin";
    for (auto kind_cnt : reactions.kinds())
    {
      auto kind=std::get<0>(kind_cnt);
      auto cnt=std::get<1>(kind_cnt);

      BOOST_LOG_TRIVIAL(debug) << "linear_search::initialize kind="
          <<int(kind) <<" cnt="<<cnt;
      auto propensity_kind=std::make_shared<Searchable>(cnt, 1.0);
      this->add(propensity_kind, kind);      
    }
    BOOST_LOG_TRIVIAL(debug) << "linear_search::initialize() end";
  }


  void add(std::shared_ptr<Searchable> searchable, const Key& key)
  {
    _b[key]=searchable;
  }


  double total() const
  {
    double sum=0;
    for (auto& id_search : _b)
    {
      sum+=id_search.second->total();
    }
    return sum;
  }


  RxChoiceId choose(double val) const
  {
    for (auto& id_search : _b)
    {
      if (val<id_search.second->total())
      {
        return std::make_tuple(id_search.first, id_search.second->choose(val));
      }
      else
      {
        val-=id_search.second->total();
      }
    }
    throw std::runtime_error("The chosen value is more than total propensity.");
    return RxChoiceId();
  }


  template<class IterUpdates>
    void update(IterUpdates& changes)
    {
      for (const auto& key_val : changes)
      {
        _b[key_val.first]->update(key_val.second);
      }
    }
};

}

#endif // _LINEAR_SEARCH_H_
