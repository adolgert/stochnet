#ifndef _MARK_ONE_DYNAMICS_H_
#define _MARK_ONE_DYNAMICS_H_ 1

#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/random/uniform_int_distribution.hpp"
#include "petri_graph.h"

namespace afidd
{

/*! Mark one individual infected. Needs translation table from subnet to place.
 */
template<typename State>
class MarkOneDynamics
{
public:
	using SID=size_t;
	using LocalVertex=size_t;
	using PlaceTranslate=std::map<std::tuple<SID,LocalVertex>,size_t>;

	struct InputToken {};

	MarkOneDynamics(const PlaceTranslate& pt) : _place_translate(pt) {}

	void initialize(State& state) const
	{

	}


	void operator()(State& state, const InputToken& x) const
	{
    size_t infect_cnt=0;
    std::vector<SID> subnet_id;
    SID subnet;
    LocalVertex local;
    PlaceTranslate::const_iterator pt_begin, pt_end;
    for (pt_begin=_place_translate.begin();
	   pt_begin!=_place_translate.end();
	   ++pt_begin)
    {
    	std::tie(subnet, local)=pt_begin->first;
  	  if (local==0)
  		{
  		  state.marking[pt_begin->second].value=1;
  		  subnet_id.push_back(subnet);
  		}
  		else
  		{
  		  state.marking[pt_begin->second].value=0;
  		}
    }

    boost::random::uniform_int_distribution<size_t>
    		integer_variate(0, subnet_id.size()-1);
    size_t infect_idx=integer_variate(state.rng);
    BOOST_LOG_TRIVIAL(debug) << "MarkOneDynamics sets node "<<infect_idx;
    size_t susceptible=_place_translate.at(std::make_tuple(infect_idx, 0));
    assert(state.marking[susceptible].value==1);
    state.marking[susceptible].value=0;
    size_t infected=_place_translate.at(std::make_tuple(infect_idx, 1));
    assert(state.marking[infected].value==0);
    state.marking[infected].value=1;
	}
	

private:
	const PlaceTranslate& _place_translate;
};



}
#endif // _MARK_ONE_DYNAMICS_H_
