#ifndef _DIRECT_H_
#define _DIRECT_H_

#include <cmath>
#include <utility>
#include <tuple>
#include <memory>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/random/uniform_real_distribution.hpp"
#include "boost/random/uniform_01.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "boost/random/poisson_distribution.hpp"
#include "boost/random/binomial_distribution.hpp"
#include "boost/random/uniform_int_distribution.hpp"


namespace afidd
{


/*!
 * RNGen is the random number generator.
 * PropensityCache is a strategy for Direct to sum and choose
 * among propensities.
 */
template<class RNGen, class PropensityCache>
class Direct {

public:
  using UniformReal=boost::random::uniform_real_distribution<double>;
  using UniformNorm=boost::random::uniform_01<double>;
  using ReactionId=size_t;
  using Duration=double;

private:
  struct InternalState {
    UniformReal _uniform_real;
    UniformNorm _uniform_norm;
    PropensityCache _propensities;
  };

  std::unique_ptr<InternalState> _internal;
public:
  Direct() : _internal(new InternalState) {
  }



  template<typename Reactions>
  void initialize(const Reactions& reactions) const
  {
    _internal->_propensities.initialize(reactions);
  }



  std::tuple<ReactionId, double, bool> next(RNGen& gen) const {

    BOOST_LOG_TRIVIAL(trace) << "Direct::next begin";
    auto total=_internal->_propensities.total();
    if (total>0)
    {
      BOOST_LOG_TRIVIAL(trace) << "propensities total "
          << _internal->_propensities.total();
      UniformReal::param_type p(0, total);
      BOOST_LOG_TRIVIAL(trace) << "uniform real param";
      _internal->_uniform_real.param(p);
      BOOST_LOG_TRIVIAL(trace) << "uniform real";
      double selection=_internal->_uniform_real(gen);
      BOOST_LOG_TRIVIAL(trace) << "propensities.choose " << selection;
      ReactionId token;
      double excess;
      std::tie(token, excess)=_internal->_propensities.choose(selection);
      BOOST_LOG_TRIVIAL(trace) << "propensities chosen";
      double dt=-std::log(_internal->_uniform_norm(gen))
          /_internal->_propensities.total();
      BOOST_LOG_TRIVIAL(trace) << "Direct::next end";
      return std::make_tuple(token, dt, true);
    }
    else
    {
      BOOST_LOG_TRIVIAL(debug) << "Direct::next Zero propensities";
      return std::make_tuple(ReactionId(), 0, false);
    }
  }


  template<class IterUpdates>
  void update(IterUpdates&& changes) const
  {
    _internal->_propensities.update(std::forward<IterUpdates>(changes));
  }
};

}
#endif // _DIRECT_H_
