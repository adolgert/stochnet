#ifndef _MODEL_PARAMETERS_H_
#define _MODEL_PARAMETERS_H_ 1

#include <map>
#include <algorithm>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/property_map/vector_property_map.hpp"


namespace afidd
{
	class ConstantParameters
  {
  public:
    using ParameterContainer=boost::vector_property_map<double>;

    ConstantParameters()
    : _max_key{0}
    {
    }


    ~ConstantParameters() {}


    void put(size_t key, double value)
    {
      _params[key]=value;
      _max_key=std::max(key, _max_key);
    }


    void operator()(ParameterContainer& p, size_t transition,
        const PetriGraphType& net) const
    {
      //p.reserve(_max_key);

      auto iter=_params.cbegin();
      for (; iter!=_params.cend(); ++iter)
      {
        p[iter->first]=iter->second;
      }
    }

  private:
    std::map<size_t, double> _params;
    size_t _max_key;
  };
}

#endif // _MODEL_PARAMETERS_H_
