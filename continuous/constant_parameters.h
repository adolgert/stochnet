#ifndef _CONSTANT_PARAMETERS_H_
#define _CONSTANT_PARAMETERS_H_ 1



/*! Calculates parameters at each time step.
 *  This is a strategy for the reaction graph to calculate propensities.
 *  This particular one is very simple, just constant values.
 */
template<typename PropertyMap>
class ConstantParameters
{
  PropertyMap _pmap;
public:
  ConstantParameters(const PropertyMap& pmap)
  : _pmap(pmap)
  {
  }

  template<typename SubNet>
  const PropertyMap parameters(const SubNet& individual) const
  {
    return _pmap;
  }
};


#endif
