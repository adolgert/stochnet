#ifndef _FINITE_STATE_MACHINE_H_
#define _FINITE_STATE_MACHINE_H_ 1

#include <memory>
#include <stdexcept>
#include <algorithm>
#include "stochnet.h"



namespace stochnet {


/*! This class is responsible for separating time steps from measurements.
 *  It isn't the usual version of a finite state machine but more of a
 *  state translator, from input tokens X to output tokens Y.
 *  This represents a class of systems, not a known set of states
 *  and transitions among them.
 */
template<class Dynamics, class OutputFunction>
class FiniteStateMachine
{
public:
  using InputToken=typename Dynamics::InputToken;
  using State=typename Dynamics::State;
  using OutputToken=typename OutputFunction::OutputToken;
private:
  //! The dynamics determine the next step. They are const.
  const Dynamics& _dynamics;
  //! The output function reads the state and token to determine what to return.
  const OutputFunction& _output_function;
  //! This is the only mutable part.
  State _state;

public:
  //! The dynamics and output function are fixed, and const, for the lifetime.
  FiniteStateMachine(const Dynamics& dynamics,
      const OutputFunction& output_function)
  : _dynamics(dynamics), _output_function(output_function)
  {
    BOOST_LOG_TRIVIAL(debug) << "FiniteStateMachine dynamics "
      << &dynamics << std::endl
      << "FiniteStateMachine _dynamics " << &_dynamics;
  }


  ~FiniteStateMachine() {}

  //! Copy constructor uses same dynamics and output function.
  FiniteStateMachine(const FiniteStateMachine& obj)
  : _dynamics(obj._dynamics), _output_function(obj._output_function),
    _state(obj._state)
  {
  }


  //! Assignment operator just copies the state.
  FiniteStateMachine& operator=(const FiniteStateMachine& other)
  {
    _state=other._state;
    return *this;
  }


  //! Move constructor only effective if State is MoveConstructible.
  FiniteStateMachine(FiniteStateMachine&& other)
  : _dynamics(other._dynamics), _output_function(other._output_function)
  {
    _state=std::move(other._state);
  }


  //! Move assignment
  FiniteStateMachine& operator=(FiniteStateMachine&& other)
  {
    _state=std::move(other._state);
    return *this;
  }


  //! A machine is considered initialized when it receives its initial state.
  void initialize(State& initial_state)
  {
    BOOST_LOG_TRIVIAL(debug) << "fsm initialize() begin";
    _state=std::move(initial_state);
    _dynamics.initialize(_state);
    _initialized=true;
    BOOST_LOG_TRIVIAL(debug) << "fsm initialize() end";
  }


  /*! The output function measures current state before dynamics computes
   *  a new state.
   */
  OutputToken operator()(const InputToken& x)
  {
    if (!_initialized)
    {
      BOOST_LOG_TRIVIAL(info)
          << "Please call initalize() before calling the FiniteStateMachine";
      throw std::logic_error(
          "Please call initalize() before calling the FiniteStateMachine");
    }
    const auto& y=_output_function(_state, x);
    _dynamics(_state, x);
    return(y);
  }

private:
  bool _initialized=false;
};


}
#endif // _FINITE_STATE_MACHINE_H_
