#include <memory>
#include <map>
#include <cmath>
#include <boost/test/unit_test.hpp>
#include "boost/random/mersenne_twister.hpp"
#include "finite_state_machine.h"
#include "archetypes.h"
#include "direct.h"
#include "linear_search.h"
#include "rate_adjusted.h"
#include "indexed_propensity_iterator.h"


using namespace stochnet;

#include <string>


BOOST_AUTO_TEST_SUITE( concept1 )

BOOST_AUTO_TEST_CASE( test_finite_state_machine )
{
	using Dynamics=dynamics_archetype;
	using InputToken=typename Dynamics::InputToken;
	Dynamics dynamics{};
	using State=typename Dynamics::State;
	using OutputFunction=output_function_archetype<State, InputToken>;
	OutputFunction output_function{};

	FiniteStateMachine<Dynamics,OutputFunction> fsm{dynamics, output_function};
	State initial_state;

	fsm.initialize(initial_state);

	// Make a second one with same const dynamics and output function but
	// a copy of the state.
	FiniteStateMachine<Dynamics,OutputFunction> second_fsm{fsm};
	// Or copy the state into a pre-existing second fsm.
	second_fsm=fsm;

	InputToken x;
	const auto& y=fsm(x);
}


BOOST_AUTO_TEST_CASE( test_ssa_archetype )
{
	using RxChoiceId=int;
	using RNGen=boost::random::mt19937;

	RNGen rng;
	using SSA=ssa_archetype<RNGen>;
	SSA ssa;
	using SSAState=typename SSA::State;
	SSAState state;
	SSA::RxChoiceId rxid;
	double duration;
	std::tie(rxid, duration)=ssa.next(rng, state);
}



BOOST_AUTO_TEST_CASE( test_ssa_direct )
{
	using RxChoiceId=int;
	using RNGen=boost::random::mt19937;

	RNGen rng;
	using Propensities=propensities_archetype<int>;
	using SSA=Direct<RNGen, Propensities>;
	SSA ssa;
	using SSAState=SSA::State;
	SSAState state;
	SSA::ReactionId rxid;
	double duration;
	bool running;
	std::tie(rxid, duration, running)=ssa.next(rng, state);
}



BOOST_AUTO_TEST_CASE( test_indexed_propensity_iter )
{
	using RxChoiceId=std::tuple<size_t,size_t>;
	using PropIter=IndexedPropensityIterator<RxChoiceId>;
	using RXVec=PropIter::RXVec;
	auto vals=std::make_shared<RXVec>();
	  vals->push_back(RxChoiceId{0,0});
    vals->push_back(RxChoiceId{0,1});
    vals->push_back(RxChoiceId{0,2});

	PropIter running{vals, false};
	PropIter run_end{vals, true};
	LooseContainer<PropIter> ip{running, run_end};

	std::cout << "Starting the run" << std::endl;
	size_t cnt=0;
	for ( auto rx_id : ip )
	{
		std::cout << std::get<1>(rx_id) << std::endl;
		cnt+=1;
	}
	std::cout << "Ending the run " << cnt << std::endl;
	assert(cnt==3);
}




BOOST_AUTO_TEST_CASE( test_direct_propensities )
{
	using PropensityStorage=size_t;
	using KindPropensity=RateAdjusted<PropensityStorage>;
	using Propensities=LinearSearch<KindPropensity,std::string>;
	Propensities propensities;
	size_t edge_cnt=20;
	size_t node_cnt=10;
	auto infection_propensity=
								std::make_shared<KindPropensity>(edge_cnt, 1.0);
	propensities.add(infection_propensity, "infect");

	auto recovery_propensity=
								std::make_shared<KindPropensity>(node_cnt, 1.0);
	propensities.add(recovery_propensity, "recover");

	using ById=std::vector<std::tuple<std::tuple<int,size_t>,size_t>>;

	std::map<std::string,ById> changes;
	changes["infect"].push_back(std::make_tuple(
		std::make_tuple(3,7),9));
	changes["recover"]=ById();

	propensities.update(changes);

	auto token=propensities.choose(0.5);
}




BOOST_AUTO_TEST_CASE( ssa_direct_props )
{
	using RNGen=boost::random::mt19937;

	using PropensityStorage=size_t;
	using KindPropensity=RateAdjusted<PropensityStorage>;
	using Propensities=LinearSearch<KindPropensity,std::string>;
	using SSA=Direct<RNGen, Propensities>;
	using SSAState=SSA::State;

	// The random number generator would come from state outside
	// the propensities. It would be defined within the dynamics.
	RNGen rng;

	SSA ssa;
	SSAState state;

	Propensities& propensities=state._propensities;
	size_t edge_cnt=20;
	size_t node_cnt=10;
	auto infection_propensity=
								std::make_shared<KindPropensity>(edge_cnt, 1.0);
	propensities.add(infection_propensity, "infect");

	auto recovery_propensity=
								std::make_shared<KindPropensity>(node_cnt, 1.0);
	propensities.add(recovery_propensity, "recover");


	SSA::ReactionId rxid;
	double duration;
	bool running;
	std::tie(rxid, duration, running)=ssa.next(rng, state);

	// Now pretend the reactions responded with changes.
	using ById=std::vector<std::tuple<std::tuple<int,size_t>,size_t>>;

	std::map<std::string,ById> changes;
	changes["infect"]=ById();
	changes["recover"]=ById();

	ssa.update(state, changes);
}




BOOST_AUTO_TEST_CASE( propensity_testing )
{
	using PropensityStorage=size_t;
	using KindPropensity=RateAdjusted<PropensityStorage>;
	using Propensities=LinearSearch<KindPropensity,std::string>;


	Propensities propensities;
	size_t edge_cnt=20;
	auto infection_propensity=
								std::make_shared<KindPropensity>(edge_cnt, 1.0);
	propensities.add(infection_propensity, "infect");
	BOOST_CHECK( propensities.total()==0.0);

	using PropensityId=std::tuple<std::string,size_t>;
	using PropensityTuple=std::tuple<PropensityId,PropensityStorage>;

	std::map<std::string,std::vector<PropensityTuple>> vals;
	vals["infect"].push_back(std::make_tuple(
			std::make_tuple("infect",0), 1));
	propensities.update(vals);
	double eps=1e-7;
	BOOST_CHECK( std::abs(propensities.total()-1.0)<eps);
}

BOOST_AUTO_TEST_SUITE_END()
