#ifndef _COMPARTMENTAL_STATE_H_
#define _COMPARTMENTAL_STATE_H_ 1


#include <vector>
#include "boost/random/mersenne_twister.hpp"

namespace stochnet {


class CompartmentalState
{
public:
  typedef boost::mt19937 RandomGenerator;
private:
  size_t _individual_cnt;
  std::vector<int> _compartment;
  RandomGenerator _rngen;
public:
  CompartmentalState() {}
  CompartmentalState(size_t individuals)
  : _individual_cnt(individuals), _compartment(individuals)
  {}
};



class StateBuilder
{
  size_t _cnt;
public:
  typedef CompartmentalState ReturnType;
  StateBuilder(size_t how_many) : _cnt(how_many) {}
  ReturnType operator()()
  {
    return CompartmentalState(_cnt);
  }
};



template<StochasticSimulationAlgorithm, Reactions>
class SSADynamics
{
  StochasticSimulationAlgorithm _ssa;
public:
  SSADynamics() {
    _propensities.initialize(state);
  }

  state_change operator()(state, input_token x)
  {
    reaction, time=_ssa.next(_propensities, state.rand_gen);
    affected_reactions=_reactions.fire(reaction, state.components);
    _propensities.update(affected_reactions, state.components);
    return reaction, time;
  }
};


}
#endif // _COMPARTMENTAL_STATE_H_
