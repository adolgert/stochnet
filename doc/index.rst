.. Semi-Markov documentation master file, created by
   sphinx-quickstart on Tue Mar  4 08:53:27 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Semi-Markov's documentation!
=======================================

Contents:

.. toctree::
   :maxdepth: 2

   intro
   concepts


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

