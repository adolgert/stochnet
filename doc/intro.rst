==================================================
Introduction
==================================================

This is a library to simulate trajectories of semi-Markov processes in
continuous time. The library can model two representations of a semi-Markov
process, the holding time formulation and the generalized stochastic
Petri net representaion.

Let's say you own a mall that uses cell phone signals to track each
mall-goer's location within a few meters. You now know how long they
spend in each store, where they go next, and have a chart of how long
it took different customers to arrive at the destination. We could
make a model for the behavior of an individual customer by first making
every store a vertex in a graph. Then "where they go next" becomes
a Markov transition matrix, :math:`\pi_{ij}`, for a
Markov *chain* giving the probability
of a transition from one store to any other store.

If we use the travel times from store to store to estimate the customer's
expected travel time, then we are using a Markov *process*. The chart
of the distribution of travel times is called a holding time,
:math:`h_{ij}(\tau)`. It can
incorporate into the model the tendency of customers either to wander
quickly through a store or stay for a long time.

Together, the stochastic matrix and holding time define a Markov process
for movement of an individual through the mall, with probability density
:math:`q_{ij}(\tau)=\pi_{ij}h_{ij}(\tau)`.
Once we record the absolute time that individual arrives in a store, by summing
all times since entry into the mall, we have defined a semi-Markov process.


Chemical reactions are another example of a semi-Markov system.
Instead of recording people at stores, there are counts of the number of
molecules of each chemical species in a mixture and rules for how
these chemical species interact, such as autoprotolysis of sulfuric acid,
:math:`2 H_2SO_4⇌H_3SO_4^+ + HSO_4^-`.
In this reaction, there are three chemical species and, moving left to right,
two molecules of one become one each of two molecules. The factors,
:math:`(2,1,1)`, are called stochiometric coefficients.

If one reaction creates some of a molecule, then it might enable or disable
a reaction that depends on a reactant or product. The reactions therefore
affect each others' rates through their affects on the count of molecules.
We can think of these reactions as competing, each with a constant hazard
of firing, where the first to fire wins the currently-available products.

Molecular simulations of chemical reactions like this are a 
special case of semi-Markov systems, indeed a special case of the generalized
stochastic Petri net representation of semi-Markov systems, special
because they restrict the problem to hazards of firing which are constant in time.
Going back to the mall travel times, it is, in general, possible to model
systems where processes compete to fire first and do so with variation
in time since when they first were enabled.


