#ifndef _STOCHNET_H_
#define _STOCHNET_H_ 1

// Logging will not link unless this is defined _before_ calling the header.
#define BOOST_LOG_DYN_LINK 1
#include "boost/log/trivial.hpp"

#endif
