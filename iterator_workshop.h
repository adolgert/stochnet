#ifndef _ITERATOR_WORKSHOP_H_
#define _ITERATOR_WORKSHOP_H_




	using TakeFirstReaction=
			std::function<ReactionId(std::pair<ReactionId,RctNode>&)>;
	using ReactionIter=typename std::vector<ReactionId>::const_iterator;
	std::pair<ReactionIter,ReactionIter>
	reactions() const
	{
		return std::make_pair(_reaction_id.cbegin(), _reaction_id.cend());
		//using MapSample=std::map<ReactionId,RctNode>::const_iterator;

		//using MapPair=std::iterator_traits<MapSample>::value_type;
		//using TakeFirst=std::function<int(const std::pair<int,double>&)>;
		//using PairFirst=boost::transform_iterator<TakeFirst,MapSample>;
/*
		TakeFirstReaction take_first=
				[](const std::pair<ReactionId,RctNode>& pr)->ReactionId
				{ return pr.first; };

		ReactionIter b(_find_reactant.begin(), take_first);

		return std::pair<ReactionIter,ReactionIter>(
				ReactionIter(_find_reactant.begin(), take_first),
				ReactionIter(_find_reactant.end(), take_first)
				);
				*/




/*! This presents an iterator over pairs as an iterator over the first member.
 
template<typename PairIterator>
class PairFirstIterator
: public boost::iterator_adaptor<
		PairFirstIterator<PairIterator>,
		PairIterator,
		typename std::tuple_element<0,
				std::iterator_traits<PairIterator>::value_type
				>::type,
		boost::forward_traversal_tag
		>
{
private:
	struct enabler {};

public:
	PairFirstIterator()
	: PairFirstIterator::iterator_adaptor_(0)
	{}

	explicit PairFirstIterator(PairIterator p)
	: PairFirstIterator::iterator_adaptor_(p)
	{}

	template<class OtherValue>
	PairFirstIterator(ReactionIter<OtherValue> const& other,
			typename boost::enable_if<boost::is_convertible<OtherValue*,PairIterator>,
					enabler>::type = enabler())
	: PairFirstIterator::iterator_adaptor_(other.base())
	{}

private:
	friend class boost::iterator_core_access;
	void increment()
	{
		this->base_reference() = this->base()->next();
	}

	Value* dereference()
	{
		return std::get<0>(*this->base());
	}
};

*/


#endif
