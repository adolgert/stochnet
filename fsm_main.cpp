#include <memory>
#include <vector>
#include <fstream>
#include <map>
#include "stochnet.h"
#include "boost/log/core.hpp"
#include "boost/log/expressions.hpp"
#include "boost/program_options.hpp"
#include "boost/algorithm/string/join.hpp"
#include "contact_graph.h"
#include "finite_state_machine.h"
#include "individual_space.h"
#include "reaction.h"
#include "direct.h"
#include "linear_search.h"
#include "rate_adjusted.h"
#include "ssa_dynamics.h"
#include "stochnet_version.hpp"
#include "seir.h"
#include "constant_parameters.h"
#include "demangle.h"


using namespace stochnet;

template<typename State>
struct OutputFunction
{
  using OutputToken=bool;

  template<typename InputToken>
  bool operator()(const State& state, const InputToken& input) const
  {
    return state.running;
  };
};



void log_init(std::string level)
{
  namespace logging=boost::log;

  std::map<std::string,boost::log::trivial::severity_level> severities=
    {
      { "trace",   logging::trivial::trace },
      { "debug",   logging::trivial::debug },
      { "info",    logging::trivial::info },
      { "warning", logging::trivial::warning },
      { "error",   logging::trivial::error },
      { "fatal",   logging::trivial::fatal }
    };

  logging::trivial::severity_level assign_level=logging::trivial::info;
  auto logiter=severities.find(level);
  if (logiter!=severities.end())
  {
    assign_level=logiter->second;
  }
  else
  {
    std::cout << "Could not set the logging level from " << level
        << ". Choices are: ";
    std::vector<std::string> names;
    for (auto& kv : severities)
    {
      names.push_back(kv.first);
    }
    std::cout << boost::algorithm::join(names, ", ")
        << ". Choosing info." << std::endl;
  }

  logging::core::get()->set_filter(logging::trivial::severity >= assign_level);
}



int main(int argc, char** argv)
{
  namespace po=boost::program_options;
  po::options_description desc("sketch allowed options");
  size_t contact_cnt;
  std::string log_level;

  desc.add_options()
    ("help", "show help message")
    ("size,s", po::value<size_t>(&contact_cnt)->default_value(10),
      "Number of individuals in contact graph")
    ("build", "Show the version of source code for this program.")
    ("loglevel", po::value<std::string>(&log_level)->default_value("info"),
      "Set the logging level to trace, debug, info, warning, error, or fatal.")
    ;

  po::variables_map vm;
  auto parsed_options=po::parse_command_line(argc, argv, desc);
  po::store(parsed_options, vm);
  po::notify(vm);

  log_init(log_level);
  BOOST_LOG_TRIVIAL(trace) << "Starting the application";

  if (vm.count("help"))
  {
    std::cout << desc << std::endl;
    return 0;
  }

  if (vm.count("build"))
  {
    std::cout << "<?xml version=\"1.0\"?><application><version><![CDATA["
        << STOCHNET_VERSION << "]]></version>" << std::endl
        << "<config><![CDATA[" << STOCHNET_CFG << "]]></config>" << std::endl
        << "<compiledate>" << STOCHNET_COMPILE_TIME << "</compiledate>"
        << std::endl << "</application>" << std::endl;
    return 0;
  }

  // Build a contact graph.
  using RNGen=boost::random::mt19937;
  using ContactGraph=ErdosRenyiContactGraph::GraphType;
  using VertexProperty=ErdosRenyiContactGraph::VertexIdProperty;
  using EdgeIdProperty=ErdosRenyiContactGraph::EdgeIdProperty;

  auto rd=seir();
  using SEIRDescription=decltype(rd);

  // The parameters for the reactions have a strategy.
  using Parameter=typename SEIRDescription::Parameter;
  using ParamMap=std::map<Parameter,double>;
  using ParamProp=boost::associative_property_map<ParamMap>;
  using ParamStrategy=ConstantParameters<ParamProp>;

  // Make a reaction graph from the contact graph.
  using PlaceKind=SEIRDescription::PlaceKind;
  using RGraph=ReactionGraph<SEIRDescription,ContactGraph,VertexProperty>;
  using Reactions=DirectReactions<RGraph,ParamStrategy>;
  using ReactionKind=typename Reactions::ReactionKind;

  // Propensity type is int or double.
  using PropensityId=Reactions::PropensityId;
  using KindPropensity=RateAdjusted<typename Reactions::PropensityType>;
  using Propensities=LinearSearch<KindPropensity,ReactionKind>;
  using SSA=Direct<RNGen, Propensities>;

  // And then assemble it into the finite state machine.
  using Dynamics=typename stochnet::SSADynamics<SSA, Reactions, RNGen>;
  using State=typename Dynamics::State;

  using FSM=typename stochnet::FiniteStateMachine
      <
      Dynamics,
      OutputFunction<State>
      >;

  ParamMap params_map;
  ParamProp params(params_map);
  put(params, Parameter::alpha, 0.55);
  put(params, Parameter::beta, 0.55);
  put(params, Parameter::gamma, 0.55);

  ParamStrategy param_strategy(params);

  // Place the state on the free store.
  auto state=std::make_shared<State>();

  RNGen& rn_gen=state->rng;
  std::string rn_gen_file{"rn_gen.txt"};
  {
    std::ifstream rn_gen_in{rn_gen_file};
    if (rn_gen_in)
    {
      rn_gen_in >> rn_gen;
      BOOST_LOG_TRIVIAL(info) << "Read random initialization from file.";
    }
  }
  ContactGraph cg;
  VertexProperty verts;
  EdgeIdProperty edge_to_idx;

  size_t individual_cnt=contact_cnt;
  double average_neighbors=4;
  double probable_connection=average_neighbors/individual_cnt;
  std::tie(cg, verts, edge_to_idx)=
      ErdosRenyiContactGraph::create(rn_gen, individual_cnt, probable_connection);

  RGraph reaction_graph(rd, cg, verts);
  BOOST_LOG_TRIVIAL(debug) << "reaction_graph created";


  Reactions reactions(reaction_graph, param_strategy);
  //
  // Have the reactions. Time to make a dynamics on them.
  //


  SSA ssa;
  BOOST_LOG_TRIVIAL(debug) << "ssa created";

  Dynamics dynamics(ssa, reactions);
  BOOST_LOG_TRIVIAL(debug) << "dynamics created " << &dynamics;

  OutputFunction<State> output_function;

  for (size_t ind_idx=0; ind_idx<individual_cnt; ind_idx++)
  {
    state->reactions.individual(ind_idx)[PlaceKind::s]=1;
  }
  state->reactions.individual(0)[PlaceKind::i]=1;

  FSM fsm(dynamics, output_function);
  BOOST_LOG_TRIVIAL(debug) << "fsm created";
  fsm.initialize(*state);
  BOOST_LOG_TRIVIAL(debug) << "fsm initialized ";

  size_t iterations=0;
  bool running=true;
  while (running)
  {
    BOOST_LOG_TRIVIAL(debug) << "one more " << iterations;
    running=fsm({});
    ++iterations;
  }

  BOOST_LOG_TRIVIAL(info) << "Ran iterations="<<iterations;

  {
    std::ofstream rn_gen_out{rn_gen_file};
    rn_gen_out<<rn_gen;
    BOOST_LOG_TRIVIAL(info) << "Wrote random initialization to file.";
  }
  return(0);
}
