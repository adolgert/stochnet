#ifndef _DISCRETE_TIME_REACTIONS_H_
#define _DISCRETE_TIME_REACTIONS_H_ 1


#include <map>
#include <vector>
#include <type_traits>
#include "stochnet.h"
#include "discrete_reaction.h"
#include "individual_space.h"

namespace stochnet
{


struct RxDelta
{
  std::set<std::tuple<ReactantId,int>>
};



template<typename ReactionGraph>
class DiscreteTimeReactions
{
  using ReactionId=typename ReactionGraph::ReactionId;
  using ReactionKind=typename ReactionGraph::ReactionKind;
  using PropensityType=double;
  using PropensityId=std::tuple<ReactionKind,size_t>;

public:
  using PlaceKind=typename ReactionGraph::PlaceKind;
  using IndividualState=IndividualSpace<PlaceKind>;
  struct InputToken {};
  struct State {
      // Choose a vector or a map depending on how the individuals are named.
      typedef typename std::conditional<
          std::is_unsigned<IndividualState>::value,
          std::vector<IndividualState>,
          std::map<IndividualName,IndividualState>
          >::type StateStorage;
      StateStorage _individual;
      IndividualState& individual(const IndividualName& ind_idx) {
        return _individual[ind_idx];
      }  
private:
  const ReactionGraph& _reaction_graph;
  std::vector<std::vector<ReactionId>> _discrete_reactions;

public:
  DiscreteTimeReactions(const ReactionGraph& rg)
  : _reaction_graph(rg)
  {
    _discrete_reactions=discrete_reaction(reaction_graph);
  }

  void operator()(State& state, Token x)
  {

  }

};




}





#endif
