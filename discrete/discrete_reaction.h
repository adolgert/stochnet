#ifndef _DISCRETE_REACTION_H_
#define _DISCRETE_REACTION_H_ 1

#include <map>
#include <vector>
#include <algorithm>
#include <tuple>
#include "boost/range/iterator_range.hpp"



namespace stochnet
{


/*! Convert a reaction graph into a set of reactions for discrete time.
 *  This groups together all reactions with the same change set, those which
 *  affect the state of the graph the same way.
 */
template<typename ReactionGraph>
std::vector<std::vector<typename ReactionGraph::ReactionId>>
discrete_reaction(const ReactionGraph& rg)
{
  using ReactantId=typename ReactionGraph::ReactantId;
  using ReactionId=typename ReactionGraph::ReactionId;

  using RoleDelta=std::tuple<ReactantId,int>;
  using ChangeSet=std::vector<RoleDelta>;
  std::map<ChangeSet,std::vector<ReactionId>> discrete_reactions;
  for (const auto& rx_id : boost::make_iterator_range(rg.reactions()))
  {
    ChangeSet cs;
    auto species_edges=rg.inout_edges(rx_id);
    for (auto edge : species_edges)
    {
      const auto& role=rg[edge];
      if (role.delta!=0)
      {
        auto rct_id=rg.rct_target(edge);
        auto rct0=std::find_if(cs.begin(), cs.end(),[&rct_id](const RoleDelta& rd)
            {
              return rct_id==std::get<0>(rd);
            });
        if (rct0==cs.end())
        {
          cs.push_back(std::make_tuple(rct_id,role.delta));
        }
        else
        {
          auto total_delta=std::get<1>(*rct0)+role.delta;
          if (total_delta==0)
          {
            cs.erase(rct0);
          }
          else
          {
            std::get<1>(*rct0)=total_delta;
          }
        }
      }
    }

    std::sort(cs.begin(), cs.end(),
      [](const RoleDelta& a, const RoleDelta& b)
      {
        return std::get<0>(a) < std::get<0>(b);
      }
      );

    auto p_discrete_rx=discrete_reactions.find(cs);
    if (p_discrete_rx==discrete_reactions.end())
    {
      std::vector<ReactionId> reactions;
      reactions.push_back(rx_id);
      discrete_reactions[cs]=reactions;
    }
    else
    {
      p_discrete_rx->second.push_back(rx_id);
    }
  }

  std::vector<std::vector<typename ReactionGraph::ReactionId>> reaction_vec;
  for (const auto& cs_rx : discrete_reactions)
  {
    reaction_vec.push_back(cs_rx.second);
  }
  return reaction_vec;
}


}

#endif
