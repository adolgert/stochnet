#ifndef _DISCRETE_DYNAMICS_H_
#define _DISCRETE_DYNAMICS_H_ 1


#include <map>
#include <vector>
#include <cmath>
#include <type_traits>
#include "stochnet.h"
#include "boost/property_map/property_map.hpp"
#include "boost/random/uniform_01.hpp"
#include "discrete_reaction.h"
#include "individual_space.h"



namespace stochnet
{

template<class RNGen, class ReactionGraph, typename ParamStrategy>
class DiscreteTimeDynamics
{
  using ReactionId=typename ReactionGraph::ReactionId;
  using ReactionKind=typename ReactionGraph::ReactionKind;
  using PropensityType=double;
  using PropensityId=std::tuple<ReactionKind,size_t>;
  using UniformNorm=boost::random::uniform_01<double>;
public:
  using PlaceKind=typename ReactionGraph::PlaceKind;
  using IndividualState=IndividualSpace<PlaceKind>;
  using IndividualName=typename ReactionGraph::IndividualName;
  //using SSA=SSAType;
  struct InputToken {};
  struct State {
    RNGen rng;
    bool running;
    size_t current;
    UniformNorm _uniform_norm;
    // Choose a vector or a map depending on how the individuals are named.
    typedef typename std::conditional<
        std::is_unsigned<IndividualState>::value,
        std::vector<IndividualState>,
        std::map<IndividualName,IndividualState>
        >::type StateStorage;
    StateStorage _individual[2];
    IndividualState& individual(const IndividualName& ind_idx) {
      return _individual[this->current][ind_idx];
    }
    IndividualState& new_individual(const IndividualName& ind_idx) {
      return _individual[1-this->current][ind_idx];
    }
  };
private:
  const ReactionGraph& _reaction_graph;
  std::vector<std::vector<ReactionId>> _discrete_reactions;
  const ParamStrategy& _params;

public:
  DiscreteTimeDynamics(const ReactionGraph& rg, const ParamStrategy& params)
  : _reaction_graph(rg), _params(params)
  {
    _discrete_reactions=discrete_reaction(rg);
  }

  void initialize(State& state) const
  {
    BOOST_LOG_TRIVIAL(debug) << "SSADynamics::initialize() begin";
    state.running=true;
  }

  void operator()(State& state, const InputToken& x) const
  {
    state._individual[1-state.current]=state._individual[state.current];

    for (const auto&  rx_vec : _discrete_reactions)
    {
      // Sum propensities of enabled reactions.
      double total_propensity=0;
      for (const auto& rx_id : rx_vec)
      {
        total_propensity+=_propensity(state, rx_id);
      }
      if (rx_vec.size()>1 && total_propensity>0)
      {
        total_propensity=1.0-std::exp(-total_propensity);
      }

      if (total_propensity>state._uniform_norm(state.rng))
      {
        _fire(state, rx_vec.at(0));
      }
    }
    state.current=1-state.current;
  }

private:
  double _propensity(State& state, const ReactionId& rx_id) const
  {
    using RoleKind=typename ReactionGraph::RoleKind;
    using RoleMap=std::map<RoleKind,int>;
    using RolePMap=boost::associative_property_map<RoleMap>;
    RoleMap role_to_value;

    std::vector<typename ReactionGraph::ReactantId::ID> subnets;
    for (auto edge : _reaction_graph.inout_edges(rx_id))
    {
      const auto& role=_reaction_graph[edge];
      auto rct_id=_reaction_graph.rct_target(edge);
      subnets.push_back(rct_id.subnet);

      auto& rct_space=state.individual(rct_id.subnet);
      role_to_value.emplace(std::make_pair(role.role,
          int(rct_space[rct_id.place])));
      BOOST_LOG_TRIVIAL(trace)<<"ReactionGraph::propensity role="
          <<int(role.role)<< " value="<<int(rct_space[rct_id.place]);
    }

    double prop_value=0;
    try {
      BOOST_LOG_TRIVIAL(trace) << "ReactionGraph::propensity evaluate props";
      const auto& params=_params.parameters(subnets[0]);
      // params: boost::associative_property_map<
      //   std::map<stochnet::Parameters, double>>
      RolePMap role_pmap(role_to_value);
      // role_pmap: boost::associative_property_map<std::map<stochnet::RoleKind,
      //     int>
      prop_value=_reaction_graph.propensity(rx_id.kind, role_pmap, params);
    }
    catch (std::out_of_range& oor)
    {
      BOOST_LOG_TRIVIAL(error) << "The propensity calculation for " << rx_id
          << " uses a role that is not defined in the reaction graph, which "
          "has ";
      for (auto& k_p: role_to_value)
      {
        BOOST_LOG_TRIVIAL(error) << int(std::get<0>(k_p)) << " ";
      }
      throw;
    }
    BOOST_LOG_TRIVIAL(trace) << "ReactionGraph::propensity end "<<prop_value;
    return prop_value;
  }


  void _fire(State& state, const ReactionId& reaction_id) const
  {
    // First change the values.
    auto species_edges=_reaction_graph.inout_edges(reaction_id);
    for (auto edge : species_edges)
    {
      const auto& role=_reaction_graph[edge];
      auto rct_id=_reaction_graph.rct_target(edge);

      auto& space=state.new_individual(rct_id.subnet);
      space[rct_id.place]+=role.delta;
    }
  }
};



	
}


#endif
