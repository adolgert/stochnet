// contact_graph_test.cpp
#include <iterator>
#include "demangle.h"
#include "boost/test/unit_test.hpp"
#include "boost/random/mersenne_twister.hpp"
#include "discrete_dynamics.h"
#include "reaction_description.h"
#include "contact_graph.h"
#include "reaction_graph.h"
#include "sir.h"
#include "constant_parameters.h"
#include "finite_state_machine.h"
#include "discrete_dynamics.h"


using namespace stochnet;




template<typename State>
struct OutputFunction
{
  using OutputToken=bool;

  template<typename InputToken>
  bool operator()(const State& state, const InputToken& input) const
  {
    return state.running;
  };
};




BOOST_AUTO_TEST_SUITE( discrete_time )

BOOST_AUTO_TEST_CASE( ssa )
{
  // Build a contact graph.
  using RNGen=boost::random::mt19937;
  using ContactGraph=ErdosRenyiContactGraph::GraphType;
  using VertexProperty=ErdosRenyiContactGraph::VertexIdProperty;
  using EdgeIdProperty=ErdosRenyiContactGraph::EdgeIdProperty;

  auto rd=sir();
  using SIRDescription=decltype(rd);

  // The parameters for the reactions have a strategy.
  using Parameter=typename SIRDescription::Parameter;
  using ParamMap=std::map<Parameter,double>;
  using ParamProp=boost::associative_property_map<ParamMap>;
  using ParamStrategy=ConstantParameters<ParamProp>;

  // Make a reaction graph from the contact graph.
  using PlaceKind=SIRDescription::PlaceKind;
  using RGraph=ReactionGraph<SIRDescription,ContactGraph,VertexProperty>;

  using Dynamics=DiscreteTimeDynamics<RNGen,RGraph,ParamStrategy>;
  using State=typename Dynamics::State;
  using Output=OutputFunction<State>;

  using FSM=typename stochnet::FiniteStateMachine
      <
      Dynamics,
      OutputFunction<State>
      >;


  auto state=std::make_shared<State>();
  RNGen& rn_gen=state->rng;
  ContactGraph cg;
  VertexProperty verts;
  EdgeIdProperty edge_to_idx;

  size_t individual_cnt=10;
  double average_neighbors=4;
  double probable_connection=average_neighbors/individual_cnt;
  std::tie(cg, verts, edge_to_idx)=
      ErdosRenyiContactGraph::create(rn_gen, individual_cnt, probable_connection);

  for (size_t ind_idx=0; ind_idx<individual_cnt; ind_idx++)
  {
    state->individual(ind_idx)[PlaceKind::s]=1;
  }
  state->individual(0)[PlaceKind::i]=1;


  RGraph reaction_graph(rd, cg, verts);
  std::cout << "reaction_graph created" << std::endl;

  ParamMap params_map;
  ParamProp params(params_map);
  put(params, Parameter::beta, 0.55);
  put(params, Parameter::gamma, 0.55);

  ParamStrategy param_strategy(params);

  Dynamics dynamics(reaction_graph, params);
  Output output;
  FSM machine(dynamics, output);

  machine.initialize(*state);

  for (size_t iterations=0; iterations<100; iterations++)
  {
    bool running=machine({});
  }
}


BOOST_AUTO_TEST_SUITE_END()
